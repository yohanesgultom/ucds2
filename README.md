# Sirsak

**S**istem **i**nfo**r**masi **s**umber daya manusi**a** **k**reatif

### Requirements

* Grails 2.2.1
* JDK 7
* PostreSQL

You can use other DBMS, by changing driver dependency in `BuildConfig.groovy` and datasource in `.sirsak.groovy` or `DataSource.groovy`

### Running

0. Make sure requirements are installed & running properly, checkout the source
1. Create database, create `.sirsak.groovy` or update `DataSource.groovy` with proper database detail
2. Go to source root and run `grails run-app`

### Environment specific conf

Create conf file in user home `$HOME/.sirsak.groovy`
In case of deploying in app server (eg. Tomcat, Jetty), make sure you put the `.sirsak.groovy` inside the `$HOME` of user running the service

### Developer

Yohanes Gultom (yohanes.gultom@gmail.com)