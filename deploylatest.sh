#!/bin/bash

pwd=$(pwd)
src_path="/home/sirsak/ucds2"
build_path="$src_path/dist"
deploy_path="/var/lib/tomcat7/webapps"
war_build_name="sirsak"
war_deploy_name="ROOT"
service_name="tomcat7"
genv="dev"
branch="master"

# set env if given in arg $1
if [ $# -gt 0 ]; then
  genv="$1"
fi

if [ $# -gt 1 ]; then
  branch="$2"
fi

# pull latest change
cd $src_path
git checkout $branch
git pull origin $branch

# build
echo "grails $genv war $build_path/$war_build_name.war"
grails $genv war "$build_path/$war_build_name.war"

# backup
echo "creating backup.."
now=$(date +"%Y%m%d%H%M%S")
sudo cp "$deploy_path/$war_deploy_name.war" "$pwd/$war_build_name.war.$now"

# deploy
echo "deploying.."
sudo service $service_name stop
sudo rm -R "$deploy_path/$war_deploy_name"
sudo rm "$deploy_path/$war_deploy_name.war"
sudo cp "$build_path/$war_build_name.war" "$deploy_path/$war_deploy_name.war"
sudo service $service_name start

# return to original path
cd "$pwd"
