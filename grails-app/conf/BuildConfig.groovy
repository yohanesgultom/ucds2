grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.7
grails.project.source.level = 1.7
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
        excludes 'xercesImpl', 'xml-apis'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve

    repositories {
        inherits true // Whether to inherit repository definitions from plugins
        grailsPlugins()
        grailsHome()
        grailsCentral()
        mavenCentral()

        // uncomment these to enable remote dependency resolution from public Maven repositories
        //mavenCentral()
        mavenLocal()
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
        //mavenRepo "http://maven.touk.pl/nexus/content/repositories/releases"
        grailsRepo "https://grails.org/plugins"
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        compile 'joda-time:joda-time:2.6'
        compile 'com.sun.mail:javax.mail:1.5.5'
        runtime 'mysql:mysql-connector-java:5.1.16'
        runtime 'postgresql:postgresql:8.4-702.jdbc3'
    }

    plugins {
        //compile ":grails-melody:1.54.0"
        runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.2.14"
        compile ":shiro:1.1.4"
        compile ":excel-export:0.2.1"
        compile ":cache:1.0.1"
        compile "org.grails.plugins:csv:0.3.1"
//        compile "org.grails.plugins:faker:0.7"
        build ":tomcat:$grailsVersion"
    }
}
