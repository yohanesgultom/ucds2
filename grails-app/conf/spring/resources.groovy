import com.ucds.core.util.MailService

// Place your Spring DSL code here
beans = {
    mailService(MailService) {}

    xmlns task: "http://www.springframework.org/schema/task"

    task.'scheduled-tasks'{
        // run everyday on 7 am
        task.scheduled(ref:'mailService', method: 'sendTrainingProgramsReminderEmail', cron: '0 0 7 * * *')
    }
}
