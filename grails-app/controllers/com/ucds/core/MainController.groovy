package com.ucds.core

import com.ucds.core.security.ShiroUser
import org.apache.shiro.SecurityUtils

class MainController {

    static final Map DEFAULT_HOME = [controller: "report", action: "competencyMatrix"]

    def index() {
        def user = SecurityUtils.subject.principals.oneByType(ShiroUser)
        user = ShiroUser.findById(user.id)
        def home = user.permissions?.contains("*:*") || user.permissions?.contains("${DEFAULT_HOME.controller}:${DEFAULT_HOME.action}") ? DEFAULT_HOME : null
        def filteredMenus = session.getAttribute("menus")
        if (!filteredMenus) {
            filteredMenus = []
            def menus = Menu.findAll("from Menu where parent is null order by seq asc")
            for (Menu menu : menus) {
                def removeChildren = []
                // check non-permitted children menu
                for (Menu childMenu : menu.children) {
                    def permission = "$childMenu.linkController:$childMenu.linkAction"
                    if (!SecurityUtils.subject.isPermitted(permission)) {
                        removeChildren << childMenu
                    }
                }
                // remove non-permitted children
                if (!removeChildren.empty) {
                    menu.children.removeAll(removeChildren)
                }
                // if there is children display it
                if (menu.children && !menu.children.empty) {
                    filteredMenus << menu
                }
            }
            session.setAttribute("menus", filteredMenus)
        }
        [menuInstanceList: filteredMenus, home:home]
    }
}
