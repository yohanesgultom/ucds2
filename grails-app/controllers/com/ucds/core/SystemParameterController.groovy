package com.ucds.core

import grails.converters.JSON

class SystemParameterController {

    def systemParameterService

    def index() {}

    def getList = {
        def res = systemParameterService.getList(params)
        render res as JSON
    }

    def edit = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    systemParameterService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    systemParameterService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    systemParameterService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
