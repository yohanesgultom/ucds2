package com.ucds.core.business

import grails.converters.JSON

class BalanceScoreCardController {

    def balanceScoreCardService

    def index() {}

    def getList = {
        def res = balanceScoreCardService.getList(params)
        render res as JSON
    }

    def edit = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    balanceScoreCardService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    balanceScoreCardService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    balanceScoreCardService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

    def getListOfValues = {
        def res = []
        BalanceScoreCard.findAllByInactive(false).each {
            res << [id: it.id, name: it.name]
        }
        render res as JSON
    }
}
