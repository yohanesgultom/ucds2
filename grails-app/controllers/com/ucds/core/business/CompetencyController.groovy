package com.ucds.core.business

import com.ucds.core.SystemParameter
import grails.converters.JSON

class CompetencyController {

    def competencyService

    def core() {}

    def managerial() {}

    def technical() {}

    def getList = {
        def res = competencyService.getList(params)
        render res as JSON
    }

    def getFilteredList = {
        params._search = true // force reuse search mechanism
        def res = competencyService.getList(params)
        render res as JSON
    }

    def edit = {
        def res = [:]
        switch(params.oper){
            case "add" :
                try {
                    competencyService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "edit" :
                try {
                    competencyService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "del" :
                try {
                    competencyService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
        }

        render res as JSON
    }


    def getCompetencyLevels = {
        def ret = []
        def competency = Competency.findById(params.competencyId)
        if (competency) {
            def competencyLevels = competency.competencyLevels
            if (competencyLevels) {
                for (CompetencyLevel competencyLevel : competencyLevels) {
                    ret << [level: competencyLevel.level, name: competencyLevel.name, keyIndicator: competencyLevel.keyIndicator]
                }
            }
        }
        render ret as JSON
    }
}
