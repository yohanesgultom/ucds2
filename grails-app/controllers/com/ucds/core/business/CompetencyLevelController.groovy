package com.ucds.core.business

import grails.converters.JSON

class CompetencyLevelController {

    def competencyLevelService

    def getFilteredList = {
        params._search = true // force reuse search mechanism
        params.searchString = ("competency_id".equals(params.searchField)) ? Double.parseDouble(params.searchString) : params.searchString
        def res = competencyLevelService.getList(params)
        render res as JSON
    }

    def edit = {
        def res = [:]
        switch(params.oper){
            case "add" :
                try {
                    competencyLevelService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "edit" :
                try {
                    competencyLevelService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "del" :
                try {
                    competencyLevelService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
