package com.ucds.core.business

import com.ucds.core.security.ShiroUser
import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class CompetencyPointController {

    def competencyPointService

    def index() {}

    def getList() {
        def res = competencyPointService.getList(params)
        render res as JSON
    }

    def edit = {
        def res = [:]
        // set current user as evaluator
        def user = SecurityUtils.subject.principals.oneByType(ShiroUser)
        params.evaluator = user.username
        switch (params.oper) {
            case "add":
                try {
                    competencyPointService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    competencyPointService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    competencyPointService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
