package com.ucds.core.business

import com.ucds.core.SystemParameter
import grails.converters.JSON

class HolidayController {

    def holidayService
    def appUtilService

    def index() {
        def map = appUtilService.loadParams()
        def months = []
        def years = []
        map.months.each { months << "${it.key}:${it.value}" }
        map.months = months.join(";")
        map.years.each { years << "${it}:${it}" }
        map.years = years.join(";")
        return map
    }

    def getList = {
        def res = holidayService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def getSummary = {
        def ret = [monthlyLeave:0 ,effectiveDays:0, effectiveMins:0, effectiveMinsMonthly:0, effectiveMinsWeekly:0, effectiveMinsDaily:0]
        def defaultWorkContractType = WorkContractType.findByName("Default")
        ret.monthlyLeave = (defaultWorkContractType) ? defaultWorkContractType.annualLeaveDays : 1f
        ret.effectiveMinsDaily = appUtilService.getEffectiveMinutesDaily()
        if (params.year) {
            ret.effectiveDays = appUtilService.getTotalWorkingDays(params.int("year"))
            ret.effectiveMins = appUtilService.getTotalEffectiveMinutes(params.int("year"))
            ret.effectiveMinsMonthly = ret.effectiveMins / 12
            ret.effectiveMinsWeekly = ret.effectiveMins / 60
        }
        render ret as JSON
    }

    def edit = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    holidayService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    holidayService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    holidayService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
