package com.ucds.core.business

import grails.converters.JSON

class KeySuccessFactorController {

    def keySuccessFactorService

    def index() {
        def cards = BalanceScoreCard.findAllByInactive(false, [sort: "name", order: "asc"])
        def categories = KeySuccessIndicatorCategory.findAllByInactive(false, [sort: "name", order: "asc"])
        [cards: cards, categories: categories]
    }

    def getList = {
        params._search = true
        params.searchString = Long.parseLong(params.searchString)
        def ret = keySuccessFactorService.getList(params)
        render ret as JSON
    }

    def edit = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    keySuccessFactorService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    keySuccessFactorService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    keySuccessFactorService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }
        render res as JSON
    }

    def getListOfValues = {
        def res = [[id: "", name: "--"]]
        KeySuccessFactor.findAll("from KeySuccessFactor where strategyObjective is not null order by title")?.each {
            res << [id: it.id, name: it.title]
        }
        render res as JSON
    }
}
