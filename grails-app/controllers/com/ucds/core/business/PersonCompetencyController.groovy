package com.ucds.core.business

import grails.converters.JSON

class PersonCompetencyController {

    def personCompetencyService

    def index() {}

    def getList = {
        def res = personCompetencyService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [:]
        try {
            res = personCompetencyService.edit(params)
            res.message = "success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

    def getEvaluationList = {
        def res = personCompetencyService.getEvaluationList(params)
        render res as JSON
    }

    def list = {
        def res = []
        if (params.personId && params.period) {
            def personId = Long.parseLong(params.personId)
            def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
            def personCompetencies = PersonCompetency.findAll("from PersonCompetency where person_id = ? and period = ? order by competency.name", [personId, period], [fetch: [competency: 'eager']])
            personCompetencies?.each {
                res << [id: it.id, competency: it.competency.name, level: it.expectedScore]
            }
        }
        render res as JSON
    }

    def needTrainingList = {
        def res = []
        if (params.personId && params.period) {
            def personId = Long.parseLong(params.personId)
            def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
            def personCompetencies = PersonCompetency.findAll("from PersonCompetency where person_id = ? and period = ? and gap < 0 order by competency.name", [personId, period], [fetch: [competency: 'eager']])
            personCompetencies?.each {
                res << [id: it.id, competency: it.competency.name, level: it.expectedScore]
            }
        }
        render res as JSON
    }

}
