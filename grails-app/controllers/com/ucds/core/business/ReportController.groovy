package com.ucds.core.business

import com.ucds.core.security.ShiroUser
import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import pl.touk.excel.export.WebXlsxExporter

class ReportController {
    def reportService
    def appUtilService

    def index = {}

    def processParams(params) {
        params.filters = params.filters ? JSON.parse(params.filters) : [:]
        params.period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
    }

    def competencyMatrix = {
        return appUtilService.loadParams()
    }

    def analyticalMatrix = {
        return appUtilService.loadParams()
    }

    def getCompetencyMatrixData = {
        processParams params
        def ret = reportService.getCompetencyMatrixData(params)
        render ret as JSON
    }

    def getAnalyticalMatrixData = {
        processParams params
        def ret = reportService.getAnalyticalMatrixData(params)
        render ret as JSON
    }

    def qualityDiagram = {
        return appUtilService.loadParams()
    }

    def getQualityDiagram = {
        processParams params
        def data = reportService.getQualityDiagram(params)
        def ticks = [message(code: "ucds.report.description.farbelow.message"), message(code: "ucds.report.description.below.message"), message(code: "ucds.report.description.satisfy.message"), message(code: "ucds.report.description.above.message"), message(code: "ucds.report.description.farabove.message"), message(code: "ucds.report.description.unevaluated.message")];
        def series = [[label:message(code: "ucds.report.competency.category.core.label")],[label:message(code: "ucds.report.competency.category.managerial.label")],[label:message(code: "ucds.report.competency.category.technical.label")]]
        def res = [data: data, ticks: ticks, series: series]
        render res as JSON
    }

    def competencyDiagram = {
        return appUtilService.loadParams()
    }

    def getCompetencyDiagram = {
        processParams params
        def diagram = reportService.getCompetencyDiagram(params)
        def res = [data: [diagram.scores], ticks: diagram.competencies]
        render res as JSON
    }

    def strategy = {
        return appUtilService.loadParams()
    }

    def strategyObjectiveGraph = {
        def graph = [:]
        params.period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
        try {
            graph = reportService.getStrategyObjectiveGraph(params)
        } catch (Exception e) {
            log.error(e.message, e)
        }
        render graph as JSON
    }

    def getUnderTargetKeySuccessFactors = {
        params.period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
        def graph = reportService.getUnderTargetKeySuccessFactors(params)
        render graph as JSON
    }

    def competencyGrowth = {
        def res = appUtilService.loadParams()
        // set default values as one period before
        def cal = Calendar.instance
        cal.set(Calendar.DATE, 1)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)

        res.defaultYearAfter = cal.get(Calendar.YEAR)
        res.defaultMonthAfter = cal.get(Calendar.MONTH) + 1
        cal.add(Calendar.MONTH, -1)
        res.defaultYearBefore = cal.get(Calendar.YEAR)
        res.defaultMonthBefore = cal.get(Calendar.MONTH) + 1
        return res
    }



    def getCompetencyGrowth = {
        // parse params
        params.periodBefore = Date.parse(grailsApplication.config.ucds.format.date, params.periodBefore)
        params.periodAfter = Date.parse(grailsApplication.config.ucds.format.date, params.periodAfter)
        // get growth list
        def res = reportService.getCompetencyGrowthList(params)
        def rows = []
        ((List<List>) res?.rows)?.each { values ->
            def fullName = (values[2]) ? "${values[1]} ${values[2]}" : "${values[1]}"
            rows << [ id: values[0], cell: [fullName] + values[3..-1]]
        }
        res = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render res as JSON
    }

    def exportCompetencyGrowth = {
        // parse params
        params.periodBefore = Date.parse(grailsApplication.config.ucds.format.date, params.periodBefore)
        params.periodAfter = Date.parse(grailsApplication.config.ucds.format.date, params.periodAfter)
        params.rows = 10000
        // get competency growth
        def res = reportService.getCompetencyGrowthList(params)
        def headers = [
            'fullName',
            'position',
            'section',
            'competencyName',
            'actualScoreBefore',
            'actualScoreAfter',
            'actualScoreGrowth'
        ]
        def data = []
        ((List<List>) res?.rows)?.each { values ->
            def row = [:]
            headers.eachWithIndex { field, index ->
                if (field == 'fullName') {
                    row[field] = (values[2]) ? "${values[1]} ${values[2]}" : "${values[1]}"
                } else {
                    row[field] = values[index + 2]
                }
            }
            data << row
        }
        def fileName = "${g.message(code: 'COMPETENCY_GROWTH')} ${params.periodBefore.format('MMM-yyyy')} - ${params.periodAfter.format('MMM-yyyy')}.xlsx"
        new WebXlsxExporter().with {
            setResponseHeaders(response, fileName)
            fillHeader(headers)
            add(data, headers)
            save(response.outputStream)
        }
    }

}
