package com.ucds.core.business

import grails.converters.JSON

class StrategyObjectiveController {

    def strategyObjectiveService
    
    def index() {}

    def getList = {
        def res = strategyObjectiveService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [message: null, success: true]
        switch (params.oper) {
            case "add":
                try {
                    strategyObjectiveService.add(params)
                } catch (e) {
                    log.error(e.message, e)
                    res.success = false
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    strategyObjectiveService.edit(params)
                } catch (e) {
                    log.error(e.message, e)
                    res.success = false
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    strategyObjectiveService.delete(params)
                } catch (e) {
                    log.error(e.message, e)
                    res.success = false
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

    def getDependencies = {
        def res = [:]
        if (params.strategyObjectiveId) {
            def strategyObjective = StrategyObjective.findById(params.strategyObjectiveId)
            if (strategyObjective) {
                def rows = []
                def dependencies = strategyObjective.dependencies.sort{it.strategyObjective.name}
                dependencies.each {
                    rows << [
                            id: it.id,
                            cell: [
                                    it.strategyObjective.id,
                                    it.weight
                            ]]
                }
                def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
                def size = strategyObjective.dependencies.size()
                def total = Math.ceil(size / max)
                res = [rows: rows, records: size, page: params.page, total: total]
            }
        }
        render res as JSON
    }

    def editDependencies = {
        def res = [success:true,message:null]
        switch (params.oper) {
            case "add":
                try {
                    strategyObjectiveService.addDependencies(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.success = false
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    strategyObjectiveService.editDependencies(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.success = false
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    strategyObjectiveService.deleteDependencies(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.success = false
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

    def getListOfValues = {
        def res = []
        StrategyObjective.findAllByInactive(false, [sort: "name"]).each {
            res << [id: it.id, name: it.name]
        }
        render res as JSON
    }

    def getKeySuccessFactors = {
        def res = [:]
        def strategyObjective = StrategyObjective.findById(params.strategyObjectiveId)
        if (strategyObjective) {
            def rows = []
            def dependencies = strategyObjective.keySuccessFactors.sort{it.title}
            dependencies.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.title,
                                (it.parent) ? it.parent.id : null,
                                (it.responsiblePositions) ? it.responsiblePositions*.name.join(', ') : null
                        ]]
            }
            def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
            def size = strategyObjective.keySuccessFactors.size()
            def total = Math.ceil(size / max)
            res = [rows: rows, records: size, page: params.page, total: total]
        }
        render res as JSON
    }

    def editKeySuccessFactor = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    strategyObjectiveService.addKeySuccessFactor(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    strategyObjectiveService.editKeySuccessFactor(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    strategyObjectiveService.deleteKeySuccessFactor(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }
        render res as JSON
    }
}
