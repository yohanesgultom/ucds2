package com.ucds.core.business

import grails.converters.JSON

class TrainingMethodController {

    def trainingMethodService

    def getListOfMap() {
        def res = [:]
        TrainingMethod.findAll()?.each() {
            res[it.id] = it.name
        }
        render res as JSON
    }

    def getList() {
        def res = trainingMethodService.getList(params)
        render res as JSON
    }

    def getListForAutocomplete = {
        def res = []
        TrainingMethod.findAll()?.each() {
            res << [id: it.id, label: it.name, value: it.name]
        }
        render res as JSON   
    }

    def edit = {
        def res = [:]
        switch(params.oper){
            case "add" :
                try {
                    trainingMethodService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "edit" :
                try {
                    trainingMethodService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "del" :
                try {
                    trainingMethodService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
        }

        render res as JSON
    }

    def index() {

    }
}
