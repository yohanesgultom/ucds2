package com.ucds.core.business

import grails.converters.JSON

class TrainingMethodQuestionController {

    def trainingMethodQuestionService

    def getList() {
        def res = trainingMethodQuestionService.getList(params)
        render res as JSON
    }

    def getActiveList() {
        def res = TrainingMethodQuestion.findAllByActive(true)
        render res as JSON
    }

//    def getMethodsByQuestion() {
//        def methods = new HashSet<String>()
//        if (params.questionIds) {
//            params.questionIds.split(', ').collect({
//                def question = TrainingMethodQuestion.findById(Integer.parseInt(it))
//                if (question && question.trainingMethods) {
//                    for (TrainingMethod method : question.trainingMethods) {
//                        methods.add(method.name)
//                    }
//                }
//            })
//        }
//        def res = [methods:methods]
//        render res as JSON
//    }

    def getMethodsByQuestion() {
        def methodMap = new HashMap<String, Integer>()
        if (params.questionIds) {
            params.questionIds.split(', ').collect({
                def question = TrainingMethodQuestion.findById(Integer.parseInt(it))
                if (question && question.trainingMethods) {
                    for (TrainingMethod method : question.trainingMethods) {
                        int count = methodMap.get(method.name) ? methodMap.get(method.name) : 0
                        methodMap.put(method.name, count + 1)
                    }
                }
            })
        }
        def methods = methodMap.sort { -it.value }.keySet()
        def res = [methods:methods]
        render res as JSON
    }


    def index() {

    }

    def edit = {
        def res = [:]
        switch(params.oper){
            case "add" :
                try {
                    trainingMethodQuestionService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "edit" :
                try {
                    trainingMethodQuestionService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "del" :
                try {
                    trainingMethodQuestionService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
