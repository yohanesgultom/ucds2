package com.ucds.core.business

import grails.converters.JSON
import pl.touk.excel.export.WebXlsxExporter

class TrainingProgramController {

    def trainingProgramService
    def grailsApplication

    def index() {}

    def getList = {
        def res = trainingProgramService.getList(params)
        render res as JSON
    }

    def getFilteredList = {
        params._search = true // force reuse search mechanism
        // type checking
        switch(params.searchField) {
            case "trainingPlan.id" :
                params.searchString = (params.searchString instanceof String) ? Long.parseLong(params.searchString) : params.searchString;
                break;
        }
        def res = trainingProgramService.getList(params)
        render res as JSON
    }

    def getDetailsList = {
        def res = trainingProgramService.getDetailsList(params)
        render res as JSON
    }

    def exportToExcel = {
        Date period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
        def trainingPlan = TrainingPlan.findByPeriod(period)
        List<TrainingProgram> trainingPrograms = (trainingPlan) ? TrainingProgram.findAllByTrainingPlan(trainingPlan) : []
        def headers = [
                'Name': 'personCompetency.person.fullName',
                'Position': 'personCompetency.person.personPosition.name',
                'Competency': 'personCompetency.competency.name',
                'Actual': 'personCompetency.actualScore',
                'Expected': 'personCompetency.expectedScore',
                'Training Level': 'competencyLevel',
                'Training Method': 'trainingMethod.name',
                'Training Start': 'startDate',
                'Training End': 'endDate',
                'Agent of Change': 'trainerOrPIC',
                'Cost': 'cost'
        ]
        def fileName = "${g.message(code: 'TRAINING_PROGRAMS')} ${period.format('MMM-yyyy')}.xlsx"
        new WebXlsxExporter().with {
            setResponseHeaders(response, fileName)
            fillHeader(headers.keySet().toList())
            add(trainingPrograms, headers.values().toList())
            save(response.outputStream)
        }
    }

    def edit = {
        def res = [:]
        try {
            switch (params.oper) {
                case "edit":
                    try {
                        res.description = trainingProgramService.edit(params)
                        res.message = "Edit Success"
                    } catch (e) {
                        log.error(e.message, e)
                        res.message = e.message ?: e.cause?.message
                    }
                    break
                case "del":
                    try {
                        res.description = trainingProgramService.delete(params)
                        res.message = "Delete Success"
                    } catch (e) {
                        log.error(e.message, e)
                        res.message = e.message ?: e.cause?.message
                    }
                    break
            }
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

}
