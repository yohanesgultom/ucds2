package com.ucds.core.security

import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import org.apache.shiro.crypto.hash.Sha256Hash

class ShiroUserController {

    def shiroUserService

    def index() { }

    def getList = {
        def res = shiroUserService.getList(params)
        render res as JSON
    }

    def edit = {
        def res = [:]
        switch(params.oper){
            case "add" :
                try {
                    shiroUserService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "edit" :
                try {
                    shiroUserService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "del" :
                try {
                    shiroUserService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
        }

        render res as JSON
    }

    def changePassword = {
        if (request.method == 'POST') {
            def res = [message: '', error: true]
            def user = SecurityUtils.subject.principals.oneByType(ShiroUser)
            user = ShiroUser.findById(user.id)
            if (user.passwordHash == new Sha256Hash(params.currentPassword).toHex()) {
                if (params.newPassword && params.newPassword == params.confirmNewPassword) {
                    if (params.newPassword != params.currentPassword) {
                        user.passwordHash = new Sha256Hash(params.newPassword).toHex()
                        user.save(failOnError: true)
                        res.message = message(code:'default.passwordchangedsuccess.message', default: 'Password changed successfully')
                        res.error = false
                    } else {
                        res.message = message(code:'default.nopasswordchange.message', default:  'New password is the same as current password')
                    }
                } else {
                    res.message = message(code:'default.passwordnotmatch.message', default:  'Confirmation password does not match')
                }
            } else {
                res.message = message(code:'default.invalidpassword.message', default: 'Invalid password')
            }
            render res as JSON
        }
    }
}
