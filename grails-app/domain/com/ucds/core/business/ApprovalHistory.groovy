package com.ucds.core.business

class ApprovalHistory {
    String status
    String requester
    String approver
    Date dateCreated
    static belongsTo = [approval:Approval]
    static constraints = {
        status blank: false
        requester nullable: true
        approver nullable: true
    }
    static mapping = {
        cache false
    }
}
