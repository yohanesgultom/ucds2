package com.ucds.core.business

class CompetencyPoint {
    int value = 0
    String note
    Date dateStart
    Date dateEnd
    Date dateCreated
    Date lastUpdated
    String evaluator
    static belongsTo = [personCompetency:PersonCompetency]
    static constraints = {
        evaluator blank: false
        dateStart nullable: true
        dateEnd nullable: true
        note nullable: true
        dateStart validator: { val, obj -> val?.before(obj.dateEnd) || val?.equals(obj.dateEnd) }
        dateEnd validator: { val, obj -> val?.after(obj.dateStart)  || val?.equals(obj.dateStart) }
    }

    def beforeInsert() {
        if (!value) {
            value = 1
        }
    }

    def beforeUpdate() {
        beforeInsert()
    }
}
