package com.ucds.core.business

class Holiday {
    int month
    int year
    int number
    String note
    static constraints = {
        note nullable: true
        month unique: 'year'
    }
    static mapping = {
        cache true
    }
}
