package com.ucds.core.business

class Kpi {
    String position
    String name
    Date period
    String unit
    double weight = 0
    double target = 0
    double actual = 0
    double score = 0
    double result = 0
    String tools
    int reminderInDays = 0
    double percentage = 0
    boolean locked
    static belongsTo = [person:Person]
    static hasOne = [keySuccessFactor:KeySuccessFactor]
    static constraints = {
        unit(nullable: true)
        tools(nullable: true)
        keySuccessFactor(nullable: true)
    }
    static mapping = {
        cache false
    }
}
