package com.ucds.core.business

class Person implements Serializable {
    static Genders = [Male:"M", Female:"F"]
    String employeeNumber
    String level
    String section
    String firstName
    String lastName
    String gender
    Date dateOfBirth
    String email
    String skype
    boolean active = true
    Date dateCreated
    Date lastUpdated
    static hasMany = [
            personCompetencies:PersonCompetency,
            requiredCompetencies:Competency,
            keySuccessFactors:KeySuccessFactor,
            subordinates:Person,
            trainingPrograms:TrainingProgram
    ]
    static hasOne = [personPosition:PersonPosition]
    static constraints = {
        employeeNumber nullable: true
        firstName blank: false
        lastName nullable: true
        level nullable: true
        section nullable: true
        personPosition nullable: true
        subordinates nullable: true
        email email: true
        skype nullable: true
    }
    static mapping = {
        cache false
        subordinates joinTable: [name: 'person_subordinates', key: 'supervisor_id', column: 'person_id']
    }

    public String getFullName() {
        def fullName = this.firstName
        if (this.lastName) {
            fullName += ' ' + this.lastName
        }
        return fullName
    }
}
