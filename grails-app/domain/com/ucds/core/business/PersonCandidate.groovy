package com.ucds.core.business

class PersonCandidate {
    static Status = [New:"NEW", Evaluated:"EVALUATED", Approved:"APPROVED", Rejected:"REJECTED"]
    String name
    String gender
    Date dateOfBirth
    String level
    String section
    Integer totalGap
    static hasMany = [
        candidateCompetencies:PersonCandidateCompetency,
        requiredCompetencies:Competency
    ]
    static hasOne = [personPosition:PersonPosition]
    static constraints = {
        level nullable: true
        personPosition nullable: true
        section nullable: true
        totalGap nullable: true
    }
    static mapping = {
        cache false
    }
}
