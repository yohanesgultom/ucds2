package com.ucds.core.business

class PersonCompetency {
    final static Result = [NOT_AVAILABLE:"Not Available", FAR_BELOW: "Far Below", BELOW:"Below", SATISFY:"Satisfy", ABOVE:"Above", FAR_ABOVE:"Far Above"]
    Integer expectedScore
    Integer actualScore
    String expectedIndicator
    String actualIndicator
    Integer gap
    Date period
    String evaluator
    String approval
    int status = 0
    static transients = [ 'expectedIndicator','actualIndicator' ]
    static belongsTo = [ competency:Competency, person:Person ]
    static hasMany = [ competencyPoints:CompetencyPoint, trainingPrograms:TrainingProgram ]
    static constraints = {
        expectedScore(nullable: true)
        actualScore(nullable: true)
        gap(nullable: true)
        evaluator(nullable: true)
        approval(nullable: true)
        period(nullable: false)
        trainingPrograms(nullable: true)
        competencyPoints(nullable: true)
        competency(unique: ['person', 'period'])
    }
    static mapping = {
        cache false
        competency lazy: false
    }
}
