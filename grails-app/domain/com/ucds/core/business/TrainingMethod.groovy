package com.ucds.core.business

class TrainingMethod {
    String name
    String description
    static constraints = {
        name (blank: false, unique: true)
        description (nullable: true)
    }
    static mapping = {
        description type: 'text'
        cache false
    }
}
