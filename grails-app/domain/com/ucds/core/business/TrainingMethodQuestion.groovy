package com.ucds.core.business

class TrainingMethodQuestion {
    String value
    boolean active = true
    static hasMany = [trainingMethods: TrainingMethod]
    static constraints = {
        value (blank: false)
    }
    static mapping = {
        value type: 'text'
        cache false
    }
}
