package com.ucds.core.business

class TrainingProgram {
    Date startDate
    Date endDate
    String note
    String trainingMethodDisplay
    String trainingMethodDescDisplay
    Integer competencyLevel
    TrainingMethod trainingMethod
    Long cost
    String reminderDayMinus
    static transients = ['trainingMethodDisplay','trainingMethodDescDisplay']
    static belongsTo = [trainingPlan: TrainingPlan, personCompetency:PersonCompetency, trainerOrPIC:Person]
    static constraints = {
        personCompetency nullable: false
        startDate nullable: true
        endDate nullable: true
        note nullable: true
        competencyLevel nullable: true
        trainingPlan nullable: true
        trainingMethod nullable: true
        cost nullable: true
        reminderDayMinus nullable: true
        trainerOrPIC nullable: true
    }
    static mapping = {
        note type: 'text'
        cache false
    }
}
