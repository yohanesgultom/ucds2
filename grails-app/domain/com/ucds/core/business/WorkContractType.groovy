package com.ucds.core.business

class WorkContractType {
    String name
    String description
    float averageDailyWorkHours = 8f
    float annualLeaveDays = 12f
    static constraints = {
        name nullable: false, unique: true
        description nullable: true
    }
}
