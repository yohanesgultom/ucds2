package com.ucds.core.business

class Workload {
    static WORKLOAD_CRITERIA = [VERY_HIGH:"A", HIGH:"B", MEDIUM:"C",LOW:"D",VERY_LOW:"E"]
    Date period
    String position
    String section
    String summary
    String criteria
    float workload = 0f
    float effectiveness = 0f
    float manpowerRequired = 0f
    float manpowerAvailable = 1f
    float dailyWorkingTime = 0f
    float weeklyWorkingTime = 0f
    float monthlyWorkingTime = 0f
    float annualWorkingTime = 0f
    static hasMany = [workloadItems:WorkloadItem]
    static belongsTo = [person:Person]
    static constraints = {
        section(nullable: true)
        summary(nullable: true)
        workloadItems(nullable: true)
        criteria(nullable: true)
    }
    static mapping = {
        cache false
    }
    float getUnitWorkload(String unitType) {
        float res = 0f
        if (WorkloadItem.UNIT.DAILY.equals(unitType)) {
            res = this.dailyWorkingTime
        } else if (WorkloadItem.UNIT.WEEKLY.equals(unitType)) {
            res = this.weeklyWorkingTime
        } else if (WorkloadItem.UNIT.MONTHLY.equals(unitType)) {
            res = this.monthlyWorkingTime
        } else if (WorkloadItem.UNIT.ANNUALLY.equals(unitType)) {
            res = this.annualWorkingTime
        }
        return res
    }

    def recalculate() {
        float totalWorkloadDuration = 0f
        float totalEffectiveWorkDuration = 0f
        float totalManpowerRequired = 0f
        workloadItems?.each {
            totalWorkloadDuration += it.workloadDuration
            totalEffectiveWorkDuration += it.effectiveWorkloadDuration
            totalManpowerRequired += it.manpowerRequired
        }
        workload = totalEffectiveWorkDuration
        effectiveness = workload / (manpowerAvailable * annualWorkingTime) * 100
        manpowerRequired = totalManpowerRequired
        if (effectiveness > 100) {
            criteria = Workload.WORKLOAD_CRITERIA.VERY_HIGH
        } else if (effectiveness > 90) {
            criteria = Workload.WORKLOAD_CRITERIA.HIGH
        } else if (effectiveness > 70) {
            criteria = Workload.WORKLOAD_CRITERIA.MEDIUM
        } else if (effectiveness > 50) {
            criteria = Workload.WORKLOAD_CRITERIA.LOW
        } else {
            criteria = Workload.WORKLOAD_CRITERIA.VERY_LOW
        }
    }
}
