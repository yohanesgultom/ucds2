package com.ucds.core.security

import com.ucds.core.business.Person
import org.codehaus.groovy.grails.web.json.JSONObject

class ShiroUser implements Serializable {
    String username
    String passwordHash
    static hasMany = [ roles: ShiroRole, permissions: String ]
    static belongsTo = [ person: Person ]
    static constraints = {
        username nullable: false, blank: false, unique: true
        person nullable: true
    }
    static fetchMode = [person: 'eager']
    static mapping = {
        cache true
    }
}
