package com.ucds.core


class SystemParameterService {

    def gridUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(SystemParameter, params)
            def rows = []
            ((List<SystemParameter>) (res?.rows))?.each {
                // hide password
                String value = it.value
                if (it.code.toLowerCase().contains("password")) {
                    value = '*' * value.length()
                }
                rows << [
                        id: it.id,
                        cell: [
                                it.code,
                                value,
                                it.inactive
                        ]
                ]
            }

            ret = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def systemParameter = new SystemParameter()

        systemParameter.code = params.code
        systemParameter.inactive = Boolean.parseBoolean(params.inactive)
        systemParameter.value = params.value

        systemParameter.save()
        if (systemParameter.hasErrors()) {
            throw new Exception("${systemParameter.errors}")
        }
    }

    def edit(params) {
        def systemParameter = SystemParameter.findById(params.id)
        if (systemParameter) {

            systemParameter.code = params.code
            systemParameter.inactive = Boolean.parseBoolean(params.inactive)
            systemParameter.value = params.value

            systemParameter.save()
            if (systemParameter.hasErrors()) {
                throw new Exception("${systemParameter.errors}")
            }
        } else {
            throw new Exception("SystemParameter not found")
        }
    }

    def delete(params) {
        def systemParameter = SystemParameter.findById(params.id)
        if (systemParameter) {
            systemParameter.delete()
        } else {
            throw new Exception("SystemParameter not found")
        }
    }

}