package com.ucds.core.business

class CompetencyPointService {

    def gridUtilService
    def grailsApplication

    def getList(def params) {
        def ret = null

        try {
            def res = this.getDomainList(params)
            def rows = []
            ((List<CompetencyPoint>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.personCompetency?.id,
                                it.dateStart,
                                it.dateEnd,
                                it.evaluator,
                                it.note
                        ]
                ]
            }

            ret = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def obj = new CompetencyPoint()
        obj.personCompetency = PersonCompetency.findById(Integer.parseInt(params.competency))
        try {
            obj.value = Integer.parseInt(params.value)
        } catch (Exception e) {
            // let value = null
        }
        obj.note = params.note
        obj.evaluator = params.evaluator
        obj.dateStart = Date.parse("dd/MM/yyyy", params.dateStart)
        obj.dateEnd = Date.parse("dd/MM/yyyy", params.dateEnd)
        obj.save()
        if (obj.hasErrors()) {
            def msg = []
            obj.errors.allErrors.each {
                msg << it.defaultMessage
            }
            throw new Exception(msg.join('\n'))
        }
    }

    def edit(params) {
        def obj = CompetencyPoint.findById(params.id)
        if (obj) {
            try {
                obj.value = Integer.parseInt(params.value)
            } catch (Exception e) {
                // let value = null
            }
            obj.note = params.note
            obj.evaluator = params.evaluator
            obj.dateStart = Date.parse("dd/MM/yyyy", params.dateStart)
            obj.dateEnd = Date.parse("dd/MM/yyyy", params.dateEnd)
            obj.save()
            if (obj.hasErrors()) {
                throw new Exception("${obj.errors}")
            }
        } else {
            throw new Exception("CompetencyPoint not found")
        }
    }

    def delete(params) {
        def CompetencyPoint = CompetencyPoint.findById(params.id)
        if (CompetencyPoint) {
            CompetencyPoint.delete()
        } else {
            throw new Exception("CompetencyPoint not found")
        }
    }

    def getDomainList(def params){
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        def rows = []
        int totalRecords = 0
        int totalPage = 0

        if (params.personId && params.period) {
            def personId = Long.parseLong(params.personId)
            def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)

            String query = "from CompetencyPoint where personCompetency.person.id = :personId and personCompetency.period = :period"
            String countQuery = "select count(*) from CompetencyPoint where personCompetency.person.id = :personId and personCompetency.period = :period"

            try {
                rows = CompetencyPoint.findAll(query, [personId: personId, period: period], [max: max, offset: offset])
                totalRecords = CompetencyPoint.executeQuery(countQuery, [personId: personId, period: period])?.get(0)
            } catch (e) {
                log.error(e.message, e)
            }

            totalPage = Math.ceil(totalRecords / max)
        }

        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

}
