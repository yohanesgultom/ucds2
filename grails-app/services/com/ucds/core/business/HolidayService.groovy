package com.ucds.core.business

import com.ucds.core.SystemParameter
import grails.plugin.cache.CacheEvict

class HolidayService {

    def gridUtilService
    def appUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(Holiday, params)
            def rows = []

            ((List<Holiday>) (res?.rows))?.each {
                int workingDay = appUtilService.getTotalWorkingDays(it.month, it.year)
                rows << [
                        id: it.id,
                        cell: [
                                it.month,
                                it.year,
                                it.number,
                                workingDay,
                                it.note
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            throw e
        }

        return ret
    }

    @CacheEvict(value = ["TotalWorkingDays", "TotalEffectiveMinutes"], allEntries = true)
    def add(params) throws Exception {
        def holiday = new Holiday(params)
        holiday.save()
        if (holiday.hasErrors()) {
            throw new Exception("${holiday.errors}")
        }
    }

    @CacheEvict(value = ["TotalWorkingDays", "TotalEffectiveMinutes"], allEntries = true)
    def edit(params) {
        def holiday = Holiday.findById(params.id)
        if (holiday) {
            holiday.properties = params
            holiday.save()
            if (holiday.hasErrors()) {
                throw new Exception("${holiday.errors}")
            }
        } else {
            throw new Exception("Holiday not found")
        }
    }

    @CacheEvict(value = ["TotalWorkingDays", "TotalEffectiveMinutes"], allEntries = true)
    def delete(params) {
        def holiday = Holiday.findById(params.id)
        if (holiday) {
            holiday.delete()
        } else {
            throw new Exception("Holiday not found")
        }
    }

}
