package com.ucds.core.business

import grails.plugin.cache.CacheEvict

class KpiService {

    def grailsApplication

    def getEvaluationList(def params) {
        def ret = null

        try {
            def res = this.getKpiList(params)
            def rows = []
            ((List<Kpi>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.position,
                                it.name,
                                it.unit,
                                it.weight,
                                it.target,
                                it.actual,
                                it.score,
                                it.result,
                                it.tools,
                                it.reminderInDays,
                                it.locked
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def getKpiList(def params) {
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max
        def query = "from Kpi where person = :person and period = :period order by percentage desc, name asc"

        def rows = []
        int totalRecords = 0

        if (params.personId) {
            def person = Person.find("from Person person left join fetch person.personPosition pos left join fetch pos.keySuccessFactors ksf where person.id = :personId", [personId: params.long("personId")])
            def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
            if (person && period) {
                def kpis = Kpi.findAll(query, [person: person, period: period], [max: max, offset: offset])
                if (kpis && kpis.size() > 0) {
                    rows = kpis
                } else {
                    // personal kpi
                    if (person.getKeySuccessFactors()) {
                        person.getKeySuccessFactors()?.eachWithIndex { keySuccessFactor, i ->
                            def name = (keySuccessFactor.useKri) ? keySuccessFactor.kri : keySuccessFactor.title
                            new Kpi(name: name, position: person.personPosition?.name, period: period, person: person, percentage: keySuccessFactor.percentage, keySuccessFactor: keySuccessFactor).save(failOnError: true)
                        }
                    }
                    // strategy kpi
                    person.personPosition?.keySuccessFactors?.eachWithIndex { keySuccessFactor, i ->
                        def actual = this.getScoreFromChildren(keySuccessFactor, period)
                        def target = (actual.score > 0) ? 100.0 : 0.0
                        boolean locked = (actual.count > 0)
                        new Kpi(name: keySuccessFactor.title, position: person.personPosition?.name, period: period, person: person, target: target, actual: actual.score, locked: locked, keySuccessFactor: keySuccessFactor).save(failOnError: true)
                    }
                    // re-query from database
                    rows = Kpi.findAll(query, [person: person, period: period], [max: max, offset: offset])
                }
                totalRecords = rows.size()
            }
        }
        int totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    def clearKpiList(def params) {
        long personId = params.long("personId")
        def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
        Kpi.executeUpdate("delete Kpi kpi where kpi.person.id = :personId and kpi.period = :period", [personId:personId,period:period])
    }

    @CacheEvict(value = ['StrategyObjectiveGraph', 'UnderTargetKeySuccessFactors'], allEntries = true)
    def edit(params) {
        def kpi = Kpi.findById(params.id)
        if (kpi) {
            kpi.unit = params.unitWorkload
            kpi.weight = (params.weight) ? Double.parseDouble(params.weight) : 0
            kpi.target = (params.target) ? Double.parseDouble(params.target) : 0
            kpi.actual = (params.actual) ? Double.parseDouble(params.actual) : 0
            kpi.tools = params.tools
            kpi.reminderInDays = (params.reminderInDays) ? Integer.parseInt(params.reminderInDays) : 0
            kpi.score = (kpi.target > 0) ? (kpi.actual / kpi.target * 100) : 0
            kpi.result = kpi.score * kpi.weight / 100

            kpi.save()
            if (kpi.hasErrors()) {
                throw new Exception("${kpi.errors}")
            }

            return [score: kpi.score, result: kpi.result]

        } else {
            throw new Exception("Kpi not found")
        }
    }

    private Map getScoreFromChildren(KeySuccessFactor parent, Date date) {
        def countRes = KeySuccessFactor.executeQuery("select count(k) from KeySuccessFactor k where k.parent = :parent", [parent:parent])
        def avgRes = Kpi.executeQuery("select avg(k.score) from Kpi k where k.keySuccessFactor.parent = :parent and period = :period", [parent: parent, period: date])
        Double count = (countRes.size() > 0 && countRes[0]) ? countRes[0] as Double : 0.0
        Double score = (avgRes.size() > 0 && avgRes[0]) ? avgRes[0] as Double : 0.0
        return [score:score,count:count]
    }
}
