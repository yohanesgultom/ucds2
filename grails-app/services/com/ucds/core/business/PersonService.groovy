package com.ucds.core.business

import com.ucds.core.security.ShiroUser
import grails.converters.JSON
import grails.plugin.cache.CacheEvict
import grails.plugin.cache.Cacheable
import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

class PersonService {

    def gridUtilService

    def getList(Map params) {
        def ret = null
        try {
            def res = gridUtilService.getDomainList(Person, params)
            def rows = []
            ((List<Person>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.firstName,
                                it.lastName,
                                it.gender,
                                it.dateOfBirth,
                                it.level,
                                it.personPosition?.id,
                                it.section,
                                it.email,
                                it.skype,
                                it.subordinates*.fullName.join(", "),
                                it.active
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            throw e
        }

        return ret
    }

    private void overrideParams(params) {
        params._search = "true"
        if (params.filters) {
            def filters = JSON.parse(params.filters)
            filters.rules << new JSONObject([field: "active", op: "eq", data: true])
            params.filters = filters.toString()
        } else if (params.searchField && params.searchOper && params.searchString) {
            def filters = JSON.parse("{groupOp: 'and', rules:[]}")
            filters.rules << new JSONObject([field: params.searchField, op: params.searchOper, data: params.searchString])
            filters.rules << new JSONObject([field: "active", op: "eq", data: true])
            params.filters = filters.toString()
            params.remove('searchField')
            params.remove('searchOper')
        } else {
            params.searchField = "active"
            params.searchOper = "eq"
            params.searchString = "true"
        }
    }

    @CacheEvict(value = ['EvaluationList'], allEntries = true)
    add(params) throws Exception {
        def person = new Person()
        def position = PersonPosition.findById(params.position)
        if (position) {
            this.updatePerson(params, person, position)
        } else {
            throw new Exception("Position not found")
        }
        if (person.hasErrors()) {
            throw new Exception("${person.errors}")
        }
    }

    @CacheEvict(value = ['EvaluationList'], allEntries = true)
    edit(params) {
        def person = Person.findById(params.id)
        def position = PersonPosition.findById(params.position)
        if (person && position) {
            this.updatePerson(params, person, position)
        } else {
            throw new Exception("Person not found")
        }
    }

    private void updatePerson(params, Person person, PersonPosition position) {
        person.dateOfBirth = Date.parse("dd/MM/yyyy", params.dateOfBirth)
        person.firstName = params.firstName
        person.gender = params.gender
        person.lastName = params.lastName
        person.section = params.section
        person.level = params.level
        person.email = params.email
        person.skype = params.skype
        person.active = 'true'.equalsIgnoreCase(params.active)
        //person.position = params.position
        person.personPosition = position
        // clear all subordinates
        person.subordinates?.clear()
        if (params.subordinates) {
            // add new ones
            params.subordinates?.split(",")?.each {
                it = StringUtils.trim(it)
                if (it) {
                    def names = it.split(' ')
                    def sub
                    if (names.length > 1) {
                        sub = Person.executeQuery("from Person where concat(firstName, ' ', lastName) = ?", [it])?.first()
                    } else {
                        sub = Person.executeQuery("from Person where firstName = ?", [it])?.first()
                    }

                    if (!sub) {
                        throw new Exception("Can not find subordinate: ${it}")
                    } else {
                        person.addToSubordinates(sub)
                    }
                }
            }
        }
        person.save(failOnError: true)
    }

    @CacheEvict(value = ['EvaluationList'], allEntries = true)
    delete(params) {
        def person = Person.findById(params.id)
        if (person) {
            try {
                person.delete(flush: true)
            } catch (Exception e) {
                throw new Exception("Person can not be deleted permanently due to existing evaluation data")
            }
        } else {
            throw new Exception("Person not found")
        }
    }

    @Cacheable(value='EvaluationList')
    getEvaluationList(params) {
        def ret = null

        try {
            this.overrideParams(params)
            def res = gridUtilService.getDomainList(Person, params)
            def rows = []
            ((List<Person>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.firstName,
                                it.lastName,
                                it.level,
                                it.personPosition?.id,
                                it.skype,
                                it.section
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def addRequiredCompetency(params) {
        def ret = null
        Person person = Person.findById(params.personId)
        Competency competency = Competency.findById(params.competencyId)
        if (person) {
            if (competency) {
                person.addToRequiredCompetencies(competency)
                person.save()
            } else {
                throw new Exception("Invalid competency")
            }
        } else {
            throw new Exception("Invalid person")
        }
        return ret
    }

    def removeRequiredCompetency(params) {
        def ret = null
        Person person = Person.findById(params.personId)
        Competency competency = Competency.findById(params.competencyId)
        if (person) {
            if (competency) {
                person.removeFromRequiredCompetencies(competency)
                person.save()
            } else {
                throw new Exception("Invalid competency")
            }
        } else {
            throw new Exception("Invalid person")
        }
        return ret
    }

    def getPositionList(params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(PersonPosition, params)
            def rows = []
            def counter = 0
            ((List<PersonPosition>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def addPosition(params) {
        def p = new PersonPosition()
        p.name = params.name
        p.save()
        if (p.hasErrors()) {
            throw new Exception("${p.errors}")
        }
    }

    def editPosition(params) {
        def p = PersonPosition.findById(params.id)
        if (p) {
            p.name = params.name
            p.save()
            if (p.hasErrors()) {
                throw new Exception("${p.errors}")
            }
        } else {
            throw new Exception("Person position not found")
        }
    }

    def deletePosition(params) {
        def p = PersonPosition.findById(params.id)
        if (p) {
            p.delete()
        } else {
            throw new Exception("Person position is not found")
        }
    }

    def getTraineeList(GrailsParameterMap params) {
        String sort = params.sidx && "" != params.sidx ? params.sidx : "fullName"
        String order = params.sord && "" != params.sord ? params.sord : "asc"
        int max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        int offset = ((params.page ? params.int('page') : 1) - 1) * max

        def personId = ShiroUser.findById(params.userId)?.person?.id
        Map res = [rows: [], records: 0, page: 0, total: 0]
        if (personId) {
            String from = """
            from TrainingProgram tp
                join tp.personCompetency pc
                join tp.trainingMethod tm
                join pc.person trainee
                join trainee.personPosition pos
                join pc.competency c
                join c.competencyLevels cl
            where
                tp.trainerOrPIC.id = :personId
                and pc.period = :period
                and cl.level = pc.expectedScore
            """
            String select = """
            select
                concat(trainee.firstName, ' ', trainee.lastName) as fullName,
                pos.name as position,
                trainee.section,
                c.name as competency,
                pc.expectedScore as competencyLevel,
                cl.keyIndicator,
                tm.name as trainingMethod,
                tm.description as trainingMethodDescription,
                tp.startDate,
                tp.endDate,
                trainee.email,
                trainee.skype
            """
            String count = "select count(distinct tp)"
            String orderBy = "order by ${sort} ${order}"
            res.rows = Person.executeQuery(select + from + orderBy, [personId: personId, period: params.period], [max:max,offset:offset])
            res.records = Person.executeQuery(count + from, [personId: personId, period: params.period])?.get(0)
            res.total = Math.ceil(res.records / max)
            res.page = params.page
        }
        return res
    }
}