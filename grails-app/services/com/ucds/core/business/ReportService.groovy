package com.ucds.core.business

import grails.converters.JSON
import grails.plugin.cache.Cacheable

class ReportService {

    def gridUtilService

    @Cacheable(value='QualityDiagram')
    def getQualityDiagram(Map params) {
        def res = []
        def selectClause = """
        select distinct p
        from Person as p
            left join fetch p.personCompetencies as pc
            left join fetch pc.competency as c
            join fetch p.personPosition as pp
         """
        def whereClause = " where (p.active = true or p.lastUpdated >= :period)"
        def orderClause = " order by pp.id asc, p.firstName asc"
        def whereArray = []
        ((Map)params.filters)?.each {
            whereArray << "${it.key} = '${it.value}'"
        }
        whereClause += (whereArray.size() > 0) ? " and " + whereArray.join(" and ") : ""
        def query = selectClause + whereClause + orderClause
        def people = Person.executeQuery(query, [period: params.period]);
        def types = Competency.Types.values().toArray()
        def countFarBelow = [0,0,0]
        def countBelow = [0,0,0]
        def countSatisfy = [0,0,0]
        def countAbove = [0,0,0]
        def countFarAbove = [0,0,0]
        def countNotAvailable = [0,0,0]
        for(Person person : people) {
            for(int i = 0; i < types.size(); i++) {
                def quality = this.getPersonCompetencyQuality(person, types[i], params.period)
                if (PersonCompetency.Result.FAR_BELOW == quality) {
                    countFarBelow[i]++
                } else if (PersonCompetency.Result.BELOW == quality) {
                    countBelow[i]++
                } else if (PersonCompetency.Result.SATISFY == quality) {
                    countSatisfy[i]++
                } else if (PersonCompetency.Result.ABOVE == quality) {
                    countAbove[i]++
                } else if (PersonCompetency.Result.FAR_ABOVE == quality) {
                    countFarAbove[i]++
                } else  {
                    countNotAvailable[i]++
                }
            }
        }
        for(int i = 0; i < types.size(); i++) res << [countFarBelow[i],countBelow[i],countSatisfy[i],countAbove[i],countFarAbove[i],countNotAvailable[i]]
        return res
    }

    private def getPersonCompetencyQuality(Person person, String type, Date period) {
        def res = PersonCompetency.Result.NOT_AVAILABLE
        if (person && person.personCompetencies) {
            def total = null
            for (PersonCompetency personCompetency : person.personCompetencies) {
                if (period == personCompetency.period
                        && type.equalsIgnoreCase(personCompetency.competency.type)
                        && personCompetency.gap != null) {
                    total = (total) ? total + personCompetency.gap : personCompetency.gap
                }
            }

            int top = person.personCompetencies.size()
            int bottom = person.personCompetencies.size() * (-1)
            if (total > top) {
                res = PersonCompetency.Result.FAR_ABOVE
            } else if (total > 0) {
                res = PersonCompetency.Result.ABOVE
            } else if (total == 0) {
                res = PersonCompetency.Result.SATISFY
            } else if (total >= bottom) {
                res = PersonCompetency.Result.BELOW
            } else if (total) {
                res = PersonCompetency.Result.FAR_BELOW
            }
        }
        return res
    }

    @Cacheable(value='CompetencyDiagram')
    def getCompetencyDiagram(Map params) {
        def selectClause = """
            select distinct p
            from Person as p
                join fetch p.personCompetencies as pc
                join fetch pc.competency as c
                join fetch p.personPosition as pp
            """
        def whereClause = " where (p.active = true or p.lastUpdated >= :period)"
        def orderClause = " order by pp.id asc, p.firstName asc"
        def whereArray = []
        ((Map)params.filters)?.each {
            whereArray << "${it.key} = '${it.value}'"
        }
        whereClause += (whereArray.size() > 0) ? " and " + whereArray.join(" and ") : ""
        def query = selectClause + whereClause + orderClause
        def people = Person.executeQuery(query, [period: params.period]);
        def competencies = Competency.all;
        def competencyNames = []
        def scores = []
        // init
        for (int j = 0; j < competencies.size(); j++) {
            competencyNames << ((Competency) competencies[j]).name
            scores << 0
        }
        for(Person person : people) {
            if (person.personCompetencies) {
                for (PersonCompetency personCompetency : person.personCompetencies) {
                    // only display negatives gap
                    if (params.period == personCompetency.period
                            && personCompetency.gap && personCompetency.gap < 0) {
                        def i = 0
                        def found = false
                        while (i < competencies.size() && !found) {
                            if (personCompetency.competency.id == ((Competency)competencies[i]).id) {
                                scores[i] += personCompetency.gap
                                found = true
                            }
                            i++;
                        }
                    }
                }
            }
        }
        return [competencies: competencyNames, scores: scores]
    }

    @Cacheable(value='CompetencyMatrixData')
    def getCompetencyMatrixData(Map params) {
        def res = [:]
        def selectClause = """
        select
            distinct p
        from Person as p
            left join fetch p.personCompetencies as pc
            left join fetch p.personPosition as pp
        """
        def whereClause = " where (p.active = true or p.lastUpdated >= :period)"
        def orderClause = " order by pp.id asc, p.firstName asc"
        def whereArray = []
        ((Map)params.filters)?.each {
            whereArray << "${it.key} = '${it.value}'"
        }
        whereClause += (whereArray.size() > 0) ? " and " + whereArray.join(" and ") : ""
        def query = selectClause + whereClause + orderClause
        def people = Person.executeQuery(query, [period: params.period]);
        def competencies = Competency.findAll("from Competency as competency left join fetch competency.competencyLevels order by competency.id asc");
        if (competencies && people) {
            // matrix header
            def header = [:]
            for (Competency com : competencies) {
                int level = (com.competencyLevels) ? com.competencyLevels.size() : 1
                header[com.id] = [competencyName: com.name, competencyLevel: level, competencyType: com.type]
            }
            res["header"] = header

            // matrix rows
            def rows = []
            for (Person person : people) {
                def competencyScore = [:]
                if (person.personCompetencies) {
                    for (PersonCompetency pCom : person.personCompetencies) {
                        if (params.period == pCom.period) {
                            competencyScore[pCom.competency.id] = [actual: pCom.actualScore, expected: pCom.expectedScore, gap: pCom.gap]
                        }
                    }
                }
                rows << [personName: person.fullName, personPosition: person.personPosition?.name, competencyScore: competencyScore]
            }
            res["rows"] = rows
        }
        return res
    }

    def getAnalyticalMatrixData(Map params) {
        return getCompetencyMatrixData(params)
    }

    private void createGraph(StrategyObjective node, Map graph, int level, Date period) {
        // take care nodes
        def res = Kpi.executeQuery("select avg(k.score) from Kpi k where k.keySuccessFactor.strategyObjective = :strategyObjective and k.keySuccessFactor.parent is null and k.period = :period", [strategyObjective: node, period: period])
        double actual = (res && res.get(0)) ? res.get(0) as Double : 0
        double target = 100
        def nodeMap = [id: node.id, label: node.name, target: target, actual: actual, warning: this.isWarning(actual, target)]
        if (((ArrayList)graph["nodes"]).size() <= level) {
            // if there is no node for this level
            graph["nodes"] << [nodeMap]
        } else {
            // if there is already at least a node for this level
            boolean exist = false
            // todo optimize
            ((ArrayList)graph["nodes"]).get(level).each {
                if (((Map)it).id == nodeMap.id) {
                    exist = true
                }
            }
            if (!exist) ((ArrayList)graph["nodes"]).get(level) << nodeMap
        }

        // take care edges
        node.dependencies?.each { dependency ->
            graph["edges"] << [source: dependency.strategyObjective.id, target: node.id]
            createGraph(dependency.strategyObjective, graph, level+1, period)
        }
    }

    @Cacheable(value='StrategyObjectiveGraph')
    def getStrategyObjectiveGraph(Map params) {
        def graph = [nodes: [], edges: []]
        def root = StrategyObjective.find("from StrategyObjective as s where s.inactive = false and s.affectees is empty")
        if (root) {
            createGraph(root, graph, 0, params.period)
            // get actual value for the root from the direct children
            double actual = 0
            ((ArrayList)graph.nodes?.get(1))?.each {
                actual += it.actual
            }
            actual = actual / ((ArrayList)graph.nodes?.get(1)).size()
            ((ArrayList)graph.nodes?.get(0))?.get(0).actual = actual
            ((ArrayList)graph.nodes?.get(0))?.get(0).warning = this.isWarning(actual, 100)
        }
        return graph
    }

    private boolean isWarning(double actual, double target) {
        // todo put the rule here
        return (actual / target <= 0.7)
    }

    @Cacheable(value='UnderTargetKeySuccessFactors')
    def getUnderTargetKeySuccessFactors(Map params) {
        def res = []
        StrategyObjective strategyObjective = StrategyObjective.find("from StrategyObjective so join fetch so.keySuccessFactors ksf left join fetch ksf.kpis kpi left join fetch kpi.person left join fetch ksf.responsiblePositions where so.id = :soid and kpi.period = :period order by ksf.parent.id", [soid: params.long('soid'), period: params.period])
        strategyObjective?.keySuccessFactors?.each { ksf ->
            def map = [ksf:[:], responsibles: []]
            ksf.kpis?.each { kpi ->
                if (this.isWarning(kpi.actual, kpi.target)) {
                    map.ksf.id = ksf.id
                    map.ksf.title = ksf.title
                    ksf.responsiblePositions?.each { pos ->
                        def fullname = (kpi.person.lastName) ? kpi.person.firstName + " " + kpi.person.lastName : kpi.person.firstName
                        map.responsibles << [id: pos.id, name: fullname, position: pos.name]
                    }
                }
            }
            res << map
        }
        return res
    }

    @Cacheable(value='CompetencyGrowthList')
    def getCompetencyGrowthList(def params) {
        int max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        int offset = ((params.page ? params.int('page') : 1) - 1) * max
        String orderBy = params.sidx ?: 'p.id'
        String order = params.sord ?: 'asc'

        List rows = []
        int totalRecords = 0
        String whereClause = ''
        ArrayList whereClauseItems = []
        ArrayList filterData = [params.periodBefore, params.periodAfter]
        Map searchFieldTypes = [
            'p.firstName': String.class,
            'p.lastName': String.class,
            'p.level': String.class,
            'pos.name': String.class,
            'p.section': String.class,
            'c.name': String.class,
            'pcb.actualScore': int.class,
            'pca.actualScore': int.class,
            '(pca.actualScore - pcb.actualScore)': int.class
        ]
        try {

            // process filter criteria
            if (params.boolean('_search')) {
                if (params.filters) {
                    def filters = JSON.parse(params.filters)
                    filters.rules.each {
                        gridUtilService.buildWhereClause(
                            searchFieldTypes.get(it.field),
                            it.field,
                            it.op,
                            it.data.toString(),
                            whereClauseItems,
                            filterData
                        )
                    }
                    whereClause = whereClauseItems ? "and ${whereClauseItems.join(" ${filters.get('groupOp')} ")}" : ""
                } else if (params.searchField && params.searchOper && params.searchString) {
                    gridUtilService.buildWhereClause(
                        searchFieldTypes.get(params.searchField),
                        params.searchField,
                        params.searchOper,
                        params.searchString.toString(),
                        whereClauseItems,
                        filterData
                    )
                    whereClause = whereClauseItems ? "and ${whereClauseItems.join(' and ')}" : ""
                }
            }

            String from = """
            from PersonCompetency pcb, PersonCompetency pca, Person p, Competency c, PersonPosition pos
            where
                pcb.id != pca.id
                and pca.competency.id = pcb.competency.id
                and pca.person.id = pcb.person.id
                and pcb.period = ?
                and pca.period = ?
                and p.id = pca.person.id
                and c.id = pca.competency.id
                and pos.id = p.personPosition.id
                ${whereClause}
            """
            String query = """
            select
                pca.id as id,
                p.firstName as firstName,
                p.lastName as lastName,
                pos.name as position,
                p.section as section,
                c.name as competencyName,
                pcb.actualScore as actualScoreBefore,
                pca.actualScore as actualScoreAfter,
                (pca.actualScore - pcb.actualScore) as actualScoreGrowth
            ${from}
            order by ${orderBy} ${order}"""
            rows = Person.executeQuery(query, filterData, [max:max,offset:offset])
            String count = "select count(pcb.id) ${from}"
            totalRecords = Person.executeQuery(count, filterData)?.get(0)
        } catch (Exception e) {
            log.error(e.message, e)
        }

        int totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }


}
