package com.ucds.core.business

import grails.plugin.cache.CacheEvict

class StrategyObjectiveService {


    def gridUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(StrategyObjective, params)
            def rows = []
            def counter = 0
            ((List<StrategyObjective>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.perspective?.id,
                                it.inactive
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    @CacheEvict(value=['StrategyObjectiveGraph'], allEntries=true)
    def add(params) throws Exception {
        def strategyObjective = new StrategyObjective()

        strategyObjective.inactive = Boolean.parseBoolean(params.inactive)
        strategyObjective.perspective = BalanceScoreCard.findById(params.perspective)
        strategyObjective.name = params.name

        strategyObjective.save()
        if (strategyObjective.hasErrors()) {
            throw new Exception("${strategyObjective.errors}")
        }
    }

    @CacheEvict(value=['StrategyObjectiveGraph'], allEntries=true)
    def edit(params) {
        def strategyObjective = StrategyObjective.findById(params.id)
        if (strategyObjective) {

            strategyObjective.inactive = Boolean.parseBoolean(params.inactive)
            strategyObjective.perspective = BalanceScoreCard.findById(params.perspective)
            strategyObjective.name = params.name

            strategyObjective.save()
            if (strategyObjective.hasErrors()) {
                throw new Exception("${strategyObjective.errors}")
            }
        } else {
            throw new Exception("StrategyObjective not found")
        }
    }

    @CacheEvict(value=['StrategyObjectiveGraph'], allEntries=true)
    def delete(params) {
        def strategyObjective = StrategyObjective.findById(params.id)
        if (strategyObjective) {
            strategyObjective.delete()
        } else {
            throw new Exception("StrategyObjective not found")
        }
    }

    @CacheEvict(value=['StrategyObjectiveGraph'], allEntries=true)
    def addDependencies(params) {
        def strategyObjective = StrategyObjective.findById(params.strategyObjectiveId)
        def dependency = StrategyObjective.findById(params.dependency)
        // validate
        if (strategyObjective.equals(dependency)) throw new Exception("Self dependency detected")
        if (isCircularDependency(strategyObjective, dependency)) throw new Exception("Circular dependency detected")
        if (strategyObjective && dependency) {
            strategyObjective.addToDependencies(new StrategyObjectiveDependency(strategyObjective: dependency, weight: params.double('weight')))
            strategyObjective.save()
        }else {
            throw new Exception("StrategyObjective not found")
        }
    }

    @CacheEvict(value=['StrategyObjectiveGraph'], allEntries=true)
    def editDependencies(params) {
        def strategyObjective = StrategyObjective.findById(params.strategyObjectiveId)
        def strategyObjectiveDependency = StrategyObjectiveDependency.findById(params.int('id'))
        def dependency = StrategyObjective.findById(params.dependency)
        // validate
        if (strategyObjective.equals(dependency)) throw new Exception("Self dependency detected")
        if (isCircularDependency(strategyObjective, dependency)) throw new Exception("Circular dependency detected")
        if (strategyObjectiveDependency && dependency) {
            strategyObjectiveDependency.weight = params.double('weight')
            strategyObjectiveDependency.strategyObjective = dependency
            strategyObjectiveDependency.save()
        }else {
            throw new Exception("StrategyObjective not found")
        }
    }

    @CacheEvict(value=['StrategyObjectiveGraph'], allEntries=true)
    def deleteDependencies(params) {
        def strategyObjectiveDependency = StrategyObjectiveDependency.findById(params.int('id'))
        if (strategyObjectiveDependency) {
            strategyObjectiveDependency.affectee = null
            strategyObjectiveDependency.strategyObjective = null
            strategyObjectiveDependency.save()
            strategyObjectiveDependency.delete()
        }else {
            throw new Exception("StrategyObjective not found")
        }
    }

    def addKeySuccessFactor(params) {
        def strategyObjective = StrategyObjective.findById(params.strategyObjectiveId)
        def parent = KeySuccessFactor.findById(params.parent)
        def keySuccessFactor = new KeySuccessFactor(title: params.title, strategyObjective: strategyObjective, parent: parent)
        params.responsible?.split(', ').collect({
            def personPosition = PersonPosition.findByName(it)
            if (personPosition) {
                keySuccessFactor.addToResponsiblePositions(personPosition)
            }
        })
        keySuccessFactor.save()
    }

    def editKeySuccessFactor(params) {
        def keySuccessFactor = KeySuccessFactor.findById(params.id)
        if (keySuccessFactor) {
            keySuccessFactor.title = params.title
            keySuccessFactor.parent = (params.parent) ? (KeySuccessFactor.findById(params.parent)) : null
            keySuccessFactor.responsiblePositions.clear()
            params.responsible?.split(', ').collect({
                def personPosition = PersonPosition.findByName(it)
                if (personPosition) {
                    keySuccessFactor.addToResponsiblePositions(personPosition)
                }
            })
            keySuccessFactor.save()
        } else {
            throw new Exception("KeySuccessFactor not found")
        }
    }

    def deleteKeySuccessFactor(params) {
        def strategyObjective = StrategyObjective.findById(params.strategyObjectiveId)
        def keySuccessFactor = KeySuccessFactor.findById(params.id)
        if (keySuccessFactor) {
            strategyObjective.removeFromKeySuccessFactors(keySuccessFactor)
            strategyObjective.save()
            keySuccessFactor.delete()
        } else {
            throw new Exception("KeySuccessFactor not found")
        }
    }

    private boolean isCircularDependency(StrategyObjective parent, StrategyObjective child) {
        boolean circular = false
        for (StrategyObjectiveDependency it in parent.affectees) {
            if (it.affectee.id.equals(child.id)) {
                circular = true
                break
            }
        }
        return circular
    }
}
