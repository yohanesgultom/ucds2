package com.ucds.core.business

class TrainingMethodQuestionService {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(TrainingMethodQuestion, params)
            def rows = []
            def counter = 0
            ((List<TrainingMethodQuestion>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                                it.value,
                                it.active,
                                it.trainingMethods*.name.join(', ')
                        ]
                ]
            }

            ret = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]

        } catch (e) {
            log.error(e.message, e)
        }

        return ret
    }

    def add(params) throws Exception{
        def trainingMethodQuestion = new TrainingMethodQuestion()

        trainingMethodQuestion.value = params.value
        trainingMethodQuestion.active = 'true'.equalsIgnoreCase(params.active)
        trainingMethodQuestion.trainingMethods = []
        params.methods?.split(', ').collect {
            def trainingMethod = TrainingMethod.findByName(it)
            if (trainingMethod) {
                trainingMethodQuestion.trainingMethods << trainingMethod       
            }
        }

        trainingMethodQuestion.save()
        if (trainingMethodQuestion.hasErrors()) {
            throw new Exception("${trainingMethodQuestion.errors}")
        }
    }

    def edit(params){
        def trainingMethodQuestion = TrainingMethodQuestion.findById(params.id)
        if (trainingMethodQuestion) {

            trainingMethodQuestion.value = params.value
            trainingMethodQuestion.active = 'true'.equalsIgnoreCase(params.active)
            trainingMethodQuestion.trainingMethods = []
            params.methods?.split(', ').collect {
                def trainingMethod = TrainingMethod.findByName(it)
                if (trainingMethod) {
                    trainingMethodQuestion.trainingMethods << trainingMethod       
                }
            }

            trainingMethodQuestion.save()
            if (trainingMethodQuestion.hasErrors()) {
                throw new Exception("${trainingMethodQuestion.errors}")
            }
        }else{
            throw new Exception("Record not found")
        }
    }

    def delete(params){
        def trainingMethodQuestion = TrainingMethodQuestion.findById(params.id)
        if (trainingMethodQuestion) {
            trainingMethodQuestion.delete()
        }else{
            throw new Exception("Record not found")
        }
    }

}
