package com.ucds.core.business

class TrainingMethodService {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(TrainingMethod, params)
            def rows = []
            def counter = 0
            ((List<TrainingMethod>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                                it.name,
                                it.description
                        ]
                ]
            }

            ret = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]

        } catch (e) {
            log.error(e.message, e)
        }

        return ret
    }

    def add(params) throws Exception{
        def trainingMethod = new TrainingMethod()

        trainingMethod.name = params.name
        trainingMethod.description = params.description

        trainingMethod.save()
        if (trainingMethod.hasErrors()) {
            throw new Exception("${trainingMethod.errors}")
        }
    }

    def edit(params){
        def trainingMethod = TrainingMethod.findById(params.id)
        if (trainingMethod) {

            trainingMethod.name = params.name
            trainingMethod.description = params.description

            trainingMethod.save()
            if (trainingMethod.hasErrors()) {
                throw new Exception("${trainingMethod.errors}")
            }
        }else{
            throw new Exception("Record not found")
        }
    }

    def delete(params){
        def trainingMethod = TrainingMethod.findById(params.id)
        if (trainingMethod) {
            trainingMethod.delete()
        }else{
            throw new Exception("Record not found")
        }
    }
}
