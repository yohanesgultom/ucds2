package com.ucds.core.business

import org.apache.shiro.SecurityUtils


class TrainingPlanService {

    def gridUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(TrainingPlan, params)
            def rows = []
            def counter = 0
            ((List<TrainingPlan>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.dateCreated,
                                it.createdBy,
                                it.lastUpdated,
                                it.updatedBy
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def trainingPlan = new TrainingPlan()
        trainingPlan.name = params.name
        trainingPlan.createdBy = SecurityUtils.subject.getPrincipal()
        trainingPlan.save()
        if (trainingPlan.hasErrors()) {
            throw new Exception("${trainingPlan.errors}")
        }
    }

    def edit(params) {
        def trainingPlan = TrainingPlan.findById(params.id)
        if (trainingPlan) {
            trainingPlan.name = params.name
            trainingPlan.updatedBy = SecurityUtils.subject.getPrincipal()
            trainingPlan.save()
            if (trainingPlan.hasErrors()) {
                throw new Exception("${trainingPlan.errors}")
            }
        } else {
            throw new Exception("TrainingPlan not found")
        }
    }

    def delete(params) {
        def trainingPlan = TrainingPlan.findById(params.id)
        if (trainingPlan) {
            trainingPlan.delete()
        } else {
            throw new Exception("TrainingPlan not found")
        }
    }

}