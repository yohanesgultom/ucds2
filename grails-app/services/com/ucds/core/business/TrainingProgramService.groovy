package com.ucds.core.business

import grails.converters.JSON
import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

class TrainingProgramService {

    def gridUtilService
    def appUtilService
    def grailsApplication

    def getList(def params) {
        def ret
        try {
            Date period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
            String sort = params.sidx && !"".equals(params.sidx) ? params.sidx : "id"
            String order = params.sord && !"".equals(params.sord) ? params.sord : "asc"
            int max = Math.min(params.rows ? params.int('rows') : 10, 1000)
            int offset = ((params.page ? params.int('page') : 1) - 1) * max

            String paginationQuery = "from PersonCompetency pc " +
                    "join fetch pc.competency c " +
                    "join fetch c.competencyLevels cl " +
                    "join fetch pc.person p " +
                    "where pc.gap < 0 " +
                    "and cl.level = pc.expectedScore " +
                    "and pc.period = ?"

            String countQuery = "select count(*) from PersonCompetency pc " +
                    "join pc.competency c " +
                    "join c.competencyLevels cl " +
                    "join pc.person p " +
                    "where pc.gap < 0 " +
                    "and cl.level = pc.expectedScore " +
                    "and pc.period = ?"

            List queryParams = [period]

            // put search criteria in rules
            params.filters = params.filters ? JSON.parse(params.filters) : [rules:[]]
            if (params.searchField && params.searchOper && params.searchString) {
                params.filters.rules << [op: params.searchOper, field: params.searchField, data: params.searchString]
            }
            if (params.filters.rules) {

                // build where clause from filters
                List whereClauseItems = []
                params.filters?.rules?.each{
                    gridUtilService.buildWhereClause(
                            String.class,
                            it.field,
                            it.op,
                            it.data.toString(),
                            whereClauseItems,
                            queryParams
                    )
                }

                paginationQuery += " and ${whereClauseItems.join( ' and ')}"
                countQuery += " and ${whereClauseItems.join( ' and ')}"
            }

            // set ordering based on params
            paginationQuery += " order by ${sort} ${order}"

            List<PersonCompetency> personCompetencies = PersonCompetency.findAll(paginationQuery, queryParams, [max: max, offset: offset])
            int count = PersonCompetency.executeQuery(countQuery, queryParams)?.get(0)

            List<PersonCompetency> rows = []
            personCompetencies?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.person.firstName,
                                it.person.lastName,
                                it.person.personPosition?.name,
                                it.competency.name,
                                it.actualScore,
                                it.expectedScore,
                                it.competency.competencyLevels?.first()?.keyIndicator
                        ]
                ]
            }

            ret = [rows: rows, records: count, page: params.page, total: Math.ceil(count / max)]

        } catch (e) {
            log.error(e.message, e)
        }

        return ret
    }

    def getDetailsList(def params) {
        def ret = null

        try {
            def res = this.getTrainingProgramList(params)
            def rows = []
            ((List<TrainingProgram>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.competencyLevel,
                                it.trainingMethod?.name,
                                it.trainingMethod?.description,
                                it.trainerOrPIC?.fullName,
                                it.trainerOrPIC?.email,
                                it.reminderDayMinus,
                                it.startDate,
                                it.endDate,
                                it.cost,
                                it.note
                        ]
                ]
            }

            ret = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]

        } catch (e) {
            log.error(e.message, e)
        }

        return ret
    }

    def getTrainingProgramList(def params) {
        def sort = params.sidx && "" != params.sidx ? params.sidx : "competencyLevel"
        def order = params.sord && "" != params.sord ? params.sord : "asc"
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        def rows = []
        def query = "from TrainingProgram tp left join fetch tp.trainerOrPIC p join fetch tp.trainingMethod tm where tp.personCompetency.id = :id order by ${sort} ${order}"
        def trainingPrograms = TrainingProgram.findAll(query, [id: params.long('personCompetencyId')], [max: max, offset: offset])
        // when empty recreate all training programs
        // this gives "reset" feature in case of one unintentionally deleting training program(s)
        if (!trainingPrograms || trainingPrograms.size() <= 0) {
            this.createTrainingPrograms(PersonCompetency.findById(params.personCompetencyId))
            // re-query to fetch newly created records by max & offset
            trainingPrograms = TrainingProgram.findAll(query, [id: params.long('personCompetencyId')], [max: max, offset: offset])
        }
        rows.addAll(trainingPrograms)
        def totalRecords = rows.size()
        def totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    /**
     * Create training programs for each gapped level and method
     * @param personCompetency
     * @return
     */
    private void createTrainingPrograms(PersonCompetency personCompetency) {
        def trainingPrograms = []
        if (personCompetency) {
            def actualScore = personCompetency.actualScore
            def expectedScore = personCompetency.expectedScore
            def trainingPlan = TrainingPlan.findByPeriod(personCompetency.period)
            personCompetency.competency.competencyLevels.each({
                if (it.level > actualScore && it.level <= expectedScore) {
                    if (it.trainingMethods) {
                        // if there is training method(s) set, generate for each training methods
                        it.trainingMethods.eachWithIndex { method, i ->
                            trainingPrograms << new TrainingProgram(
                                    personCompetency: personCompetency,
                                    trainingMethod: method,
                                    competencyLevel: it.level,
                                    reminderDayMinus: '7,3,1',
                                    trainingPlan: trainingPlan
                            )
                        }
                    }
                }
            })
            personCompetency.trainingPrograms = trainingPrograms
            personCompetency.save(failOnError: true, flush: true)
        }
    }

    def edit(GrailsParameterMap params) {
        def res
        def trainingProgram = TrainingProgram.findById(params.id)
        def trainerOrPic = Person.findById(params.trainerOrPicId)
        if (!trainerOrPic) throw new Exception("Invalid person")
        if (trainingProgram) {
            trainingProgram.trainerOrPIC = trainerOrPic
            if (params.reminderDayMinus) {
                params.reminderDayMinus.split(",").each {
                    it = StringUtils.trim(it)
                    if (!it.isInteger()) {
                        throw new Exception("${params.reminderDayMinus} contains non-integer value(s)")
                    }
                }
            }
            trainingProgram.reminderDayMinus = StringUtils.trim(params.reminderDayMinus)
            trainingProgram.startDate = appUtilService.tryParseDate(params.startDate)
            trainingProgram.endDate = appUtilService.tryParseDate(params.endDate)
            trainingProgram.note = params.note
            try {
                params.cost = Double.parseDouble(params.cost)
            } catch (Exception e) {
                params.cost = null
            }
            trainingProgram.cost = params.cost
            trainingProgram.save()
        } else {
            throw new Exception("Record is not found")
        }
        return res
    }

    def delete(params) {
        def res
        def trainingProgram = TrainingProgram.findById(params.id)
        if (trainingProgram) {
            trainingProgram.delete()
        } else {
            throw new Exception("Record is not found")
        }
        return res
    }

    /**
     * Get the total cost and time of trainings in certain period
     * @param period
     * @return
     */
    def getTotalCostAndTime(Date period) {
        def res = [:]
        def result = ((ArrayList) TrainingProgram.executeQuery("select tpl.id, sum(tpg.cost) from TrainingProgram tpg join tpg.trainingPlan tpl where tpl.period = :period group by tpl.id", [period: period]))
        res.totalCost = (result && result.size() > 0 && result[0] instanceof ArrayList && ((ArrayList)result[0]).size() > 0) ? result[0][1] as Double : 0

        // the total time counted from the number of days between
        // the earliest (min) start date and
        // the latest (max) end date
        result = TrainingProgram.executeQuery("select tpl.id, max(tpg.endDate), min(tpg.startDate) from TrainingProgram tpg join tpg.trainingPlan tpl where tpl.period = :period group by tpl.id", [period: period])
        def resDate = (result && result.size() > 0 && result[0] instanceof ArrayList) ? result[0] as ArrayList : []
        res.totalTime = (resDate.size() >= 3) ? resDate.get(1) - resDate.get(2) : 0
        return res
    }
}