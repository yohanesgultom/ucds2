package com.ucds.core.business

class WorkloadItemService {

    def gridUtilService
    def appUtilService

    def add(Map params) throws Exception {
        def newId = null
        Workload workload = Workload.findById(params.workloadId)
        if (workload) {
            WorkloadItem workloadItem = new WorkloadItem()
            workloadItem.name = params.name
            workloadItem.unitOutput = params.unitOutput
            workloadItem.unitWorkload = workload.getUnitWorkload(params.unitWorkload)
            workloadItem.workloadValue = (params.workloadValue) ? Float.parseFloat(params.workloadValue) : 0
            workloadItem.duration = (params.duration) ? Float.parseFloat(params.duration) : 0
            workload.addToWorkloadItems(workloadItem)
            workload.save(failOnError: true)
            newId = workloadItem.id
        }
        return newId
    }

    def edit(Map params) throws Exception {
        WorkloadItem workloadItem = WorkloadItem.findById(params.id)
        workloadItem.name = params.name
        workloadItem.unitOutput = params.unitOutput
        workloadItem.unitWorkload = workloadItem.workload?.getUnitWorkload(params.unitWorkload)
        workloadItem.workloadValue = (params.workloadValue) ? Float.parseFloat(params.workloadValue) : 0f
        workloadItem.duration = (params.duration) ? Float.parseFloat(params.duration) : 0f
        workloadItem.save(failOnError: true)
        return workloadItem.id
    }

    def delete(params) {
        def workloadItem = WorkloadItem.findById(params.id)
        if (workloadItem) {
            def workload = workloadItem.workload
            workload.removeFromWorkloadItems(workloadItem)
            workloadItem.delete()
            return params.id
        } else {
            throw new Exception("Workload analysis is not found")
        }
    }

    def getList(def params) {
        def ret = null
        try {
            def res = gridUtilService.getDomainList(WorkloadItem, params)
            def rows = []
            ((List<WorkloadItem>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.unitOutput,
                                it.unitWorkload,
                                it.workloadValue,
                                it.duration,
                                it.unitWorkload,
                                it.manpowerRequired,
                                it.workloadDuration,
                                it.effectiveWorkloadDuration
                        ]
                ]
            }

            ret = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]

        } catch (e) {
            throw e
        }
        return ret
    }

}
