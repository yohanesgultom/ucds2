package com.ucds.core.security

import com.ucds.core.business.Person
import org.apache.shiro.crypto.hash.Sha256Hash


class ShiroUserService {

    def gridUtilService

    def getList(def params){
        def ret
        try {
            def res = gridUtilService.getDomainList(ShiroUser, params)
            def rows = []
            ((List<ShiroUser>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                            it.username,
                            it.person?.id,
                            it.person?.fullName,
                            "",
                            it.permissions.join(",")
                        ]
                ]
            }

            ret = [rows: rows, records: res.totalRecords, page: params.page, total: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception{
        def shiroUser = new ShiroUser()
        
        shiroUser.passwordHash = new Sha256Hash(params.password).toHex()
        shiroUser.username = params.username
        if (params.permissions) {
            params.permissions.split(",").each { permission ->
                shiroUser.addToPermissions(permission)
            }
        } else {
            shiroUser.addToPermissions("main:*")
        }

        if (params.personId && params.person) {
            def person = Person.get(Long.parseLong(params.personId))
            shiroUser.person = person
        }

        shiroUser.save()
        if (shiroUser.hasErrors()) {
            throw new Exception("${shiroUser.errors}")
        }
    }

    def edit(params){
        def shiroUser = ShiroUser.findById(params.id)
        if (shiroUser) {
            // do not replace password if input empty
            if (params.password && params.password.length() > 0) {
                shiroUser.passwordHash = new Sha256Hash(params.password).toHex()
            }
            shiroUser.username = params.username
            if (params.permissions) {
                // clear previous permissions
                if (shiroUser.permissions) {
                    shiroUser.permissions.clear()
                }
                // add new ones
                params.permissions.split(",").each { permission ->
                    shiroUser.addToPermissions(permission)
                }
            } else {
                shiroUser.addToPermissions("main:*")
            }

            if (params.personId && params.person) {
                def person = Person.get(Long.parseLong(params.personId))
                shiroUser.person = person
            } else {
                shiroUser.person = null
            }

            shiroUser.save()
            if (shiroUser.hasErrors()) {
                throw new Exception("${shiroUser.errors}")
            }
        }else{
            throw new Exception("ShiroUser not found")
        }
    }

    def delete(params){
        def shiroUser = ShiroUser.findById(params.id)
        if (shiroUser) {
            shiroUser.delete()
        }else{
            throw new Exception("ShiroUser not found")
        }
    }

}