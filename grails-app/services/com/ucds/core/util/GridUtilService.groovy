package com.ucds.core.util

import grails.converters.JSON

class GridUtilService {

    def getDomainList(Class domain, def params){
        String sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        String order = params.sord && !"".equals(params.sord) ? params.sord : null
        int max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        int offset = ((params.page ? params.int('page') : 1) - 1) * max

        String whereClause = ""
        List filterData = []
        List whereClauseItems = []

        if (params.boolean('_search')) {
            if (params.filters) {
                def filters = JSON.parse(params.filters)
                filters.rules.each {
                    buildWhereClause(
                            this.getFieldType(domain, it.field),
                            it.field, it.op,
                            it.data.toString(),
                            whereClauseItems,
                            filterData
                    )
                }

                whereClause = whereClauseItems?" where ${whereClauseItems.join(" ${filters.get('groupOp')} ")}":""

            } else if (params.searchField && params.searchOper && params.searchString) {
                buildWhereClause(
                        this.getFieldType(domain, params.searchField),
                        params.searchField,
                        params.searchOper,
                        params.searchString.toString(),
                        whereClauseItems,
                        filterData
                )
                whereClause = whereClauseItems?" where ${whereClauseItems.join(' and ')}":""

            } else {
                //NA
            }
        } else {
            //NA
        }

        String query = """
            from ${domain.getName()} ${whereClause} ${(sort&&order?("order by ${sort} ${order}"):"")}
        """

        String countQuery = """
            select count(*) from ${domain.getName()} ${whereClause}
        """

        List rows = []
        int totalRecords = 0

        try {
            domain.findAll(query, filterData, [max:max,offset:offset])?.each{
                rows << it
            }
            totalRecords = domain.executeQuery(countQuery, filterData)?.get(0)
        } catch (e) {
            log.error(e.message, e)
        }

        int totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    def getFieldType(Class domain, String searchField) {
        def field = null
        try {
            field = domain.getDeclaredField(searchField)
        } catch (Exception e) {
            // do nothing
        }
        return (field) ? field.type : String.class
    }

    /**
     * Append new item to <pre>whereClauseItems</pre> and <pre>filterData</pre> based on given parameters and internal rules
     * @param fieldType
     * @param field
     * @param op
     * @param data
     * @param whereClauseItems
     * @param filterData
     * @return
     */
    def buildWhereClause(Class fieldType, String field, String op, String data, ArrayList whereClauseItems, ArrayList filterData) {
        switch (op) {
            case "eq":
                switch (field) {
                    case 'nothing':
                        break
                    default:
                        if (data) {
                            if (data == 'true' || data == 'false') {
                                whereClauseItems << "${field} = ?"
                                filterData << Boolean.parseBoolean(data)
                            } else if (data.isNumber()) {
                                op = '='
                                if (fieldType == int.class || fieldType == int.class) {
                                    whereClauseItems << "${field} ${op} ?"
                                    filterData << Integer.parseInt(data)
                                } else if (data.isLong()){
                                    whereClauseItems << "${field} ${op} ?"
                                    filterData << Long.parseLong(data)
                                } else if (data.isDouble()){
                                    whereClauseItems << "${field} ${op} ?"
                                    filterData << Double.parseDouble(data)
                                }
                            } else{
                                whereClauseItems << "upper(${field}) = upper(?)"
                                filterData << data
                            }
                        } else {
                            whereClauseItems << "${field} is null"
                        }
                        break
                }
                break
            case "ne":
                switch (field) {
                    case 'nothing':
                        break
                    default:
                        if (data) {
                            if (data == 'true' || data == 'false') {
                                whereClauseItems << "${field} != ?"
                                filterData << Boolean.parseBoolean(data)
                            }else if (data.isNumber()){
                                op = '!='
                                if (fieldType == int.class || fieldType == int.class) {
                                    whereClauseItems << "${field} ${op} ?"
                                    filterData << Integer.parseInt(data)
                                } else if (data.isLong()){
                                    whereClauseItems << "${field} ${op} ?"
                                    filterData << Long.parseLong(data)
                                } else if (data.isDouble()){
                                    whereClauseItems << "${field} ${op} ?"
                                    filterData << Double.parseDouble(data)
                                }
                            }else{
                                whereClauseItems << "upper(${field}) != upper(?)"
                                filterData << data
                            }
                        } else {
                            whereClauseItems << "${field} is not null"
                        }
                        break
                }
                break
            case "lt":
                if (data && data.isNumber()) {
                    op = '<'
                    if (fieldType == int.class || fieldType == int.class) {
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Integer.parseInt(data)
                    } else if (data.isLong()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Long.parseLong(data)
                    } else if (data.isDouble()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Double.parseDouble(data)
                    }
                }
                break
            case "le":
                if (data && data.isNumber()) {
                    op = '<='
                    if (fieldType == int.class || fieldType == int.class) {
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Integer.parseInt(data)
                    } else if (data.isLong()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Long.parseLong(data)
                    } else if (data.isDouble()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Double.parseDouble(data)
                    }
                }
                break
            case "gt":
                if (data && data.isNumber()) {
                    op = '>'
                    if (fieldType == int.class || fieldType == int.class) {
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Integer.parseInt(data)
                    } else if (data.isLong()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Long.parseLong(data)
                    } else if (data.isDouble()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Double.parseDouble(data)
                    }
                }
                break
            case "ge":
                if (data && data.isNumber()) {
                    op = '>='
                    if (fieldType == int.class || fieldType == int.class) {
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Integer.parseInt(data)
                    } else if (data.isLong()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Long.parseLong(data)
                    } else if (data.isDouble()){
                        whereClauseItems << "${field} ${op} ?"
                        filterData << Double.parseDouble(data)
                    }
                }
                break
            case "bw":
                whereClauseItems << "upper(${field}) like upper(?)||'%'"
                filterData << data
                break
            case "bn":
                whereClauseItems << "upper(${field}) not like upper(?)||'%'"
                filterData << data
                break
            case "ew":
                whereClauseItems << "upper(${field}) like '%'||upper(?)"
                filterData << data
                break
            case "en":
                whereClauseItems << "upper(${field}) not like '%'||upper(?)"
                filterData << data
                break
            case "cn":
                whereClauseItems << "upper(${field}) like '%'||upper(?)||'%'"
                filterData << data
                break
            case "nc":
                whereClauseItems << "upper(${field}) not like '%'||upper(?)||'%'"
                filterData << data
                break
            case "in":
                def inArr = data ? data.split(",").toList() : []
                def template = inArr.collect{"?"}.join(",")
                log.info("template: " + template)
                whereClauseItems << "${field} in (${template})"
                inArr.each {
                    if (it && fieldType == Long.class) {
                        filterData << Long.parseLong(it)
                    } else {
                        filterData << ''
                    }
                }
                break
            case "ni":
                def inArr = data?.split(",")
                String foo = "${field} not in ("
                inArr?.each { n ->
                    foo += "?,"
                    filterData << n?.trim()
                }
                foo += ")"
                whereClauseItems << foo
                break
            default:
                whereClauseItems << "upper(${field}) = upper(?)"
                filterData << data == 'true' || data == 'false' ? Boolean.parseBoolean(data) : data
        }
    }

}

