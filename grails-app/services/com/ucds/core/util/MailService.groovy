package com.ucds.core.util

import com.ucds.core.SystemParameter
import com.ucds.core.business.Competency
import com.ucds.core.business.CompetencyLevel
import com.ucds.core.business.Person
import com.ucds.core.business.PersonCompetency
import com.ucds.core.business.TrainingProgram
import org.joda.time.DateTime
import org.joda.time.Days

import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import java.text.SimpleDateFormat

class MailService {

    def send(String host, int port, String props, String username, String password, String from, String to, String subject, String body) {
        Properties properties = new Properties()
        properties.put("mail.smtp.host", host)
        properties.put("mail.smtp.socketFactory.port", "${port}")
        props?.split(",").each {
            def prop = it.split(":")
            properties.put(prop[0], prop[1])
        }

        Session session = Session.getDefaultInstance(properties,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password)
                }
            }
        )

        try {
            MimeMessage message = new MimeMessage(session)
            message.setFrom(new InternetAddress(from))
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to))
            message.setSubject(subject)
            message.setContent(body, "text/html")
            Transport.send(message)
        } catch (MessagingException e) {
            throw new RuntimeException(e)
        }
    }

    def sendTrainingProgramsReminderEmail() {
        String active
        // suppressing annoying meaningless error
        try {
            active = SystemParameter.findByCode("scheduler.trainingprogram.mail.active")?.value
        } catch (Exception e) {
            if (!e.message.contains("No signature of method")) {
                log.error(e.message, e)
            }
        }
        log.info("scheduler.trainingprogram.mail.active: ${active}")
        if (active && Boolean.parseBoolean(active)) {
            // get config from db
            def host = SystemParameter.findByCode("grails.mail.host")?.value
            def port = Integer.parseInt(SystemParameter.findByCode("grails.mail.port")?.value)
            def username = SystemParameter.findByCode("grails.mail.username")?.value
            def password = SystemParameter.findByCode("grails.mail.password")?.value
            def propsStr = SystemParameter.findByCode("grails.mail.props")?.value

            if (host && port && username && password && propsStr) {

                def templateLocale = SystemParameter.findByCode("template.mail.trainingprogram.locale")?.value?.split("_")
                def templateSubject = SystemParameter.findByCode("template.mail.trainingprogram.subject")?.value
                def templateBody = SystemParameter.findByCode("template.mail.trainingprogram.body")?.value

                Locale locale = new Locale(templateLocale[0], templateLocale[1])
                SimpleDateFormat formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy", locale)

                // get future training programs
                List<TrainingProgram> programs = TrainingProgram.findAll("from TrainingProgram t inner join fetch t.trainingMethod inner join fetch t.personCompetency pc inner join fetch pc.competency inner join fetch pc.person p inner join fetch t.trainerOrPIC tr where t.startDate > current_date() order by t.startDate")

                // get competency & levels to get the indicators
                List<Competency> competencies = Competency.findAll("from Competency c inner join fetch c.competencyLevels cl")

                // send email
                DateTime today = new DateTime()
                programs?.each { program ->
                    try {
                        // make sure all necessary fields are not empty
                        if (program?.trainerOrPIC?.email && program.reminderDayMinus) {
                            DateTime startDate = new DateTime(program.startDate.time)
                            List<String> reminderDayMinus = program.reminderDayMinus?.split(",")
                            int dayMinus = Days.daysBetween(today.withTimeAtStartOfDay(), startDate.withTimeAtStartOfDay()).days
                            if (reminderDayMinus.contains(String.valueOf(dayMinus))) {
                                Person trainee = program.personCompetency.person
                                PersonCompetency traineeCompetency = program.personCompetency
                                String programName = program.trainingMethod.name + " " + program.personCompetency.competency.name
                                String to = program.trainerOrPIC.email
                                String traineeSkypeLink = trainee.skype && trainee.skype.length() > 0 ? "<a href='skype:${trainee.skype}?call'>${trainee.skype}</a>" : ""

                                // find indicators
                                String indicators = ""
                                for (Competency c:competencies) {
                                    if (c.id == traineeCompetency.competency.id) {
                                        for (CompetencyLevel cl:c.competencyLevels) {
                                            if (cl.level == traineeCompetency.expectedScore) {
                                                indicators = cl.keyIndicator.replaceAll("\n", "<br>")
                                                break
                                            }
                                        }
                                        break
                                    }
                                }

                                // generate subject
                                String subject = templateSubject
                                        .replaceAll("\\{program\\}", programName)
                                        .replaceAll("\\{dayMinus\\}", String.valueOf(dayMinus))

                                // generate body
                                String body = templateBody
                                        .replaceAll("\\{pic\\}", program.trainerOrPIC.fullName)
                                        .replaceAll("\\{program\\}", programName)
                                        .replaceAll("\\{dayMinus\\}", String.valueOf(dayMinus))
                                        .replaceAll("\\{startDate\\}", formatter.format(program.startDate))
                                        .replaceAll("\\{endDate\\}", formatter.format(program.endDate))
                                        .replaceAll("\\{traineeName\\}", trainee.fullName)
                                        .replaceAll("\\{attitude\\}", traineeCompetency.competency.name)
                                        .replaceAll("\\{indicators\\}", indicators)
                                        .replaceAll("\\{traineeSkypeLink\\}", traineeSkypeLink)

                                send(host, port, propsStr, username, password, username, to, subject, body)
                                log.info("reminder email sent to ${to}")
                            }
                        }
                    } catch (Exception e) {
                        log.error(e.message, e)
                    }
                }

            }

        }
    }

}
