<div class="action-bar">
    <g:select id="periodYear" name="periodYear" title="${g.message(code:'period.year',default:'Period Year')}" from="${years}" value="${defaultYear}"/>
    <g:select id="periodMonth" name="periodMonth" title="${g.message(code:'period.month',default:'Period Month')}" from="${months.entrySet()}" optionKey="key" optionValue="value" value="${defaultMonth}"/>
    <g:select id="section" class="filter" name="p.section" title="${g.message(code:'ucds.person.section.label',default:'Section')}" from="${sections}" noSelection="${['':g.message(code: 'select.all', default: 'All')]}" />
    <g:select id="position" class="filter" name="pp.name" title="${g.message(code:'ucds.person.position.label',default:'Position')}" from="${positions}" noSelection="${['':g.message(code: 'select.all', default: 'All')]}" />
    <span id="reload">${g.message(code: "refresh", default: "Refresh")}</span>
</div>

<script>
    function getFilterParams() {
        var period = '1-' + $('#periodMonth').val() + '-' + $('#periodYear').val();
        var filters = {};
        var count = 0;
        $.each($('#reload').siblings('.filter'), function(i, e) {
            if (e.value) {
                filters[e.name] = e.value;
                count++;
            }
        });
        filters = (count > 0) ? JSON.stringify(filters) : null;
        return {period: period, filters: filters}
    }
</script>