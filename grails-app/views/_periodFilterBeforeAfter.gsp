<div class="action-bar">
    <span>Before</span>
    <g:select id="periodYearBefore" name="periodYearBefore" title="${g.message(code:'period.year',default:'Period Year Before')}" from="${years}" value="${defaultYearBefore}"/>
    <g:select id="periodMonthBefore" name="periodMonthBefore" title="${g.message(code:'period.month',default:'Period Month')}" from="${months.entrySet()}" optionKey="key" optionValue="value" value="${defaultMonthBefore}"/>
    <span>After</span>
    <g:select id="periodYearAfter" name="periodYearAfter" title="${g.message(code:'period.year',default:'Period Year After')}" from="${years}" value="${defaultYearAfter}"/>
    <g:select id="periodMonthAfter" name="periodMonthAfter" title="${g.message(code:'period.month',default:'Period Month')}" from="${months.entrySet()}" optionKey="key" optionValue="value" value="${defaultMonthAfter}"/>
    <span id="reload">${g.message(code: "refresh", default: "Refresh")}</span>
    <span id="export">${g.message(code: "export", default: "Export")}</span>
</div>

<script>
    function getFilterParams() {
        var periodBefore = '1-' + $('#periodMonthBefore').val() + '-' + $('#periodYearBefore').val();
        var periodAfter = '1-' + $('#periodMonthAfter').val() + '-' + $('#periodYearAfter').val();
        return {periodBefore: periodBefore, periodAfter: periodAfter}
    }
</script>