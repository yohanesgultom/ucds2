<div class="action-bar">
    <g:select id="periodYear" name="periodYear" title="${g.message(code:'period.year',default:'Period Year')}" from="${years}" value="${defaultYear}"/>
    <g:select id="periodMonth" name="periodMonth" title="${g.message(code:'period.month',default:'Period Month')}" from="${months.entrySet()}" optionKey="key" optionValue="value" value="${defaultMonth}"/>
</div>

<script>
    function getFilterParams() {
        var period = '1-' + $('#periodMonth').val() + '-' + $('#periodYear').val();
        return {period: period}
    }

    function getFilterPeriodForDisplay() {
        return $('#periodMonth').find(':selected').text() + ' ' + $('#periodYear').val();
    }
</script>