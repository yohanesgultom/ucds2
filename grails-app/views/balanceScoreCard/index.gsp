<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Balance Score Cards</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="BalanceScoreCard-Grid"></table>
<div id="BalanceScoreCard-Pager"></div>

<script type="text/javascript">

    function initBalanceScoreCardGrid(){
        $('#BalanceScoreCard-Grid').jqGrid({
            url: '${request.getContextPath()}/balanceScoreCard/getList',
            editurl: '${request.getContextPath()}/balanceScoreCard/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.balanceScoreCard.name.label" default="Name"/>',
                '<g:message code="app.balanceScoreCard.inactive.label" default="Status"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'inactive', index:'inactive',editable:true,edittype:'select',formatter:'select',editoptions:{value:'true:Inactive;false:Active'},searchoptions:{sopt:['eq']}}
            ],
            height: 'auto',
            pager: '#BalanceScoreCard-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Balance Score Cards',
            onSelectRow: function(rowid){
                var rd = $("#BalanceScoreCard-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#BalanceScoreCard-Grid').jqGrid(
                'navGrid','#BalanceScoreCard-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#BalanceScoreCard-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#BalanceScoreCard-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
    }
    }, // del options
    {}, // search options
    {} // view options
    );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initBalanceScoreCardGrid();
    });

</script>
</body>
</html>