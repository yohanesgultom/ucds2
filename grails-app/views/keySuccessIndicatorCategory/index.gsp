<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Key Success Indicator Category</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="KeySuccessIndicatorCategory-Grid"></table>
<div id="KeySuccessIndicatorCategory-Pager"></div>

<script type="text/javascript">

    function initKeySuccessIndicatorCategoryGrid(){
        $('#KeySuccessIndicatorCategory-Grid').jqGrid({
            url: '${request.getContextPath()}/keySuccessIndicatorCategory/getList',
            editurl: '${request.getContextPath()}/keySuccessIndicatorCategory/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.KeySuccessIndicatorCategory.name.label" default="Name"/>',
                '<g:message code="app.KeySuccessIndicatorCategory.inactive.label" default="Status"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'inactive', index:'inactive',editable:true,edittype:'select',formatter:'select',editoptions:{value:'true:Inactive;false:Active'},searchoptions:{sopt:['eq']}}
            ],
            height: 'auto',
            pager: '#KeySuccessIndicatorCategory-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Key Success Indicator Category',
            onSelectRow: function(rowid){
                var rd = $("#KeySuccessIndicatorCategory-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#KeySuccessIndicatorCategory-Grid').jqGrid(
                'navGrid','#KeySuccessIndicatorCategory-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#KeySuccessIndicatorCategory-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#KeySuccessIndicatorCategory-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
    }
    }, // del options
    {}, // search options
    {} // view options
    );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initKeySuccessIndicatorCategoryGrid();
    });

</script>
</body>
</html>