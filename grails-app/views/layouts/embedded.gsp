<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
<r:layoutResources />
<h3><g:layoutTitle/></h3>
<g:layoutBody/>
<g:layoutHead/>
<r:layoutResources />
