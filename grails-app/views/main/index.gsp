<%@ page import="org.apache.shiro.SecurityUtils" %>
<!DOCTYPE html>
<html>
<head>

    <link rel="shortcut icon" href="<g:resource dir='images' file='favicon.ico'/>">
    <title><g:message code="app.name" default="Sirsak"/></title>

    <!-- $ UI Layout -->
    <g:javascript src="jquery-1.8.3.js"/>
    <g:javascript src="jquery-ui-1.10.4.sirsak.min.js"/>
    <g:javascript src="jquery.layout-latest.min.js"/>

    <g:javascript src="jquery.jqGrid.min.js"/>
    <g:javascript src="grid.locale-en.js"/>

    <!-- Vertical Accordion -->
    <g:javascript src="jquery.cookie.js"/>
    <g:javascript src="jquery.hoverIntent.minified.js"/>
    <g:javascript src="jquery.dcjqaccordion.2.9.js"/>

    <g:javascript src="tablecloth/js/jquery.tablecloth.js"/>
    <g:javascript src="jqueryplot/jquery.jqplot.min.js"/>
    <!--[if lt IE 9]><g:javascript src="jqueryplot/excanvas.min.js"/><![endif]-->
    <g:javascript src="jqueryplot/plugins/jqplot.dateAxisRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.canvasTextRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.canvasAxisTickRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.barRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.categoryAxisRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.pointLabels.min.js"/>

<script type="text/javascript">

        var confirmMsg = '${g.message(code: "default.confirm.message", default: "Are you sure?")}',
            myLayout, // a var is required because this page utilizes: myLayout.allowOverflow() method
            spinner = $('<img>').attr('src', '${g.resource(dir: "images", file: 'ajax-loader.gif')}')

        // tooltip
        $(document).tooltip({
            content: function() { return $(this).attr('title'); }
        });

        $(document).ready(function () {
            // $ UI Layout
            myLayout = $('body').layout({
                west__size: '15%'
                // enable showOverflow on west-pane so popups will overlap north pane
                //west__showOverflowOnHover: true
                //,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
            });

            // Vertical Accordion
            initMenu();

            // Load default page
            var defaultLink = $("a[data-action='${ home ? home.controller + ":" + home.action : ""}']").first();
            var defaultMenu = defaultLink.parent("li");
            var parentMenu = defaultMenu.parent("li");
            $('a', parentMenu).click();
            defaultLink.click();
        });

        /**
         * init vertical accordion menu
         */
        function initMenu() {
            $('.menu', '#dc_jqaccordion_widget-7-item').dcAccordion({
                eventType: 'click',
                hoverDelay: 0,
                menuClose: false,
                autoClose: false,
                saveState: false,
                autoExpand: false,
                classExpand: 'current-menu-item',
                classDisable: '',
                showCount: false,
                disableLink: true,
                cookie: 'dc_jqaccordion_widget-7',
                speed: 'fast'
            });
        }

        function loadPage(url) {
            if (url) {
                var content = $('#content');
                $('.ui-dialog').remove();
                content.empty().append(spinner);
                content.load(url, function () {});
            }
        }

        function showOverlay() {
            var overlay = $('div.overlay');
            overlay.show();
        }

        function hideOverlay() {
            var overlay = $('div.overlay');
            overlay.hide();
        }

        /* jqGrid Datepicker */

        function initDatePicker(elm) {
            setTimeout(function () {
                $(elm).datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'both',
                    buttonImage: '${resource(dir: "images", file: "calendar-icon.png")}',
                    buttonImageOnly: true
                });
                $('.ui-datepicker').css({'font-size': '90%'});
            }, 200);
        }

        function initDOBDatePicker(elm) {
            setTimeout(function () {
                $(elm).datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeYear: true,
                    changeMonth: true,
                    defaultDate: '-30y',
                    showOn: 'both',
                    buttonImage: '${resource(dir: "images", file: "calendar-icon.png")}',
                    buttonImageOnly: true
                });
                $('.ui-datepicker').css({'font-size': '90%'});
            }, 200);
        }

        function getJSONDataSync(url, data) {
            data = (!data) ? {} : data;
            var data = $.ajax({
                url: url,
                dataType: 'json',
                data: data,
                async: false,
                success: function (data, result) {
                    if (!result) {
                        alert('Fail to retrieve lookup data.');
                    }
                }
            }).responseText;
            return JSON.parse(data);
        }

        function parseUcdsDate(val, separator) {
            separator = (separator) ? separator : '-';
            var arrVal = val.split(separator);
            return new Date(parseInt(arrVal[2]), parseInt(arrVal[1]) - 1, parseInt(arrVal[0]));
        }

        /**
         * Parse JSON response and alert response.message
         * @param response
         * @param postdata
         * @returns {*[]}
         */
        function afterSubmitForm(response, postdata){
            var res = $.parseJSON(response.responseText);
            alert(res.message);
            var success = (res.message) && ((res.message.toLowerCase().indexOf('success') > -1) || (res.message.toLowerCase().indexOf('sukses') > -1));
            return [success, res.message];
        }

        /**
         * Get jqgrid select LOV string from server asyncronously
         * @param url
         * @param cache
         */
        function getLOV(url, cache, flush) {
            if (!cache || flush) {
                cache = '';
                var data = getJSONDataSync(url);
                for (var i = 0; i < data.length; i++) {
                    cache += data[i].id + ':' + data[i].name;
                    if (i < (data.length - 1)) {
                        cache += ';';
                    }
                }
            }
            return cache;
        }

        /**
        * Return mapper of lov
        * @param values
        * @returns {{}}
         */
        function getLOVMapper(values) {
            var mapper = {};
            if (values) {
                $.each(values.split(';'), function(i, str) {
                    var lov = str.split(':');
                    if (lov && lov.length == 2) {
                        mapper[lov[0]] = lov[1];
                    }
                })
            }
            return mapper;
        }

        function addHelpIcons(form, helpList) {
            var imgInfo = $('<img>').attr('src','${g.resource(dir: "images", file: "info.png")}').css('padding-left','3px');
            $.each(helpList, function(i, help){
                $('#tr_'+help.name+' .CaptionTD',form).append(imgInfo.clone().attr('title', help.value));
            });
        }

        // skype link formatter for jqgrid
        function skypeFormatter ( cellvalue, options, rowObject ) {
            var url, skypeClass, skypeIcon
            if (cellvalue && cellvalue.length > 0) {
                url = 'skype:' + cellvalue + '?call'
                skypeClass = 'skype-call'
                skypeIcon = '<img class="' + skypeClass + '" src="${resource(dir: "images", file: "skype-icon.png")}" height="16px"/>'
                cellvalue = '<a href="' + url + '">' + skypeIcon + '</a>'
            } else {
                skypeClass = 'skype-call skype-not-available'
                skypeIcon = '<img class="' + skypeClass + '" src="${resource(dir: "images", file: "skype-grey-icon.png")}" height="16px"/>'
                cellvalue = skypeIcon
            }
            return cellvalue
        }

        // skype link unformatter for jqgrid
        function skypeUnformat (cellvalue, options, cell) {
            var a = $('a', cell)
            return (a.length > 0) ? a.attr('href').replace(/skype:(.*)\?call/, '$1') : ''
        }

        function wrapFormatter (cellvalue, options) {
            return '<div>' + cellvalue + '</div>'
        }

        // List of HTML entities for escaping.
        var htmlEscapes = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#x27;',
            '/': '&#x2F;'
        };

        // Regex containing the keys listed immediately above.
        var htmlEscaper = /[&<>"'\/]/g;

        // Escape a string for HTML interpolation.
        function escapeHTML(string) {
            return ('' + string).replace(htmlEscaper, function(match) {
                return htmlEscapes[match];
            });
        };

        // split text by comma by considering optional empty space(s)
        function split(term) {
            return term.split( /,\s*/ );
        }

        // extract last item of comma separated values
        function extractLast(term) {
            return split(term).pop();
        }

        var people = [] // person autocomplete cache

        // init person autocomplate in jqgrid
        function personAutoCompleteInit(el) {
            $(el)
            // don't navigate away from the field on tab when selecting an item
            .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "ui-autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    var term = extractLast(request.term);
                    if (!people || people.length <= 0) {
                        $.getJSON(
                                "${request.getContextPath()}/person/getListForAutocomplete",
                                {term: term},
                                function (data) {
                                    people = data;
                                }
                        );
                    }

                    // filter
                    var res = [];
                    var pattern = new RegExp('(' + term + ')', 'i');
                    for (var i = 0; i < people.length; i++) {
                        if (people[i].value && people[i].value.match(pattern)) {
                            res.push(people[i]);
                        }
                    }
                    response(res);
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 2 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    this.value = ui.item.value;
                    return false;
                }
            });
        }
</script>

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto"/>
    <link rel="stylesheet" type="text/css" media="screen" href="${g.resource(dir: 'css/sirsak', file: 'jquery-ui-1.10.4.custom.min.css')}"/>
    <link rel="stylesheet" type="text/css" media="screen" href="${g.resource(dir: 'css', file: 'ui.jqgrid.css')}"/>

    <!-- Vertical Accordion -->
    <link rel="stylesheet" href="${resource(dir: 'css/accordion', file: 'vertical-accordion.css')}" type="text/css">

    <!-- $ UI Layout -->
    <style type="text/css">
        /**
         *	Basic Layout Theme
         *
         *	This theme uses the default layout class-names for all classes
         *	Add any 'custom class-names', from options: paneClass, resizerClass, togglerClass
         */

    .ui-layout-pane {
        /* all 'panes' */
        background: #FFF;
        border: 1px solid #BBB;
        padding: 10px;
        overflow: auto;
    }

    .ui-layout-north {
        overflow: hidden;
    }

    .ui-layout-west {
        padding: 0;
        background: #ECECEC;
    }

    .ui-layout-resizer {
        /* all 'resizer-bars' */
        background: #DDD;
    }

    .ui-layout-toggler {
        /* all 'toggler-buttons' */
        background: #AAA;
    }

    .overlay {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url(${resource(dir: "images", file: "ajax-loader.gif")}) 50% 50% no-repeat;
    }

    h3 {
        /*color: #2E6E9E;*/
        color: #1fb811;
    }

    body, input, textarea, select, button {
        font:normal 0.9em Roboto,Tahoma;
    }

    p {
        margin: 1em 0;
    }

    .ui-layout-north ul ul {
        /* Drop-Down */
        bottom: auto;
        margin: 0;
        margin-top: 1.45em;
    }

    #logo {
        /*margin-bottom: 2em;*/
    }

    #app-title {
        margin-left: 62px;
        margin-top: -10px;
        color: #595959;
        font-size: 0.7em;
    }

    #current-user {
        /*margin-top: 1em;*/
        font-size: 0.85em;
        color: gray;
        float: right;
    }

    #current-user a {
        color: red;
        text-decoration: none;
        font-weight: bold;
    }

    .ui-tooltip {
        font: 9pt Roboto,sans-serif;
    }

    .build-info {
        font-size: 7pt;
        color: rgb(225, 222, 222);
        float: right;
    }

    /* jqgrid cell value wrapping */
    .ui-jqgrid tr.jqgrow td {
        word-wrap: break-word; /* IE 5.5+ and CSS3 */
        white-space: pre-wrap; /* CSS3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        overflow: hidden;
        height: auto;
        vertical-align: middle;
        padding-top: 3px;
        padding-bottom: 3px
    }

    .ui-tooltip {
        max-width: 600px;
        word-wrap: break-word;
        background-color: whitesmoke;
        border: none;
    }

    ul.ui-autocomplete {
        z-index: 3000;
        font-size: 90%;
    }
    .ui-autocomplete-loading {
        background: white url(${resource(dir: 'images', file: 'ui-anim_basic_16x16.gif')}) right center no-repeat;
    }

</style>
</head>

<body>

<!-- manually attach allowOverflow method to pane -->
<div class="ui-layout-north">
    <!-- build info -->
    <g:if test="${grailsApplication.config.build.time}">
        <div class="build-info">Build ${grailsApplication.config.app.version}.${grailsApplication.config.build.time}</div>
    </g:if>
    <div id="logo" role="banner">
        <a href="${resource(uri:'/')}"><img src="${resource(dir: 'images', file: 'logo.png')}" alt="Sirsak"/></a>
        <div id="app-title"><g:message code="app.name.long" default="Human Resource Development System"></g:message></div>
    </div>
    <div id="current-user">
        <g:message code="menu.user.loginas.label" default="Currently login as"/>: <strong>${SecurityUtils.getSubject().getPrincipals().oneByType(String.class)}</strong> | <g:link controller="auth" action="signOut" onclick="return confirm(confirmMsg)"><g:message
            code="default.signout.label" default="logout"/></g:link>
    </div>
</div>

<!-- allowOverflow auto-attached by option: west__showOverflowOnHover = true -->
<div class="ui-layout-west">
    <div class="dcjq-accordion" id="dc_jqaccordion_widget-7-item">
        <ul id="menu" class="menu">
            <g:each in="${menuInstanceList}" status="i" var="menuInstance">
                <li id="menu-item-${i}" class="menu-item menu-item-type-custom menu-item-object-custom">
                    <a href="#">${g.message(code: menuInstance.menuCode, default: menuInstance.label)}</a>
                    <g:if test="${menuInstance.children?.size() > 0}">
                        <ul class="sub-menu">
                            <g:each in="${menuInstance.children}" status="j" var="menuChild">
                                <li id="menu-item-${i}${j}" class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a href="#" target="${menuChild.targetMode ?: '_self'}"
                                       data-action="${menuChild.linkController}:${menuChild.linkAction}"
                                       onclick="loadPage('${g.createLink(controller: menuChild.linkController, action: menuChild.linkAction)}', '${menuChild.linkController}:${menuChild.linkAction}')">${g.message(code: menuChild.menuCode, default: menuChild.label)}</a>
                                </li>
                            </g:each>
                        </ul>
                    </g:if>
                </li>
            </g:each>
        </ul>
    </div>
</div>

<div id="content" class="ui-layout-center"></div>
<div class="overlay"></div>

</body>
</html>