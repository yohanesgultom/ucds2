<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Menu</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="Menu-Grid"></table>
<div id="Menu-Pager"></div>

<script type="text/javascript">

    function initMenuGrid(){
        $("#Menu-Grid").jqGrid({
            url: '${request.getContextPath()}/menu/getList',
            editurl: '${request.getContextPath()}/menu/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[

                '<g:message code="app.menu.details.label" default="Details"/>',
                '<g:message code="app.menu.label.label" default="Label"/>',
                '<g:message code="app.menu.linkAction.label" default="LinkAction"/>',
                '<g:message code="app.menu.linkController.label" default="LinkController"/>',
                '<g:message code="app.menu.menuCode.label" default="MenuCode"/>',
                '<g:message code="app.menu.seq.label" default="Seq"/>',
                '<g:message code="app.menu.targetMode.label" default="TargetMode"/>',
                '<g:message code="app.menu.url.label" default="Url"/>'

            ],
            colModel :[

                {name:'details', index:'details',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'label', index:'label',editable:true,edittype:'text',required:true,searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'linkAction', index:'linkAction',editable:true,edittype:'text',required:true,searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'linkController', index:'linkController',editable:true,edittype:'text',required:true,searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'menuCode', index:'menuCode',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'seq', index:'seq',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'targetMode', index:'targetMode',editable:true,edittype:'select',editoptions:{value:'_self:Self;_blank:Blank'},stype:'select'}  ,
                {name:'url', index:'url',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}

            ],
            height: 'auto',
            pager: '#Menu-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Menu',
            onSelectRow: function(rowid){
                var rd = $("#Menu-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#Menu-Grid').jqGrid(
                'navGrid','#Menu-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#Menu-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#Menu-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initMenuGrid();
    });

</script>

</body>
</html>