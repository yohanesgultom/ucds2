<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="COMPETENCY_POINTS" default="Points"></g:message></title>
    <meta name="layout" content="embedded"/>
    <style>
    </style>
</head>

<body>

<g:include view="_periodFilterNoRefresh.gsp"/>

<table id="Person-Grid"></table>
<div id="Person-Pager"></div>

<!-- dialog -->
<div id="competency-point-dialog">
    <table id="competency-point-grid"></table>
    <div id="competency-point-pager"></div>
</div>


<script type="text/javascript">

    // globals
    var personGrid = $('#Person-Grid');
    var competencyPointDialog = $('#competency-point-dialog');
    var competencyPointGrid = $('#competency-point-grid');
    var competencyPointPager = $('#competency-point-pager');
    var periodMonth = $('#periodMonth');
    var periodYear = $('#periodYear');
    var selPersonId = '';
    var personCompetencies = {};
    var positions = '';

    function initPersonGrid() {
        personGrid.jqGrid({
            url: '${request.getContextPath()}/person/getEvaluationList',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.person.skype.label" default="Skype"/>'
            ],
            colModel: [
                {name: 'firstName', index: 'firstName', searchoptions: {sopt: ['cn', 'nc']}},
                {name: 'lastName', index: 'lastName', searchoptions: {sopt: ['cn', 'nc']}},
                {name: 'level', index: 'level', searchoptions: {sopt: ['cn', 'nc']}},
                {name:'position', index:'position',editable:true,formatter:'select',edittype:'select', editoptions:{value:positions}, searchoptions: {sopt: ['cn', 'nc']}},
                {name:'skype', index:'skype', formatter: skypeFormatter, unformat: skypeUnformat, align: 'center'},

            ],
            height: 'auto',
            pager: '#Person-Pager',
            width: 550,
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'People',
            onSelectRow: function (rowid) {
                var rd = personGrid.jqGrid('getRowData', rowid);
                var personName = (rd.lastName) ? rd.firstName + ' ' + rd.lastName : rd.firstName;
                var period = 1 + '-' + periodMonth.val() + '-' + periodYear.val();
                var evaluationPeriod = periodMonth.find(':selected').text() + ' ' + periodYear.val();
                competencyPointGrid.jqGrid('setGridParam', {
                    url: '${request.getContextPath()}/competencyPoint/getList',
                    editurl: '${request.getContextPath()}/competencyPoint/edit',
                    postData: {personId: rowid, period: period}
                }).trigger('reloadGrid');
                // open dialog
                competencyPointDialog.dialog('option', {'title': 'Name: ' + personName + ' | Period: ' + evaluationPeriod}).dialog('open');
                selPersonId = rowid;
                // update person competencies
                competencyPointGrid.setColProp('competency',{editoptions:{value:getCompetencyOptions(rowid, period)}});
            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        personGrid.jqGrid(
                'navGrid', '#Person-Pager',
                {edit: false, add: false, del: false, view: false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );  

    }

    function initCompetencyPointGrid() {
        competencyPointGrid.jqGrid({
            url: '${request.getContextPath()}/competencyPoint/getList',
            datatype: 'json',
            mtype: 'POST',
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.point.startdate.label" default="Start Date"/>',
                '<g:message code="ucds.competency.point.enddate.label" default="End Date"/>',
                '<g:message code="ucds.competency.point.evaluator.label" default="Evaluator"/>',
                '<g:message code="ucds.competency.point.note.label" default="Note"/>'
            ],
            colModel: [
                {name:'competency', index:'competency', width: 100, editable:true, edittype:'select', formatter: 'select', editrules:{required:true},editoptions:{value:':'}},
                {name:'dateStart', index:'dateStart', width: 50, editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'}, editrules:{required:true}, editoptions:{readonly:'readonly', dataInit: initDatePicker}},
                {name:'dateEnd', index:'dateEnd', width: 50, editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'}, editrules:{required:true}, editoptions:{readonly:'readonly', dataInit: initDatePicker}},
                {name: 'evaluator', index: 'evaluator', width: 50, editable: false, searchoptions: {sopt: ['eq', 'ne', 'cn', 'nc']}},
                {name: 'note', index: 'note', hidden:true, editable:true, editrules:{edithidden:true}, edittype:'textarea', editoptions: {rows: 10, cols: 50}, searchoptions: {sopt: ['cn']}}
            ],
            height: 'auto',
            width: 750,
            pager: '#competency-point-pager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="COMPETENCY_POINTS"/>',
            sortname: 'dateCreated',
            sortorder: 'desc'
        });

        competencyPointGrid.jqGrid(
                'navGrid', '#competency-point-pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true,
                    width: 'auto'
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true,
                    width: 'auto'
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = personGrid.jqGrid('getGridParam','selrow');
                        var rd = personGrid.jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );

    }

    function getCompetencyOptions(personId, period) {
        var res = [];
        var options = getJSONDataSync('${request.getContextPath()}/personCompetency/needTrainingList', {personId: personId, period: period});
        for (var i=0; i<options.length; i++) {
            res.push(options[i].id + ':' + options[i].competency + ' Lv ' + options[i].level);
        }
        return res.join(';');
    }

    /* Init Dialog */

    function initDialog() {
        competencyPointDialog.dialog({
            autoOpen: false,
            modal: false,
            height: 'auto',
            width: 800,
            title: 'Competency Points',
            position: {my: 'center top', at: 'center top', of: window}
        }).parent().css({'font-size': '90%'}).css({'z-index': '200'});
    }

    $(document).ready(function () {
        $.ajax({
            type: 'GET',
            url:'${request.getContextPath()}/person/selectPositions'
        }).done(function(res){
            positions = res;
            initPersonGrid();
            initCompetencyPointGrid();
            initDialog();
        });
    });

</script>

</body>
</html>