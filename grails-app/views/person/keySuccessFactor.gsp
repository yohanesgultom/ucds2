<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="ucds.keysuccessfactor" default="Key Success Factor"></g:message></title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
    #required-competency-table tr {
        vertical-align: top;
    }

    #Key-Grid .cards {
        background-color: #FAF000;
    }

    #TblGrid_Key-Grid tr.FormData {
        height: 4em;
    }

    #TblGrid_Key-Grid td.DataTD {
        vertical-align: middle;
    }

    </style>
</head>
<body>

<table id="Person-Grid"></table>
<div id="Person-Pager"></div>

<!-- Key Success Factor Dialog -->
<div id="KeySuccessFactorDialog" style="display: none">
    %{--<p><span id="toggleBalanceScorecards">Toggle Score Cards</span></p>--}%
    <table id="Key-Grid"></table>
    <div id="Key-Pager"></div>
</div>

<script type="text/javascript">

    // globals
    var personGrid = $('#Person-Grid');
    var lastSel = '';
    var isShown = true;

    function initpersonGrid(){
        personGrid.jqGrid({
            url: '${request.getContextPath()}/person/getEvaluationList',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>'
            ],
            colModel :[
                {name:'firstName', index:'firstName',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'lastName', index:'lastName',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'level', index:'level'},
                {name:'position', index:'position'}
            ],
            height: 'auto',
            pager: '#Person-Pager',
            width: 550,
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'People',
            onSelectRow: function(rowid){
                var rd = personGrid.jqGrid('getRowData', rowid);
                var personName = (rd.lastName) ? rd.firstName + ' ' + rd.lastName : rd.firstName;
                var personPosition = rd.level + ' ' + rd.position;
                var listUrl = '${request.getContextPath()}/keySuccessFactor/getList';
                var editUrl = '${request.getContextPath()}/keySuccessFactor/edit?personId=' + rowid;
                $('#Key-Grid').jqGrid('setGridParam', {
                    url: listUrl,
                    editurl: editUrl,
                    postData: {searchField: 'person_id', searchOper: 'eq', searchString: rowid}
                }).trigger("reloadGrid");
                $('#KeySuccessFactorDialog').dialog('option', {'title' : 'Key Success Factor | Name: ' + personName + ' | Position: ' + personPosition}).dialog('open').scrollLeft('0');
            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        personGrid.jqGrid(
                'navGrid','#Person-Pager',
                {edit:false,add:false,del:false,view:true,search:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );

    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function initKeyGrid() {
        $("#Key-Grid").jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="app.keysuccessfactors.name.label" default="Key Success Factor"/>'
                ,'<g:message code="app.keysuccessfactors.kri.label" default="Key Result Indicator"/>'
                ,'<g:message code="app.keysuccessfactors.usekri.label" default="Use KRI"/>'
                <g:each in="${cards}">
                ,'${it.name}'
                </g:each>
                <g:each in="${categories}">
                ,'${it.name}'
                </g:each>
                ,'<g:message code="app.keysuccessfactors.percentage.label" default="Percentage"/>'
                ,'<g:message code="app.keysuccessfactors.selectedkpi.label" default="Selected KPI"/>'
            ],
            colModel: [
                {name: 'title', index: 'title', editable: true, edittype: 'textarea', formoptions: {colpos: 1, rowpos: 1}}
                ,{name: 'kri', index: 'kri', editable: true, edittype: 'textarea', formoptions: {colpos: 2, rowpos: 1}}
                ,{name: 'useKri', index: 'useKri', width: 30, editable: true, edittype: 'checkbox', editoptions:{value:'true:false'}, formoptions: {colpos: 2, rowpos: 2}}
                <g:each in="${cards}" var="card" status="i">
                ,{name: 'card_${card.id}', index: 'card_${card.id}', width: 50, editable: true, edittype: 'checkbox', editoptions:{value:'Yes:No'}, classes: 'cards', formoptions: {colpos: 1, rowpos: ${i+2}}}
                </g:each>
                <g:each in="${categories}" var="cat" status="i">
                ,{name: 'cat_${cat.id}', index: 'card_${cat.id}', width: 80, hidden: true, editable: true, edittype: 'textarea', editrules: {edithidden:true}, formoptions: {colpos: 2, rowpos: ${i+3}}}
                </g:each>
                ,{name: 'percentage', index: 'percentage', width: 50, editable: false, formatter: 'currency', formatoptions: {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, suffix: '%', defaultValue: ''}}
                ,{name: 'selectedKpi', index: 'selectedKpi', hidden: true, editable: true, edittype: 'textarea'}
            ],
            height: 'auto',
            pager: '#Key-Pager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Key Success Factors',
            onSelectRow: function (rowid) {
                var rd = $("#Key-Grid").jqGrid('getRowData', rowid);
            },
            sortname: 'title',
            sortorder: 'asc'
        });

        $('#Key-Grid').jqGrid(
                'navGrid', '#Key-Pager',
                {edit: true, add: true, del: true, view: false, search: false}, // options
                {
                    zIndex: 1000,
                    width: 800,
                    //afterShowForm: addCheckboxes,
                    //onclickSubmit: appendCheckboxValues,
                    onclickSubmit: appendSelectedKpi,
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    zIndex: 1000,
                    width: 800,
                    //afterShowForm: addCheckboxes,
                    //onclickSubmit: appendCheckboxValues,
                    onclickSubmit: appendSelectedKpi,
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#Key-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#Key-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata) {
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true, ""];
    }

    // @deprecated
    function addCheckboxes(formid) {
        // remove before add new ones
        $('input[name=selectedKpi]', $(formid)).remove();
        var selectedKpi = $('#selectedKpi', $(formid)).val();
        selectedKpi = (selectedKpi) ? selectedKpi.split(',') : [];
        var catIdStr = '<g:each in="${categories}">cat_${it.id},</g:each>';
        var catIds = catIdStr.split(',');
        for (var i = 0; i < catIds.length-1; i++) {
            var status = (selectedKpi.indexOf(catIds[i].split('_')[1]) >= 0) ? 'checked' : '';
            var cb = $('<input type="checkbox" id="' + catIds[i] + '_checkbox" name="selectedKpi" value="' + catIds[i].split('_')[1] + '" ' + status + '/>');
            $('#'+ catIds[i], $(formid)).parent().append('&nbsp;&nbsp;').append(cb);
        }
    }

    // @deprecated
    function appendCheckboxValues(params, postdata) {
        var selectedKpi = [];
        $('input[name=selectedKpi]:checked').each(function() {
            var item = '"' + this.value + '":"' + $(this).prev('textarea').val() + '"';
            selectedKpi.push(item);
        });
        return {selectedKpi: '{' + selectedKpi.join(',') + '}'};
    }

    function appendSelectedKpi(params, postdata) {
        // get selected kpi
        var selectedCategory = [];
        var selectedCard = [];
        var catIds = '<g:each in="${categories}">cat_${it.id},</g:each>'.split(',');
        var cardIds = '<g:each in="${cards}">card_${it.id},</g:each>'.split(',');

        for (var i = 0; i < catIds.length-1; i++) {
            // append value if not empty
            var value = $('#'+ catIds[i]).val();
            if (value) {
                var item = '"' + catIds[i].split('_')[1] + '":"' + value + '"';
                selectedCategory.push(item);
            }
        }

        for (var i = 0; i < cardIds.length-1; i++) {
            // append if checked
            var value = $('#' + cardIds[i] + ':checked').length;
            if (value === 1) {
                selectedCard.push(cardIds[i]);
            }
        }

        // calculate percentage
        // -1 due to additional commas
        var percentage = (selectedCategory.length + selectedCard.length) / ((catIds.length - 1) + (cardIds.length - 1)) * 100;

        // return only selected kpi & percentage
        return {selectedKpi: '{' + selectedCategory.join(',') + '}', percentage: percentage};
    }

    $(document).ready(function(){
        initpersonGrid();
        initKeyGrid();

        // dialog
        $('#KeySuccessFactorDialog').dialog({
            autoOpen: false,
            title: 'Key Success Factors',
            width: 830,
            height: 500,
            modal: false
        }).parent().css({'font-size' : '90%', 'z-index': 100});

        // button
        /*$('#toggleBalanceScorecards')
                .button({icons: {
                    primary: 'ui-icon-triangle-2-e-w'
                }})
                .click(function(){
                    if (isShown) {
                        $('#Key-Grid').jqGrid('hideCol',[<g:each in="${cards}">'card_${it.id}',</g:each>]);
                    } else {
                        $('#Key-Grid').jqGrid('showCol',[<g:each in="${cards}">'card_${it.id}',</g:each>]);
                    }
                    isShown = !isShown;
                });*/
    });

</script>


</body>
</html>