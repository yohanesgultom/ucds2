<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="ucds.kpi" default="Key Performance Indicator"></g:message></title>
    <meta name="layout" content="embedded"/>
    <style>
    #evaluation-form-dialog > span {
        text-align: right;
        margin-bottom: 10px;
    }
    </style>
</head>
<body>

<g:render template="/periodFilterNoRefresh"/>

<table id="Evaluation-Grid"></table>
<div id="Evaluation-Pager"></div>

<!-- Evaluation dialog -->
<div id="evaluation-form-dialog">
    <span id="reload-kpi-button"><g:message code="ucds.recruitment.deletekpi" default="Delete KPI"></g:message></span>
    <span id="calculate-weight"><g:message code="ucds.recruitment.calculateweight" default="Calculate Weight"></g:message></span>
    <table id="evaluation-form-grid"><tr><td>&nbsp;</td></tr></table>
    <div id="calculate-weight-dialog"></div>
</div>

<script type="text/javascript" src="${g.resource(file: 'js/sylvester.js')}"/>
<script type="text/javascript" src="${g.resource(file: 'js/sylvester.ahp.js')}"/>
<script type="text/javascript" src="${g.resource(file: 'js/jquery.jqgrid.ahp.js')}"/>
<script type="text/javascript">

    // globals
    var evaluationGrid = $('#Evaluation-Grid');
    var evaluationFormDialog = $('#evaluation-form-dialog');
    var evaluationFormGrid = $('#evaluation-form-grid');
    var periodMonth = $('#periodMonth');
    var periodYear = $('#periodYear');
    var reloadKpiButton = $('#reload-kpi-button');
    var calculateWeightButton = $('#calculate-weight');
    var calculateWeightDialog = $('#calculate-weight-dialog');
    var lastSel = '';

    function initEvaluationGrid(){

        evaluationGrid.jqGrid({
            url: '${request.getContextPath()}/person/getEvaluationList',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>'
            ],
            colModel :[
                {name:'firstName', index:'firstName',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'lastName', index:'lastName',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'level', index:'level'},
                {name:'position', index:'position'}
            ],
            height: 'auto',
            pager: '#Evaluation-Pager',
            width: 550,
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="ucds.people" default="People"></g:message>',
            onSelectRow: function(rowid){
                var rd = evaluationGrid.jqGrid('getRowData', rowid);
                var personName = rd.firstName + ' ' + rd.lastName;
                var filterParams = getFilterParams();
                var evaluationPeriod = getFilterPeriodForDisplay();
                var period = filterParams.period;
                var personPosition = rd.level + ' ' + rd.position;

                // prepare grid
                evaluationFormGrid.jqGrid('setGridParam', {
                    url: '${request.getContextPath()}/kpi/getEvaluationList',
                    editurl: '${request.getContextPath()}/kpi/edit?personId='+rowid+'&period='+period,
                    postData: {personId: rowid, period: period}
                }).trigger('reloadGrid');

                // prepare reset button
                reloadKpiButton
                        .button({icons: {primary: 'ui-icon-refresh'}})
                        .off()
                        .on('click', function() {
                            if (confirm('${g.message(code:"deletekpi", default:"Delete all KPI?")}'))
                    $.getJSON('${request.contextPath}/kpi/clearKpi',{personId:rowid,period:period},function(res) {
                        if (res) {
                            if (!res.success) {
                                alert(res.message);
                            } else {
                                evaluationFormGrid.trigger('reloadGrid');
                            }
                        } else {
                            alert('${g.message(code:"networkfailure", default:"Network failure. Please try again")}');
                        }
                    });
                });

                // calculate button
                JqGridAHP.initButton('kpi', calculateWeightButton, calculateWeightDialog, evaluationFormGrid);

                // open dialog
                evaluationFormDialog.dialog('option', {'title' : 'Key Performance Indicator | Name: ' + personName + ' | Position: ' + personPosition + ' | Period: ' + evaluationPeriod}).dialog('open');

            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        evaluationGrid.jqGrid(
                'navGrid','#Evaluation-Pager',
                {edit:false,add:false,del:false,view:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );

    }

    function initEvaluationFormGrid(){
        evaluationFormGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: false,
            colNames:[
                '<g:message code="app.kpi.personid.label" default="Position"/>',
                '<g:message code="app.kpi.kpi.label" default="KPI"/>',
                '<g:message code="app.kpi.unit.label" default="Unit"/>',
                '<g:message code="app.kpi.weight.label" default="Weight (%)"/>',
                '<g:message code="app.kpi.target.label" default="Target"/>',
                '<g:message code="app.kpi.actual.label" default="Actual"/>',
                '<g:message code="app.kpi.score.label" default="Score (%)"/>',
                '<g:message code="app.kpi.result.label" default="Result (%)"/>',
                '<g:message code="app.kpi.tools.label" default="Tools"/>',
                '<g:message code="app.kpi.reminder.label" default="Reminder (day)"/>',
                '<g:message code="app.kpi.locked.label" default="Locked"/>'
            ],
            colModel :[
                {name:'position',index:'position',hidden:true, editable: false},
                {name:'kpi',width:100,index:'kpi',editable:false},
                {name:'unitWorkload',width:30,index:'unitWorkload',editable:true},
                {name:'weight',width:20,index:'weight',editable:true, editrules: {number: true, required: true}},
                {name:'target',width:20,index:'target',editable:true, editrules: {number: true, required: true}},
                {name:'actual',width:20,index:'actual',editable:true, editrules: {number: true, required: true}},
                {name:'score',width:20,index:'score',editable:false},
                {name:'result',width:20,index:'result',editable:false,summaryType:'sum', summaryTpl:'<b>Total: {0}</b>'},
                {name:'tools',width:50,index:'tools',editable:true},
                {name:'reminderInDays',width:30,index:'reminderInDays',editable:true, editrules: {integer: true, required: true}},
                {name:'locked',index:'locked',hidden:true, editable: false}
            ],
            width: 980,
            height: 'auto',
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            grouping:true,
            groupingView : {
                groupField : ['position'],
                groupSummary : [true],
                groupColumnShow : [false],
                groupText : ['<b>Position: {0}</b>'],
                groupCollapse : false,
                groupOrder: ['asc']
            },
            caption: 'KPI Evaluation',
            onSelectRow: function(rowid){
                if(rowid && rowid!=lastSel){
                    evaluationFormGrid.jqGrid('saveRow',lastSel, {
                        aftersavefunc: afterSave
                    });
                    lastSel=rowid;
                }
                evaluationFormGrid.jqGrid('editRow', rowid, {
                    keys: true,
                    aftersavefunc: afterSave,
                    oneditfunc: function(rowid) {
                        var locked = evaluationFormGrid.jqGrid('getCell', rowid, 'locked');
                        $('#' + rowid + '_target').attr('readonly', (locked == 'true'));
                        $('#' + rowid + '_actual').attr('readonly', (locked == 'true'));
                    }
                });
            },
            sortname: 'id',
            sortorder: 'asc'
        });
    }

    evaluationFormDialog.dialog({
        autoOpen: false,
        height: 600,
        width: 1000,
        modal: true,
        title: 'Evaluation Form'
    }).parent().css({'font-size' : '90%'});

    // init calculate weight dialog
    JqGridAHP.initDialog('weight', calculateWeightDialog, evaluationFormGrid, 100);

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function afterSave(rowid, response) {
        evaluationFormGrid.trigger('reloadGrid');
        return true;
    }

    $(document).ready(function(){
        initEvaluationGrid();
        initEvaluationFormGrid();
    });

</script>


</body>
</html>