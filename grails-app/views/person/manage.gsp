<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="PEOPLE" /></title>
    <meta name="layout" content="embedded"/>
    <style>
    ul.ui-autocomplete {
        z-index: 3000;
        font-size: 90%;
    }
    .ui-autocomplete-loading {
        background: white url(${resource(dir: 'images', file: 'ui-anim_basic_16x16.gif')}) right center no-repeat;
    }
    </style>
</head>
<body>

<table id="Person-Grid"></table>
<div id="Person-Pager"></div>

<script type="text/javascript">

    // cache
    var personGrid = $('#Person-Grid');
    var positions = '';
    var people = [];
    var currentRowId;

    function split(term) {
        return term.split( /,\s*/ );
    }

    function extractLast(term) {
        return split(term).pop();
    }

    function initPersonGrid(){

        personGrid.jqGrid({
            url: '${request.getContextPath()}/person/getList',
            editurl: '${request.getContextPath()}/person/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.gender.label" default="Gender"/>',
                '<g:message code="ucds.person.dateOfBirth.label" default="Date of Birth"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.person.section.label" default="Section"/>',
                '<g:message code="ucds.person.email.label" default="Email"/>',
                '<g:message code="ucds.person.skype.label" default="Skype"/>',
                '<g:message code="ucds.person.subordinates.label" default="Subordinates"/>',
                '<g:message code="ucds.person.active.label" default="Active"/>',
            ],
            colModel :[
                {name:'firstName', index:'firstName',editable:true,edittype:'text',searchoptions:{sopt:['cn','nc']}},
                {name:'lastName', index:'lastName',editable:true,edittype:'text',searchoptions:{sopt:['cn','nc']}},
                {name:'gender', index:'gender',editable:true,formatter:'select',edittype:'select',editoptions:{value:'M:Male;F:Female'}},
                {name:'dateOfBirth', index:'dateOfBirth',editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'},editoptions:{readonly:'readonly', dataInit: initDOBDatePicker}},
                {name:'level', index:'level',editable:true, edittype:'text'},
                {name:'position', index:'personPosition.name',editable:true,formatter:'select',edittype:'select', editoptions:{value:positions}},
                {name:'section', index:'section',editable:true, edittype:'text'},
                {name:'email', index:'email',editable:true, edittype:'text', formatter: 'email', editrules: {email: true}},
                {name:'skype', index:'skype',editable:true, edittype:'text', formatter: skypeFormatter, unformat: skypeUnformat, align: 'center'},
                {name:'subordinates',index:'subordinates',hidden:true,editable:true,editrules:{edithidden:true},edittype:'textarea',editoptions:{
                    rows: 10,
                    cols: 50,
                    dataInit:function(el){
                        // don't navigate away from the field on tab when selecting an item
                        $(el).bind( "keydown", function( event ) {
                            if ( event.keyCode === $.ui.keyCode.TAB &&
                                    $( this ).data( "ui-autocomplete" ).menu.active ) {
                                event.preventDefault();
                            }
                        })
                        .autocomplete({
                            source: function( request, response ) {
                                var term = extractLast(request.term);
                                if (!people || people.length <= 0) {
                                    $.getJSON(
                                            "${request.getContextPath()}/person/getListForAutocomplete",
                                            {term: term},
                                            function (data) {
                                                people = data;
                                            }
                                    );
                                }
                                console.log(currentRowId);
                                // filter
                                var res = [];
                                var pattern = new RegExp('(' + term + ')', 'i');
                                for (var i = 0; i < people.length; i++) {
                                    if (people[i].value && people[i].value.match(pattern) && currentRowId != people[i].id) {
                                        res.push(people[i]);
                                    }
                                }
                                response(res);
                            },
                            search: function() {
                                // custom minLength
                                var term = extractLast( this.value );
                                if ( term.length < 2 ) {
                                    return false;
                                }
                            },
                            focus: function() {
                                // prevent value inserted on focus
                                return false;
                            },
                            select: function( event, ui ) {
                                var terms = split( this.value );
                                // remove the current input
                                terms.pop();
                                // add the selected item
                                terms.push( ui.item.value );
                                // add placeholder to get the comma-and-space at the end
                                terms.push( "" );
                                this.value = terms.join( ", " );
                                return false;
                            }
                        });
                    }
                }},
                {name:'active', index:'active', width: 10, editable:true, hidden: true, edittype:'checkbox',editoptions: {value: 'true:false', defaultValue: 'true'}, editrules: {edithidden:true}, searchoptions:{sopt:['eq']}}
            ],
            height: 'auto',
            pager: '#Person-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="PEOPLE" />',
            onSelectRow: function(rowid){
                currentRowId = rowid;
            },
            gridComplete: function(){

            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        personGrid.jqGrid(
            'navGrid','#Person-Pager',
            {edit:true,add:true,del:true,view:true}, // options
            {
                afterSubmit: afterSubmitForm,
                closeAfterEdit: true,
                width: 600
            }, // edit options
            {
                afterSubmit: afterSubmitFormClearCache,
                closeAfterAdd: true,
                width: 600
            }, // add options
            {
                afterSubmit: afterSubmitFormClearCache,
                onclickSubmit: function(params, posdata){
                    var id = $("#Person-Grid").jqGrid('getGridParam','selrow');
                    var rd = $("#Person-Grid").jqGrid('getRowData', id);
                    return {id : rd.id};
                }
            }, // del options
            {}, // search options
            {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function afterSubmitFormClearCache(response, postdata){
        // clear cache
        people = [];
        return afterSubmitForm(response, postdata)
    }

    $(function(){
        $.ajax({
            type: 'GET',
            url:'${request.getContextPath()}/person/selectPositions'
        }).done(function(res){
            positions = res;
            initPersonGrid();
        });
    });

</script>
</body>
</html>