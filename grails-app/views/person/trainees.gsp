<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="TRAINEES" default="Trainees"></g:message></title>
    <meta name="layout" content="embedded"/>
    <style>
    div.ui-jqdialog-content td.form-view-data {
        white-space: normal !important;
        height: auto;
        vertical-align: middle;
        padding-top: 3px; padding-bottom: 3px
    }
    </style>
</head>

<body>

<g:include view="_periodFilter.gsp"/>

<table id="Person-Grid"></table>
<div id="Person-Pager"></div>

<script type="text/javascript">

    // globals
    var personGrid = $('#Person-Grid');
    var periodMonth = $('#periodMonth');
    var periodYear = $('#periodYear');

    function initPersonGrid() {
        personGrid.jqGrid({
            url: '${request.getContextPath()}/person/getTraineeList',
            postData: getFilterParams(),
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.fullName.label" default="Name"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.person.section.label" default="Section"/>',
                '<g:message code="ucds.competency.name.label" default="Attitude"/>',
                '<g:message code="ucds.competency.level.label" default="Level"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>',
                '<g:message code="ucds.trainingmethod.label" default="Method"/>',
                '<g:message code="ucds.trainingmethod.description.label" default="Method Description"/>',
                '<g:message code="ucds.trainingProgram.startDate.label" default="Start Date"/>',
                '<g:message code="ucds.trainingProgram.endDate.label" default="End Date"/>',
                '<g:message code="ucds.person.email.label" default="Email"/>',
                '<g:message code="ucds.person.skype.label" default="Skype"/>',
            ],
            colModel :[
                {name:'firstName', index:'fullName'},
                {name:'position', index:'position'},
                {name:'section', index:'trainee.section'},
                {name:'competency', index:'competency'},
                {name:'level', index:'competencyLevel'},
                {name:'keyIndicator', index:'cl.keyIndicator', hidden: true, editrules: {edithidden: true}, formatter: wrapFormatter, sortable: false},
                {name:'trainingMethod', index:'trainingMethod'},
                {name:'trainingMethodDescription', index:'trainingMethodDescription', hidden: true, editrules: {edithidden: true}, formatter: wrapFormatter, sortable: false},
                {name:'startDate', index:'tp.startDate', formatter: 'date', formatoptions: {newformat: 'd-m-Y'}},
                {name:'endDate', index:'tp.endDate', formatter: 'date', formatoptions: {newformat: 'd-m-Y'}},
                {name:'email', index:'trainee.email', formatter: 'email', hidden: true, editrules: {edithidden: true}, sortable: false},
                {name:'skype', index:'trainee.skype', formatter: skypeFormatter, unformat: skypeUnformat, align: 'center', sortable: false}
            ],
            height: 'auto',
            pager: '#Person-Pager',
            width: 550,
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '${g.message(code:"TRAINING_PROGRAMS")}',
            sortname: 'fullName',
            sortorder: 'asc'
        });

        personGrid.jqGrid(
                'navGrid', '#Person-Pager',
                {edit: false, add: false, del: false, search: false, view: true, refresh: false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {
                    width: 600
                } // view options
        );

    }

    $('#reload').button({
        icons: {
            primary: 'ui-icon-refresh'
        }
    }).click(function () {
        personGrid.jqGrid('setGridParam', {
            postData: getFilterParams()
        }).trigger('reloadGrid');
    }).css({'font-size': '90%'});

    $(document).ready(function () {
        initPersonGrid()
    });

</script>

</body>
</html>