<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Evaluation</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="PersonCompetency-Grid"></table>
<div id="PersonCompetency-Pager"></div>

<script type="text/javascript">

    function initPersonCompetencyGrid(){
        $('#PersonCompetency-Grid').jqGrid({
            url: '${request.getContextPath()}/personCompetency/getList',
            editurl: '${request.getContextPath()}/personCompetency/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.personCompetency.name.label" default="Name"/>',
                '<g:message code="app.personCompetency.expectedScore.label" default="ExpectedScore"/>',
                '<g:message code="app.personCompetency.actualScore.label" default="ActualScore"/>',  
                '<g:message code="app.personCompetency.approval.label" default="Approval"/>',   
                '<g:message code="app.personCompetency.evaluator.label" default="Evaluator"/>',  
                '<g:message code="app.personCompetency.status.label" default="Status"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}} ,
                {name:'expectedScore', index:'expectedScore',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}} ,
                {name:'actualScore', index:'actualScore',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                {name:'approval', index:'approval',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,    
                {name:'evaluator', index:'evaluator',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,   
                {name:'status', index:'status',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}
            ],
            height: 'auto',
            pager: '#PersonCompetency-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'PersonCompetency',
            onSelectRow: function(rowid){
                var rd = $("#PersonCompetency-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#PersonCompetency-Grid').jqGrid(
                'navGrid','#PersonCompetency-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#PersonCompetency-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#PersonCompetency-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
    }
    }, // del options
    {}, // search options
    {} // view options
    );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initPersonCompetencyGrid();
    });

</script>
</body>
</html>