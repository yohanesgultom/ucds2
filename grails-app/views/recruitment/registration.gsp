<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="ucds.recruitmentregistration" default="Recruitment Registration"></g:message></title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="Candidate-Grid"></table>
<div id="Candidate-Pager"></div>

<div id="person-dialog">
    <table id="Person-Grid"></table>
    <div id="Person-Pager"></div>
</div>

<script type="text/javascript">

    var candidateGrid = $('#Candidate-Grid');
    var personDialog = $('#person-dialog');
    var personGrid = $('#Person-Grid');
    var personPositions = '';
    var personPickerTarget;

    function initCandidateGrid(){

        candidateGrid.jqGrid({
            url: '${request.getContextPath()}/recruitment/candidateList',
            editurl: '${request.getContextPath()}/recruitment/candidateEdit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.name.label" default="Name"/>',
                '<g:message code="ucds.person.gender.label" default="Gender"/>',
                '<g:message code="ucds.person.dateOfBirth.label" default="Date of Birth"/>',
                '<g:message code="ucds.recruitment.replacementfor" default="Replacement for"/>',
                '<g:message code="ucds.person.applicationlevel.label" default="Application Level"/>',
                '<g:message code="ucds.person.applicationposition.label" default="Application Position"/>',
                '<g:message code="ucds.person.applicationsection.label" default="Application Section"/>',
                '<g:message code="ucds.approval.status.label" default="Status"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['cn']}},
                {name:'gender', index:'gender',editable:true,formatter:'select',edittype:'select',editoptions:{value:'M:Male;F:Female'},search:false},
                {name:'dateOfBirth', index:'dateOfBirth',editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'},editoptions:{readonly:'readonly', dataInit: initDOBDatePicker},search:false},
                {name:'replacementfor',index:'replacementfor',hidden:true,editable:true,editrules:{edithidden:true},editoptions:{readonly:'readonly', dataInit: initPersonPicker}},
                {name:'level', index:'level',editable:true, edittype:'text',search:false},
                {name:'position', index:'position',editable:true, edittype:'select',editoptions:{value:getLOV('${request.contextPath}/recruitment/personPositions', personPositions, true)},formatter:'select',search:false},
                {name:'section', index:'section',editable:true, edittype:'text',search:false},
                {name:'status', index:'status',editable:false,searchoptions:{sopt:['eq']}}
            ],
            height: 'auto',
            pager: '#Candidate-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="ucds.recruitmentcandidates" default="Recruitment Candidates"></g:message>',
            sortname: 'name',
            sortorder: 'asc'
        });

        candidateGrid.jqGrid(
                'navGrid','#Candidate-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    zIndex:100,
                    height:'auto',
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    zIndex:100,
                    height:'auto',
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = candidateGrid.jqGrid('getGridParam','selrow');
                        var rd = candidateGrid.jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function initDialogs() {
        // init dialog itself
        personDialog.dialog({
            autoOpen:false,
            title:'${g.message(code:"ucds.recruitment.addforreplacement",default:"Add for Replacement")}',
            width:'auto',
            height:'auto'
        }).parent().css({'font-size': '90%'}).css({'z-index': '200'});

        // init grid inside dialog
        initPersonGrid();
    }

    function initPersonGrid(){
        personGrid.jqGrid({
            url: '${request.getContextPath()}/person/getList',
            editurl: '${request.getContextPath()}/person/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.gender.label" default="Gender"/>',
                '<g:message code="ucds.person.dateOfBirth.label" default="Date of Birth"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.person.section.label" default="Section"/>'
            ],
            colModel :[
                {name:'firstName', index:'firstName',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'lastName', index:'lastName',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'gender', index:'gender',editable:true,formatter:'select',edittype:'select',editoptions:{value:'M:Male;F:Female'}},
                {name:'dateOfBirth', index:'dateOfBirth',editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'},editoptions:{readonly:'readonly', dataInit: initDOBDatePicker}},
                {name:'level', index:'level',editable:true, edittype:'text'},
                {name:'position', index:'position',editable:true, edittype:'text'},
                {name:'section', index:'section',editable:true, edittype:'text'}
            ],
            height: 'auto',
            pager: '#Person-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '${g.message(code:"ucds.people",default:"People")}',
            onSelectRow: function (rowid) {
                var rowdata = $(this).jqGrid('getRowData', rowid);
                personPickerTarget.value = rowid + ':' + rowdata.firstName + ' ' + rowdata.lastName;
                personDialog.dialog('close');
            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        personGrid.jqGrid(
                'navGrid','#Person-Pager',
                {edit:false,add:false,del:false,view:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        var msg = (res.success) ? '' : res.message;
        return [res.success,msg];
    }

    function initPersonPicker(elm) {
        setTimeout(function () {
            $(elm).click(function() {
                personPickerTarget = elm;
                personDialog.dialog('open');
            });
        }, 200);
    }

    $(function(){
        initCandidateGrid();
        initDialogs();
    });

</script>
</body>
</html>