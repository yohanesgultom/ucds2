<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="ucds.recruitment.requiredcompetency" default="Candidate's Required Competency"/></title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
    #required-competency-table tr {
        vertical-align: top;
    }
    </style>
</head>
<body>

<table id="Candidate-Grid"></table>
<div id="Candidate-Pager"></div>

<div id="required-competency-dialog">
    <table id="required-competency-table" width="920px">
        <tr>
            <td width="50%">
                <table id="required-competency-grid" width="100%"></table>
                <div id="required-competency-pager"></div>
            </td>
            <td width="50%">
                <table id="competency-grid" width="100%"></table>
                <div id="competency-pager"></div>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">

    // globals
    var candidateGrid = $('#Candidate-Grid');
    var competencyGrid = $("#competency-grid");
    var requiredCompetencyGrid = $('#required-competency-grid');
    var competencyDialog = $('#required-competency-dialog');
    var lastSel = '';
    var personPositions = '';

    function initCandidateGrid(){
        candidateGrid.jqGrid({
            url: '${request.getContextPath()}/recruitment/newCandidateList',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.name.label" default="Name"/>',
                '<g:message code="ucds.person.gender.label" default="Gender"/>',
                '<g:message code="ucds.person.dateOfBirth.label" default="Date of Birth"/>',
                '<g:message code="ucds.person.applicationlevel.label" default="Application Level"/>',
                '<g:message code="ucds.person.applicationposition.label" default="Application Position"/>',
                '<g:message code="ucds.person.applicationsection.label" default="Application Section"/>',
                '<g:message code="ucds.approval.requester.label" default="Requester"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['cn']}},
                {name:'gender', index:'gender',editable:true,formatter:'select',edittype:'select',editoptions:{value:'M:Male;F:Female'},search:false},
                {name:'dateOfBirth', index:'dateOfBirth',editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'},editoptions:{readonly:'readonly', dataInit: initDOBDatePicker},search:false},
                {name:'level', index:'level',editable:true, edittype:'text',search:false},
                {name:'position', index:'position',editable:true, edittype:'select',editoptions:{value:getLOV('${request.contextPath}/recruitment/personPositions', personPositions, true)},formatter:'select',search:false},
                {name:'section', index:'section',editable:true, edittype:'text',search:false},
                {name:'requester', index:'requester',editable:false,search:false}
            ],
            height: 'auto',
            pager: '#Candidate-Pager',
            width: 550,
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="ucds.recruitmentcandidates" default="Recruitment Candidates"></g:message>',
            onSelectRow: function(rowid){
                var rd = candidateGrid.jqGrid('getRowData', rowid);
                var name = rd.name;
                var listUrl = '${request.getContextPath()}/recruitment/getRequiredCompetency';
                requiredCompetencyGrid.jqGrid('setGridParam', {
                    url: listUrl,
                    postData: {candidateId: rowid}
                }).trigger("reloadGrid");
                competencyDialog.dialog('option', {'title' : '<g:message code="COMPETENCY_REQUIRED"/> for : ' + name}).dialog('open');
            },
            sortname: 'name',
            sortorder: 'asc'
        });

        candidateGrid.jqGrid(
                'navGrid','#Candidate-Pager',
                {edit:false,add:false,del:false,view:false,search:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );

    }

    function initRequiredCompetencyGrid() {
        requiredCompetencyGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>'
            ],
            colModel: [
                {name: 'name', index: 'name', editable: true, edittype: 'text'},
                {name: 'keyIndicator', index: 'keyIndicator', hidden: true, editable: true, edittype: 'textarea', editrules: {edithidden: true}}
            ],
            height: 'auto',
            pager: '#required-competency-pager',
            rowNum: 99,
            rowList: [99],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="ucds.competency.required" default="Required Competency"></g:message>',
            sortname: 'name',
            sortorder: 'asc'
        });

        requiredCompetencyGrid.jqGrid(
                'navGrid', '#required-competency-pager',
                {edit: false,add:false,del:false,view:false,search:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        ).jqGrid('navSeparatorAdd', '#required-competency-pager',{sepclass : 'ui-separator',sepcontent: ''})
                .jqGrid('navButtonAdd','#required-competency-pager',{ caption:'Remove', buttonicon:'ui-icon-circle-triangle-e', onClickButton:removeRequiredCompetency, position: 'last', title:'', cursor: 'pointer'});
    }

    function initCompetencyGrid() {
        competencyGrid.jqGrid({
            url: '${request.getContextPath()}/competency/getFilteredList',
            postData: {searchField: 'type', searchOper: 'eq', searchString: 'Technical'},
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>'
            ],
            colModel: [
                {name: 'name', index: 'name', editable: true, edittype: 'text', searchoptions: {sopt: ['eq', 'ne', 'cn', 'nc']}},
                {name: 'keyIndicator', index: 'keyIndicator', hidden: true, editable: true, edittype: 'textarea', editrules: {edithidden: true}, searchoptions: {sopt: ['cn', 'nc']}}
            ],
            height: 'auto',
            pager: '#competency-pager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="ucds.competency" default="Competency"></g:message>',
            sortname: 'name',
            sortorder: 'asc'
        });

        competencyGrid.jqGrid(
                'navGrid', '#competency-pager',
                {edit:false,add:false,del:false,view:false,search:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        ).jqGrid('navSeparatorAdd', '#competency-pager',{sepclass : 'ui-separator',sepcontent: ''})
                .jqGrid('navButtonAdd','#competency-pager',{ caption:'Add', buttonicon:'ui-icon-circle-triangle-w', onClickButton:addRequiredCompetency, position: 'last', title:'', cursor: 'pointer'});
    }

    /* Init Dialog */

    function initDialog() {
        competencyDialog.dialog({
            autoOpen: false,
            height: 600,
            width: 950,
            modal: true,
            title: '<g:message code="ucds.competency.required" default="Required Competency"></g:message>'
        }).parent().css({'font-size' : '90%'});
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function addRequiredCompetency(){
        var competencyRowid = competencyGrid.jqGrid('getGridParam', 'selrow');
        if (competencyRowid) {
            var personRowid = candidateGrid.jqGrid('getGridParam', 'selrow');
            var data = {candidateId: personRowid, competencyId: competencyRowid};
            var addUrl = '${request.getContextPath()}/recruitment/addRequiredCompetency';
            $.post(addUrl, data, function(response) {
                if (response) {
                    //alert(response.message);
                    requiredCompetencyGrid.trigger("reloadGrid");
                } else {
                    alert('error');
                }
            });
        } else {
            alert('Please select a record');
        }
    }

    function removeRequiredCompetency(){
        var competencyRowid = requiredCompetencyGrid.jqGrid('getGridParam', 'selrow');
        if (competencyRowid) {
            var personRowid = candidateGrid.jqGrid('getGridParam', 'selrow');
            var data = {candidateId: personRowid, competencyId: competencyRowid};
            var addUrl = '${request.getContextPath()}/recruitment/removeRequiredCompetency';
            $.post(addUrl, data, function(response) {
                if (response) {
                    //alert(response.message);
                    requiredCompetencyGrid.trigger("reloadGrid");
                } else {
                    alert('error');
                }
            });
        } else {
            alert('Please select a record');
        }
    }

    $(document).ready(function(){
        initCandidateGrid();
        initRequiredCompetencyGrid();
        initCompetencyGrid();
        initDialog();
    });

</script>


</body>
</html>