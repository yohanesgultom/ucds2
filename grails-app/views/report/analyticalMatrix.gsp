<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="ANALYTICAL_MATRIX" default="Analytical Matrix"></g:message></title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/tablecloth/css', file: 'tablecloth.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'js/jqueryplot', file: 'jquery.jqplot.min.css')}" type="text/css">
    <style type="text/css">
    td.total-cell, th.total-cell {
        background-color: #DA4F49;
        color: white;
        text-align: center;
    }

    table#core-analytical-matrix th,
    table#managerial-analytical-matrix th,
    table#technical-analytical-matrix th {
        background-color: #e2f9d3;
    }

    td.center {
        text-align: center;
        font-weight: bolder;
    }

    td.ok {
        background-color: white;
        color: black;
    }

    td.not-ok {
        background-color: black;
        color: white;
    }

    #show-quality-chart, #show-competency-chart {
        background: url(${resource(dir: 'images', file: 'chart_bar.png')}) center left no-repeat;
        padding-left: 22px;
        margin-right: 33px;
    }

    p {
        font-size: 11px;
        font-weight: bolder;
        color: grey;
    }
    </style>
</head>

<body>

<g:render template="/periodAndEmployeeFilter" />
<h4><g:message code="ucds.competency.core" default="Core Competency"/></h4>
<table id="core-analytical-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<h4><g:message code="ucds.competency.managerial" default="Managerial Competency"/></h4>
<table id="managerial-analytical-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<h4><g:message code="ucds.competency.technical" default="Technical Competency"/></h4>
<table id="technical-analytical-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<script>
    var coreAnalyticalMatrixTable = $('#core-analytical-matrix');
    var managerialAnalyticalMatrixTable = $('#managerial-analytical-matrix');
    var techAnalyticalMatrixTable = $('#technical-analytical-matrix');

    $(document).ready(function () {

        // initialize page
        var params = getFilterParams();
        loadAnalyticalMatrix(params);
        //loadCharts(params);

        $('table.matrix').tablecloth({
            theme: 'stats',
            bordered: true,
            condensed: true
        });

        $('#reload').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        }).click(function () {
            var params = getFilterParams();
            loadAnalyticalMatrix(params);
        }).css({'font-size': '90%'});

    });

    function loadAnalyticalMatrix(params) {
        var tables = {
            Core: coreAnalyticalMatrixTable,
            Managerial: managerialAnalyticalMatrixTable,
            Technical: techAnalyticalMatrixTable
        }

        // show loading message
        for (var type in tables) {
            var table = tables[type]
            table.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        }

        // get data from server
        $.get('${g.createLink(controller: "report", action: "getAnalyticalMatrixData")}', params, function (response) {
            if (response) {
                // get available types
                var availableTypes = getAvailableCompetencyTypes(response);
                console.log(availableTypes);
                for (var type in tables) {
                    var table = tables[type]
                    if (availableTypes.indexOf(type) >= 0) {
                        populateAnalyticalMatrix(response, table, type);
                        table.prev().show();
                        table.show();
                    } else {
                        table.prev().hide();
                        table.hide();
                    }
                }
            } else {
                for (var type in tables) {
                    var table = tables[type]
                    populateMessage(table, 'Failed to load data');
                }
            }
        });
    }

    function getAvailableCompetencyTypes(response) {
        var availableTypes = []
        for (var key in response.header) {
            var value = response.header[key]
            if (availableTypes.indexOf(value.competencyType) < 0) {
                availableTypes.push(value.competencyType)
            }
            // stop once all types captured
            if (availableTypes.length >= 3) break
        }
        return availableTypes
    }

    function populateMessage(table, message) {
        table.empty();
        table.append('<tr><td>' + message + '</td></tr>');
    }

    function populateAnalyticalMatrix(response, table, type) {
        // header & subheader
        var header = '<th rowspan=2>#</th><th rowspan=2>Name</th><th rowspan=2>Position</th>';
        var subheader = '';
        for (var key in response.header) {
            var value = response.header[key]
            if (value.competencyType == type) {
                header += '<th colspan=' + value.competencyLevel + '>' + value.competencyName + '</th>';
                if (value.competencyLevel > 1) {
                    for (var i = 1; i <= value.competencyLevel; i++) {
                        subheader += '<th>Level ' + i + '</th>';
                    }
                } else {
                    subheader += '<th></th>';
                }
            }
        }
        // add total column
        header += '<th rowspan=2>Total</th>';

        // close header & subheader
        header = '<tr>' + header + '</tr>';
        subheader = '<tr>' + subheader + '</tr>';

        // body
        var body = '';
        var totalByCompetency = new Array();
        $(response.rows).each(function (rowKey, rowValue) {
            var totalByPerson = 0;
            rowKey += 1;
            var row = '<td>' + rowKey + '</td>' + '<td>' + rowValue.personName + '</td>' + '<td>' + rowValue.personPosition + '</td>';
            for (var headerKey in response.header) {
                var headerValue = response.header[headerKey];
                if (headerValue.competencyType == type) {
                    var competencyScore = rowValue.competencyScore[headerKey];
                    for (var i = 1; i <= headerValue.competencyLevel; i++) {
                        var mark = ((competencyScore && competencyScore.expected == i) || (competencyScore && competencyScore.expected && headerValue.competencyLevel <= 1)) ? competencyScore.gap : '';
                        var classAttr = 'center';
                        var totalByCompetencyKey = headerKey + '' + i;
                        classAttr += (mark != '' && competencyScore) ? ' ' + getClass(competencyScore.gap) : '';
                        classAttr = ' class="' + classAttr + '"';
                        row += '<td' + classAttr + '>' + mark + '</td>';
                        if (mark) {
                            totalByPerson += competencyScore.gap;
                            totalByCompetency[totalByCompetencyKey] = (totalByCompetency[totalByCompetencyKey]) ? totalByCompetency[totalByCompetencyKey] : 0;
                            totalByCompetency[totalByCompetencyKey] += competencyScore.gap;
                        }
                    }
                }
            }
            // append total cell
            row += '<td class="total-cell">' + totalByPerson + '</td>';
            // close row
            body += '<tr>' + row + '</tr>';
        });

        // add total row
        var totalRow = '<td colspan="3"></td>';
        for (var headerKey in response.header) {
            var headerValue = response.header[headerKey];
            if (headerValue.competencyType == type) {
                for (var i = 1; i <= headerValue.competencyLevel; i++) {
                    var totalByCompetencyKey = headerKey + '' + i;
                    var total = (totalByCompetency[totalByCompetencyKey]) ? totalByCompetency[totalByCompetencyKey] : 0;
                    totalRow += '<td class="total-cell">' + total + '</td>';
                }
            }
        }
        totalRow = '<tr>' + totalRow + '<td></td>' + '</tr>';
        body += totalRow;

        // merge
        table.empty();
        table.append(header + subheader + body);
    }

    function getClass(gap) {
        var res;
        if (gap >= 0) {
            res = 'ok';
        } else if (gap < 0) {
            res = 'not-ok';
        }
        return res;
    }

</script>

</body>
</html>