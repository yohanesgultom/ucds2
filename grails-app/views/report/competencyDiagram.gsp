<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="COMPETENCY_DIAGRAM" default="Competency Diagram"></g:message></title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/jqueryplot', file: 'jquery.jqplot.min.css')}" type="text/css">
</head>

<body>

<g:render template="/periodAndEmployeeFilter" />

<div class="info">* ${g.message(code: "info.competencyDiagram")}</div>
<div id="competency-chart" class="chart"></div>

<script>
    var competencyPlot = null;
    var competencyChart = $('#competency-chart');

    $(document).ready(function () {

        // initialize page
        var params = getFilterParams();
        loadCharts(params);

        $('#reload').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        }).click(function () {
            var params = getFilterParams();
            loadCharts(params);
        }).css({'font-size': '90%'});

    });

    function loadCharts(params) {
        competencyChart.html(spinner);

        $.getJSON('${request.getContextPath()}/report/getCompetencyDiagram', params, function (diagram) {
            var data = diagram.data;
            var ticks = diagram.ticks;
            competencyChart.html('');
            competencyPlot = $.jqplot('competency-chart', data, {
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    rendererOptions: {fillToZero: true, barWidth: 30, varyBarColor: true, animation: {speed: 1000}},
                    pointLabels: { show: true, location: 'n', edgeTolerance: -15 }
                },
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        fontSize: '8pt'
                    }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        label: 'Score',
                        max: 0
                    }
                },
                animate: true
            });
        });

    }

</script>

</body>
</html>