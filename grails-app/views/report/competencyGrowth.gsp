<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="COMPETENCY_GROWTH"/></title>
    <meta name="layout" content="embedded"/>
    <style>
    </style>
</head>
<body>

<g:render template="/periodFilterBeforeAfter" />

<table id="CompetencyGrowth-Grid" width="100%"></table>
<div id="CompetencyGrowth-Pager" width="100%"></div>

<script type="text/javascript">

    var exportUrl = '${request.getContextPath()}/report/exportCompetencyGrowth'
    var competencyGrowthGrid = $('#CompetencyGrowth-Grid')

    function initCompetencyGrowthGrid(){
        competencyGrowthGrid.jqGrid({
            url: '${request.getContextPath()}/report/getCompetencyGrowth',
            postData: getFilterParams(),
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.fullName.label" default="Full Name"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.person.section.label" default="Section"/>',
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.actualScoreBefore.label" default="Score Before"/>',
                '<g:message code="ucds.competency.actualScoreAfter.label" default="Score After"/>',
                '<g:message code="ucds.competency.actualScoreGrowth.label" default="Growth"/>'
            ],
            colModel :[
                {name:'fullName', index:'p.fullName', stype:'', searchoptions:{sopt:['cn','nc']}},
                {name:'position', index:'pos.name', searchoptions:{sopt:['cn','nc']}},
                {name:'section', index:'p.section', searchoptions:{sopt:['cn','nc']}},
                {name:'competency', index:'c.name', searchoptions:{sopt:['cn','nc']}},
                {name: 'actualScoreBefore', width: 50, index: 'pcb.actualScore', searchoptions:{sopt:['eq','gt','ge','lt','le']}, searchrules:{number: true}},
                {name: 'actualScoreAfter', width: 50, index: 'pca.actualScore', searchoptions:{sopt:['eq','gt','ge','lt','le']}, searchrules:{number: true}},
                {name: 'actualScoreGrowth', width: 50, index: '(pca.actualScore - pcb.actualScore)', searchoptions:{sopt:['eq','gt','ge','lt','le']}, searchrules:{number: true}}
            ],
            height: 'auto',
            pager: '#CompetencyGrowth-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="PEOPLE" default="People"/>',
            sortname: 'firstName',
            sortorder: 'asc'
        })

        competencyGrowthGrid.jqGrid(
                'navGrid','#CompetencyGrowth-Pager',
                {edit: false, add: false, del: false, search: true, view: false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        )
    }

    // reload button
    $('#reload').button({
        icons: {
            primary: 'ui-icon-refresh'
        }
    }).click(function () {
        var params = getFilterParams()
        competencyGrowthGrid.jqGrid('setGridParam', {
            postData: params
        }).trigger("reloadGrid")
    }).css({'font-size': '90%'})

    // export button
    $('#export').button({
        icons: {
            primary: 'ui-icon-document'
        }
    }).click(function() {
        // get current grid search parameters
        // and apply them to exported data
        var postData = competencyGrowthGrid.jqGrid('getGridParam', 'postData')
        console.log(postData)
        window.open(exportUrl + '?' + $.param(postData))
    }).css({'font-size' : '90%'})

    // main
    $(function(){
        initCompetencyGrowthGrid()
    })

</script>

</body>
</html>