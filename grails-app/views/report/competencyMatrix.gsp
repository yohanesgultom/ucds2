<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="COMPETENCY_MATRIX" default="Competency Matrix"></g:message></title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/tablecloth/css', file: 'tablecloth.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'js/tablecloth/css', file: 'prettify.css')}" type="text/css">
    <style>
    table#core-competency-matrix th,
    table#managerial-competency-matrix th,
    table#technical-competency-matrix th {
        background-color: #e2f9d3;
    }

    td.center {
        text-align: center;
        font-weight: bolder;
    }

    td.satisfying {
        background-color: yellow;
    }

    td.outstanding {
        background-color: aquamarine;
    }

    td.need-training {
        background-color: red;
    }

    p {
        font-size: 11px;
        font-family: verdana;
        font-weight: bolder;
        color: grey;
    }
    </style>
</head>

<body>

<g:render template="/periodAndEmployeeFilter" />

<table class="legend">
    <tr><td class="outstanding">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><g:message
            code="ucds.competency.result.outstanding" default="Outstanding"/></td></tr>
    <tr><td class="satisfying">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><g:message
            code="ucds.competency.result.satisfying" default="Satisfying"/></td></tr>
    <tr><td class="need-training">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><g:message
            code="ucds.competency.result.needtraining" default="Need Training"/></td></tr>
</table>

<h4><g:message code="ucds.competency.core" default="Core Competency"/></h4>
<table id="core-competency-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<h4><g:message code="ucds.competency.managerial" default="Managerial Competency"/></h4>
<table id="managerial-competency-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<h4><g:message code="ucds.competency.technical" default="Technical Competency"/></h4>
<table id="technical-competency-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<script type="text/javascript">
    var coreCompetencyMatrixTable = $('#core-competency-matrix');
    var managerialCompetencyMatrixTable = $('#managerial-competency-matrix');
    var techCompetencyMatrixTable = $('#technical-competency-matrix');

    $(function () {
        $('table.matrix').tablecloth({
            theme: 'stats',
            bordered: true,
            condensed: true
        });

        $('table.legend').tablecloth({
            theme: 'stats',
            condensed: true
        });

        $('#reload').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        }).click(function () {
            var params = getFilterParams();
            loadCompetencyMatrix(params);
        }).css({'font-size': '90%'});

        var params = getFilterParams();
        loadCompetencyMatrix(params);

    });

    function loadCompetencyMatrix(params) {
        var tables = {
            Core: coreCompetencyMatrixTable,
            Managerial: managerialCompetencyMatrixTable,
            Technical: techCompetencyMatrixTable
        }

        // show loading message
        for (var type in tables) {
            var table = tables[type]
            table.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        }

        // get data from server
        $.get('${g.createLink(controller: "report", action: "getCompetencyMatrixData")}', params, function (response) {
            if (response) {
                // get available types
                var availableTypes = getAvailableCompetencyTypes(response);
                console.log(availableTypes);
                for (var type in tables) {
                    var table = tables[type]
                    if (availableTypes.indexOf(type) >= 0) {
                        populateCompetencyMatrix(response, table, type);
                        table.prev().show();
                        table.show();
                    } else {
                        table.prev().hide();
                        table.hide();
                    }
                }
            } else {
                for (var type in tables) {
                    var table = tables[type]
                    populateMessage(table, '${g.message(code: "message.error.failedToLoadData", default: "Failed to load data")}');
                }
            }
        }).fail(function() {
            for (var type in tables) {
                var table = tables[type]
                populateMessage(table, '${g.message(code: "message.error.failedToLoadData", default: "Failed to load data")}');
            }
        })
    }

    function getAvailableCompetencyTypes(response) {
        var availableTypes = []
        for (var key in response.header) {
            var value = response.header[key]
            if (availableTypes.indexOf(value.competencyType) < 0) {
                availableTypes.push(value.competencyType)
            }
            // stop once all types captured
            if (availableTypes.length >= 3) break
        }
        return availableTypes
    }

    function populateMessage(table, message) {
        table.empty();
        table.append('<tr><td>' + message + '</td></tr>');
    }

    function populateCompetencyMatrix(response, table, type) {
        // header & subheader
        var header = '<th rowspan=2>#</th><th rowspan=2>Name</th><th rowspan=2>Position</th>';
        var subheader = '';
        for (var key in response.header) {
            var value = response.header[key]
            if (value.competencyType == type) {
                header += '<th colspan=' + value.competencyLevel + '>' + value.competencyName + '</th>';
                if (value.competencyLevel > 1) {
                    for (var i = 1; i <= value.competencyLevel; i++) {
                        subheader += '<th>Level ' + i + '</th>';
                    }
                } else {
                    subheader += '<th></th>';
                }
            }
        }
        header = '<tr>' + header + '</tr>';
        subheader = '<tr>' + subheader + '</tr>';

        // body
        var body = '';
        $(response.rows).each(function (rowKey, rowValue) {
            rowKey += 1;
            var row = '<td>' + rowKey + '</td>' + '<td>' + rowValue.personName + '</td>' + '<td>' + rowValue.personPosition + '</td>';
            for (var headerKey in response.header) {
                var headerValue = response.header[headerKey];
                if (headerValue.competencyType == type) {
                    var competencyScore = rowValue.competencyScore[headerKey];
                    for (var i = 1; i <= headerValue.competencyLevel; i++) {
                        var mark = ((competencyScore && competencyScore.expected == i) || (competencyScore && competencyScore.expected && headerValue.competencyLevel <= 1)) ? 'X' : '';
                        var classAttr = 'center';
                        classAttr += (mark != '' && competencyScore) ? ' ' + getClass(competencyScore.gap) : '';
                        classAttr = ' class="' + classAttr + '"';
                        row += '<td' + classAttr + '>' + mark + '</td>';
                    }
                }
            }
            body += '<tr>' + row + '</tr>';
        });

        // merge
        table.empty();
        table.append(header + subheader + body);
    }

    function getClass(gap) {
        var res;
        if (gap == 0) {
            res = 'satisfying';
        } else if (gap > 0) {
            res = 'outstanding';
        } else if (gap < 0) {
            res = 'need-training';
        }
        return res;
    }
</script>
</body>
</html>