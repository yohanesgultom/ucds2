<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="QUALITY_DIAGRAM" default="Quality Diagram"></g:message></title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/jqueryplot', file: 'jquery.jqplot.min.css')}" type="text/css">
</head>

<body>

<g:render template="/periodAndEmployeeFilter" />

<div class="info">* ${g.message(code: "info.qualityDiagram")}</div>
<div id="quality-chart" class="chart"></div>

<script>
    var qualityPlot = null;
    var qualityChart = $('#quality-chart');

    $(document).ready(function () {

        // initialize page
        var params = getFilterParams();
        loadCharts(params);

        $('#reload').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        }).click(function () {
            var params = getFilterParams();
            loadCharts(params);
        }).css({'font-size': '90%'});

    });

    function loadCharts(params) {
        qualityChart.html(spinner);

        $.getJSON('${request.getContextPath()}/report/getQualityDiagram', params, function (diagram) {
            var data = diagram.data;
            var ticks = diagram.ticks;
            var series = diagram.series;
            qualityChart.html('');
            qualityPlot = $.jqplot('quality-chart', data, {
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    rendererOptions: {fillToZero: true, animation: {speed: 1000}},
                    pointLabels: { show: true, location: 'n', edgeTolerance: -15 }
                },
                series: series,
                legend: {
                    show: true,
                    placement: 'insideGrid'
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        label: 'People',
                        min: 0,
                        tickOptions: {formatString: '%d'}
                    }
                },
                animate: true
            });
        });

    }

</script>

</body>
</html>