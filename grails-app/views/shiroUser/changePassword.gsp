<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="CHANGE_PASSWORD"/></title>
    <meta name="layout" content="embedded"/>
    <style>
    table {
        font-size: 80%;
        vertical-align: middle;
    }
    table td {
        padding-right: 2.5em;
        padding-bottom: 0.5em;
    }
    </style>
</head>

<body>

<table>
    <tr>
        <td colspan="2" class="info">* ${g.message(code: "user.info.password")}</td>
    </tr>
    <tr>
        <td>${g.message(code: "user.currentPassword", default: "Current Password")} </td>
        <td><input id="current-password" type="password" /></td>
    </tr>
    <tr>
        <td>${g.message(code: "user.newPassword", default: "New Password")} </td>
        <td><input id="new-password" type="password" /></td>
    </tr>
    <tr>
        <td>${g.message(code: "user.confirmNewPassword", default: "Confirm New Password")} </td>
        <td><input id="confirm-new-password" type="password"/></td>
    </tr>
    <tr><td colspan="2"></td></tr>
    <tr>
        <td><div id="submit-change-password">${g.message(code: "default.button.submit", default: "Submit")}</div></td>
    </tr>
</table>

<script type="text/javascript">
    // define button
    $('#submit-change-password').button({
        icons: {
            primary: 'ui-icon-check'
        }
    }).click(function() {
        var minLength = 5
        var currentPassword = $('#current-password').val()
        var newPassword = $('#new-password').val()
        var confirmNewPassword = $('#confirm-new-password').val()
        var error = false
        $('input[type=password]').each(function (index, elmt) {
            if (!$(elmt).val() || $(elmt).val().length < minLength) {
                $(elmt).css('border-color', 'tomato')
                error = true
            } else {
                $(elmt).css('border-color', '')
            }
        })
        if (!error && newPassword != confirmNewPassword) {
            alert('${g.message(code: "default.passwordnotmatch.message", default: "Confirmation password does not match")}')
            error = true
        }
        if (!error && newPassword == currentPassword) {
            alert('${g.message(code: "default.nopasswordchange.message", default: "New password is the same as current password")}')
            error = true
        }
        // change password
        if (!error) {
            var data = {
                currentPassword: currentPassword,
                newPassword: newPassword,
                confirmNewPassword: confirmNewPassword
            }
            $.post('${request.getContextPath()}/shiroUser/changePassword', data, function(res) {
                alert(res.message)
            })
        } else {
            $('#confirm-new-password').css('border-color', 'tomato')
        }
    }).css({'font-size' : '90%'})

    // init
    $(document).ready(function() {
        $('#current-password').focus()
    })
</script>
</body>
</html>