<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="USER_MANAGEMENT"/></title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="ShiroUser-Grid" width="100%"></table>
<div id="ShiroUser-Pager" width="100%"></div>

<script type="text/javascript">

    function initShiroUserGrid(){
        $("#ShiroUser-Grid").jqGrid({
            url: '${request.getContextPath()}/shiroUser/getList',
            editurl: '${request.getContextPath()}/shiroUser/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.shiroUser.username.label" default="Username"/>',
                '<g:message code="app.shiroUser.personId.label" default="Person ID"/>',
                '<g:message code="app.shiroUser.person.label" default="Person"/>',
                '<g:message code="app.shiroUser.password.label" default="Password"/>',
                '<g:message code="app.shiroUser.permissions.label" default="Permissions"/>'
            ],
            colModel :[
                {name:'username', width: 100, index:'username', editable:true, edittype:'text', searchoptions:{sopt:['cn','nc']}},
                {name:'personId', index:'personId', editable:true, edittype:'text', hidden: true},
                {name:'person', width: 100, index:'person', editable:true, edittype:'text', search: false, editoptions: {dataInit: personAutoCompleteInit}},
                {name:'password', index:'password', editable:true, edittype:'password', hidden:true, editrules: {edithidden: true}},
                {name:'permissions', width: 600, index:'permissions', editable:true, edittype:'textarea', editoptions:{rows:'12', cols:'100'}, search: false}
            ],
            height: 'auto',
            pager: '#ShiroUser-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="users" default="Users"/>',
            onSelectRow: function(rowid){
                var rd = $("#ShiroUser-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#ShiroUser-Grid').jqGrid(
                'navGrid','#ShiroUser-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    // todo add password confirmation & validation
                    afterSubmit: afterSubmitForm,
                    beforeSubmit: beforeSubmitEditForm,
                    closeAfterEdit: true,
                    width: 800
                }, // edit options
                {
                    // todo add password confirmation & validation
                    afterSubmit: afterSubmitForm,
                    beforeSubmit: beforeSubmitAddForm,
                    closeAfterAdd: true,
                    width: 800
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#ShiroUser-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#ShiroUser-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    // check if Person exist and set person id
    function setPersonIdIfExist(postdata) {
        for (var i = 0; i < people.length; i++) {
            var person = people[i];
            if (person.value.toLowerCase() == postdata.person.toLowerCase()) {
                postdata.personId = person.id;
                break;
            }
        }
    }

    function beforeSubmitAddForm(postdata, formid) {
        // set person id
        setPersonIdIfExist(postdata)
        if (!postdata.personId) {
            return[false, '${g.message(code:"message.invalidPerson")}']
        }
        return[true, ''];
    }

    function beforeSubmitEditForm(postdata, formid) {
        // confirm password change
        if (postdata.password) {
            if (!confirm('${g.message(code: "confirmation.resetPassword", default: "Are you sure you want to change password?")}')) {
                return[false, "${g.message(code: "message.canceledByUser", default: "Canceled by user")}"];
            }
        }
        // set person id
        setPersonIdIfExist(postdata)
        if (!postdata.personId) {
            return[false, '${g.message(code:"message.invalidPerson")}']
        }
        return[true, ''];
    }

    $(function(){
        initShiroUserGrid();
    });

</script>

</body>
</html>