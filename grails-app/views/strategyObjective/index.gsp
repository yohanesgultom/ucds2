<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Strategy Objective Category</title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
    #strategy-objective-table tr {
        vertical-align: top;
    }
    .grid {
        margin-bottom: 3em;
    }
    </style>
</head>
<body>

<table id="strategy-objective-table" width="100%">
    <tr>
        <td width="50%">
            <table id="StrategyObjective-Grid"></table>
            <div id="StrategyObjective-Pager"></div>
        </td>
        <td width="50%">
            <div id="StrategyObjectiveChildContainer" class="grid">
                <table id="StrategyObjectiveChild-Grid"></table>
                <div id="StrategyObjectiveChild-Pager"></div>
            </div>
            <div id="KeySuccessFactorContainer" class="grid" style="display: none">
                <table id="KeySuccessFactor-Grid"></table>
                <div id="KeySuccessFactor-Pager"></div>
            </div>
        </td>
    </tr>
</table>
<div id="calculate-weight-dialog"></div>

<script type="text/javascript" src="${g.resource(file: 'js/sylvester.js')}"/>
<script type="text/javascript" src="${g.resource(file: 'js/sylvester.ahp.js')}"/>
<script type="text/javascript" src="${g.resource(file: 'js/jquery.jqgrid.ahp.js')}"/>
<script type="text/javascript">

    var strategyObjectiveGrid = $('#StrategyObjective-Grid');
    var strategyObjectivePager = $('#StrategyObjective-Pager');

    var strategyObjectiveChildGrid = $('#StrategyObjectiveChild-Grid');
    var strategyObjectiveChildPager = $('#StrategyObjectiveChild-Pager');
    var strategyObjectiveChildContainer = $('#StrategyObjectiveChildContainer');
    var calculateWeightDialog = $('#calculate-weight-dialog');

    var keySuccessFactorGrid = $('#KeySuccessFactor-Grid');
    var keySuccessFactorPager = $('#KeySuccessFactor-Pager');
    var keySuccessFactorContainer = $('#KeySuccessFactorContainer');

    var balanceScoreCardValues = '';
    var strategyObjectiveValues = '';
    var keySuccessFactorValues = '';
    var dependencyValues = '';
    var dependencyValuesMapper = {};

    var positions = [
        "Implementer",
        "Programmer",
        "CFO"
    ];

    function initStrategyObjectiveGrid(){
        strategyObjectiveGrid.jqGrid({
            url: '${request.getContextPath()}/strategyObjective/getList',
            editurl: '${request.getContextPath()}/strategyObjective/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.StrategyObjective.name.label" default="Name"/>',
                '<g:message code="app.StrategyObjective.perspective.label" default="Perspective"/>',
                '<g:message code="app.StrategyObjective.inactive.label" default="Status"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text'},
                {name:'perspective', index:'perspective', editable:true,edittype:'select',formatter:'select',editoptions:{value:getLOV('${request.getContextPath()}/balanceScoreCard/getListOfValues', balanceScoreCardValues, true)}},
                {name:'inactive', index:'inactive',editable:true,edittype:'select',formatter:'select',editoptions:{value:'false:Active;true:Inactive'}}
            ],
            height: 'auto',
            pager: '#StrategyObjective-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Strategy Objective',
            onSelectRow: function(rowid){
                reloadDependenciesGrid(rowid);
                reloadKeySuccesFactorsGrid(rowid);
            },
            sortname: 'id',
            sortorder: 'asc'
        });

        strategyObjectiveGrid.jqGrid(
                'navGrid','#StrategyObjective-Pager',
                {edit:true,add:true,del:true,view:false,search:false,refresh:false}, // options
                {
                    afterSubmit: afterSubmitFormStrategyObjective,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitFormStrategyObjective,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitFormStrategyObjective,
                    onclickSubmit: function(params, posdata){
                        var id = strategyObjectiveChildGrid.jqGrid('getGridParam','selrow');
                        var rd = strategyObjectiveChildGrid.jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        )
                .jqGrid('navButtonAdd','#StrategyObjective-Pager',{ caption:'', buttonicon:'ui-icon-link', onClickButton:function() {toggleGrid('StrategyObjectiveChild-Grid');}, position: 'last', title:'Dependency', cursor: 'pointer'})
                .jqGrid('navButtonAdd','#StrategyObjective-Pager',{ caption:'', buttonicon:'ui-icon-key', onClickButton:function() {toggleGrid('KeySuccessFactor-Grid');}, position: 'last', title:'Strategy Key Success Factors', cursor: 'pointer'});
    }

    function initStrategyObjectiveChildGrid(){
        dependencyValues = getLOV('${request.getContextPath()}/strategyObjective/getListOfValues', strategyObjectiveValues, true);
        dependencyValuesMapper = getLOVMapper(dependencyValues);

        strategyObjectiveChildGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.StrategyObjective.strategyobjective.label" default="Strategy Objective"/>',
                '<g:message code="app.StrategyObjective.weight.label" default="Weight(0..1)"/>'
            ],
            colModel :[
                {name:'dependency', index:'dependency', editable:true,edittype:'select',formatter:'select',editrules:{required:true},editoptions:{value:dependencyValues}},
                {name:'weight', index:'weight',editable:true,edittype:'text', editrules: {number: true, required:true, minValue:0,maxValue:1}}
            ],
            height: 'auto',
            pager: '#StrategyObjectiveChild-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Dependency',
            sortname: 'id',
            sortorder: 'asc'
        });

        strategyObjectiveChildGrid.jqGrid(
                'navGrid','#StrategyObjectiveChild-Pager',
                {edit:true,add:true,del:true,view:false,search:false}, // options
                {
                    zIndex:100,
                    width:'auto',
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    zIndex:100,
                    width:'auto',
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    zIndex:100,
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = strategyObjectiveChildGrid.jqGrid('getGridParam','selrow');
                        var rd = strategyObjectiveChildGrid.jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        ).jqGrid('navButtonAdd','#StrategyObjectiveChild-Pager',{ caption:'', buttonicon:'ui-icon-calculator', onClickButton:function() {JqGridAHP.openDialog('dependency', calculateWeightDialog, strategyObjectiveChildGrid, dependencyValuesMapper);}, position: 'last', title:'Calculate Weight', cursor: 'pointer'});

        // init calculate weight dialog
        JqGridAHP.initDialog('weight', calculateWeightDialog, strategyObjectiveChildGrid);
    }

    function initKeySuccessFactorGrid(){
        keySuccessFactorGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.keysuccessfactor.name.label" default="Title"/>',
                '<g:message code="app.keysuccessfactor.parent.label" default="Parent"/>',
                '<g:message code="app.keysuccessfactor.responsible.label" default="Responsible"/>'
            ],
            colModel :[
                {name:'title', index:'title',editable:true,edittype:'textarea'},
                {name:'parent', index:'parent', editable:true, edittype:'select',formatter:'select'},
                {name:'responsible', index:'responsible',editable:true,edittype:'textarea',editoptions:{
                    dataInit: function(elm) {
                        setTimeout(function () {
                            $(elm)
                            // don't navigate away from the field on tab when selecting an item
                            .bind( "keydown", function( event ) {
                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                        $( this ).data( "ui-autocomplete" ).menu.active ) {
                                    event.preventDefault();
                                }
                            })
                            .autocomplete({
                                minLength: 0,
                                source: function( request, response ) {
                                    // delegate back to autocomplete, but extract the last term
                                    response( $.ui.autocomplete.filter(
                                            positions, extractLast( request.term ) ) );
                                },
                                focus: function() {
                                    // prevent value inserted on focus
                                    return false;
                                },
                                select: function( event, ui ) {
                                    var terms = split( this.value );
                                    // remove the current input
                                    terms.pop();
                                    // add the selected item
                                    terms.push( ui.item.value );
                                    // add placeholder to get the comma-and-space at the end
                                    terms.push( "" );
                                    this.value = terms.join( ", " );
                                    return false;
                                }
                            });
                        }, 200);
                    }}
                }
            ],
            height: 'auto',
            pager: '#KeySuccessFactor-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Strategy Key Success Factor',
            sortname: 'id',
            sortorder: 'asc'
        });

        keySuccessFactorGrid.jqGrid(
                'navGrid','#KeySuccessFactor-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    zIndex:100,
                    width:'auto',
                    afterSubmit: afterSubmitFormKeySuccessFactor,
                    closeAfterEdit: true
                }, // edit options
                {
                    zIndex:100,
                    width:'auto',
                    afterSubmit: afterSubmitFormKeySuccessFactor,
                    closeAfterAdd: true
                }, // add options
                {
                    zIndex:100,
                    afterSubmit: afterSubmitFormKeySuccessFactor,
                    onclickSubmit: function(params, posdata){
                        var id = keySuccessFactorGrid.jqGrid('getGridParam','selrow');
                        var rd = keySuccessFactorGrid.jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }

    function afterSubmitFormStrategyObjective(response, postdata) {
        var res = $.parseJSON(response.responseText);
        if (res && res.success == true) {
            strategyObjectiveValues = getLOV('${request.getContextPath()}/strategyObjective/getListOfValues', strategyObjectiveValues, true);
            strategyObjectiveChildGrid.jqGrid('setColProp','dependency',{editoptions:{value:strategyObjectiveValues}});
        }
        return [res.success,res.message];
    }

    function afterSubmitFormKeySuccessFactor(response, postdata) {
        var res = $.parseJSON(response.responseText);
        keySuccessFactorValues = getLOV('${request.getContextPath()}/keySuccessFactor/getListOfValues', keySuccessFactorValues, true);
        keySuccessFactorGrid.setColProp('parent',{editoptions:{value:keySuccessFactorValues}});
        alert(res.message);
        return [true,""];
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        var msg = (res.success) ? '' : res.message;
        return [res.success,msg];
    }

    function toggleGrid(gridId) {
        if (gridId == 'StrategyObjectiveChild-Grid') {
            strategyObjectiveChildContainer.slideDown();
            keySuccessFactorContainer.slideUp();
        } else if (gridId == 'KeySuccessFactor-Grid') {
            strategyObjectiveChildContainer.slideUp();
            keySuccessFactorContainer.slideDown();
        }
    }

    function reloadDependenciesGrid(id) {
        var listUrl = '${request.getContextPath()}/strategyObjective/getDependencies?strategyObjectiveId='+id;
        var editUrl = '${request.getContextPath()}/strategyObjective/editDependencies?strategyObjectiveId='+id;
        strategyObjectiveChildGrid.jqGrid('setGridParam', {
            url: listUrl,
            editurl: editUrl
        }).trigger("reloadGrid");
    }

    function reloadKeySuccesFactorsGrid(id) {
        var listUrl = '${request.getContextPath()}/strategyObjective/getKeySuccessFactors?strategyObjectiveId='+id;
        var editUrl = '${request.getContextPath()}/strategyObjective/editKeySuccessFactor?strategyObjectiveId='+id;
        keySuccessFactorGrid.jqGrid('setGridParam', {
            url: listUrl,
            editurl: editUrl
        }).trigger("reloadGrid");
    }

    $(function(){
        initStrategyObjectiveGrid();
        initStrategyObjectiveChildGrid();
        initKeySuccessFactorGrid();
    });

</script>
</body>
</html>