<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="TRAINING_METHOD_QUESTION"/></title>
    <meta name="layout" content="embedded"/>
    <style>
        ul.ui-autocomplete {
            z-index: 3000;
            font-size: 90%;
        }
        .ui-autocomplete-loading {
            background: white url(${resource(dir: 'images', file: 'ui-anim_basic_16x16.gif')}) right center no-repeat;
        }
    </style>
</head>
<body>

<table id="TrainingMethodQuestion-Grid"></table>
<div id="TrainingMethodQuestion-Pager"></div>

<script type="text/javascript">

    // cache
    var trainingMethods = [];

    function split(term) {
        return term.split( /,\s*/ );
    }

    function extractLast(term) {
        return split(term).pop();
    }

    function initTrainingMethodQuestionGrid(){
        $("#TrainingMethodQuestion-Grid").jqGrid({
            url: '${request.getContextPath()}/trainingMethodQuestion/getList',
            editurl: '${request.getContextPath()}/trainingMethodQuestion/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.question.value.label" default="Value"/>',
                '<g:message code="app.question.active.label" default="Active"/>',
                '<g:message code="app.question.methods.label" default="Methods"/>'
            ],
            colModel :[
                {name:'value', index:'value',editable:true,edittype:'textarea',editrules: {required: true}},
                {name:'active', index:'active', width: 10, editable:true,edittype:'checkbox',editoptions: {value: "true:false"}},
                {name: 'methods', index: 'methods', hidden: true, editable:true, editrules: {edithidden: true, required: true}, edittype: 'textarea', editoptions: {
                    dataInit:function(el){
                        $(el)
                            // don't navigate away from the field on tab when selecting an item
                          .bind( "keydown", function( event ) {
                            if ( event.keyCode === $.ui.keyCode.TAB &&
                                $( this ).data( "ui-autocomplete" ).menu.active ) {
                              event.preventDefault();
                            }
                          })
                          .autocomplete({
                            source: function( request, response ) {
                                var term = extractLast(request.term);
                                if (!trainingMethods || trainingMethods.length <= 0) {
                                    $.getJSON(
                                        "${request.getContextPath()}/trainingMethod/getListForAutocomplete", 
                                        {term: term}, 
                                        function (data) { 
                                            trainingMethods = data;
                                        }
                                    );
                                }                                

                                // filter
                                var res = []; 
                                var pattern = new RegExp('(' + term + ')', 'i');
                                for (var i = 0; i < trainingMethods.length; i++) {                                    
                                    if (trainingMethods[i].value && trainingMethods[i].value.match(pattern)) {
                                        res.push(trainingMethods[i]);
                                    }
                                }
                                response(res);
                            },
                            search: function() {
                              // custom minLength
                              var term = extractLast( this.value );
                              if ( term.length < 2 ) {
                                return false;
                              }
                            },
                            focus: function() {
                              // prevent value inserted on focus
                              return false;
                            },
                            select: function( event, ui ) {
                              var terms = split( this.value );
                              // remove the current input
                              terms.pop();
                              // add the selected item
                              terms.push( ui.item.value );
                              // add placeholder to get the comma-and-space at the end
                              terms.push( "" );
                              this.value = terms.join( ", " );
                              return false;
                            }
                          });
                    }                    
                }}
            ],
            height: 'auto',
            pager: '#TrainingMethodQuestion-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="TRAINING_METHOD_QUESTION"/>',
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#TrainingMethodQuestion-Grid').jqGrid(
                'navGrid','#TrainingMethodQuestion-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#TrainingMethodQuestion-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#TrainingMethodQuestion-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initTrainingMethodQuestionGrid();
    });

</script>

</body>
</html>