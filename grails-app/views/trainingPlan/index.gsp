<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Training Plans</title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
        .grid {
            margin-bottom: 3em;
        }
    </style>
</head>
<body>

<div id="TrainingPlan-Container" class="grid">
    <table id="TrainingPlan-Grid"></table>
    <div id="TrainingPlan-Pager"></div>
</div>

<div id="TrainingProgram-Container" class="grid" style="display: none">
    <table id="TrainingProgram-Grid"></table>
    <div id="TrainingProgram-Pager"></div>
</div>

<script type="text/javascript">

    // cache
    var trainingPlanContainer = $('#TrainingPlan-Container');
    var trainingProgramContainer = $('#TrainingProgram-Container');
    var trainingPlanGrid = $('#TrainingPlan-Grid');
    var trainingProgramGrid = $('#TrainingProgram-Grid')

    function initTrainingPlanGrid(){
        trainingPlanGrid.jqGrid({
            url: '${request.getContextPath()}/trainingPlan/getList',
            editurl: '${request.getContextPath()}/trainingPlan/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.trainingPlan.name.label" default="Name"/>',
                '<g:message code="app.trainingPlan.dateCreated.label" default="Date Created"/>',
                '<g:message code="app.trainingPlan.createdBy.label" default="Created By"/>',
                '<g:message code="app.trainingPlan.lastUpdated.label" default="Last Updated"/>',
                '<g:message code="app.trainingPlan.updatedBy.label" default="Updated By"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'dateCreated', index:'dateCreated',editable:false,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'createdBy', index:'createdBy',editable:false,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'lastUpdated', index:'lastUpdated',editable:false,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'updatedBy', index:'updatedBy',editable:false,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}
            ],
            height: 'auto',
            pager: '#TrainingPlan-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Training Plans',
            onSelectRow: function(rowid){
                lastSel = rowid;
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        trainingPlanGrid.jqGrid(
                'navGrid','#TrainingPlan-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = trainingPlanGrid.jqGrid('getGridParam','selrow');
                        var rd = trainingPlanGrid.jqGrid('getRowData', id);
                        return {id : rd.id};
                }
                }, // del options
                {}, // search options
                {} // view options
                )
                .jqGrid('navSeparatorAdd', '#TrainingPlan-Pager',{sepclass : 'ui-separator',sepcontent: ''})
                .jqGrid('navButtonAdd','#TrainingPlan-Pager',{
                    caption:'Edit Programs',
                    buttonicon:'ui-icon-gear',
                    position: 'last', title:'',
                    cursor: 'pointer',
                    onClickButton:function() {
                        var trainingPlanId = trainingPlanGrid.jqGrid('getGridParam','selrow');
                        if(trainingPlanId) {
                            toggleGrid('TrainingProgram-Grid');
                            var trainingPlanData = trainingPlanGrid.jqGrid('getRowData', trainingPlanId);
                            var listUrl = '${request.getContextPath()}/trainingProgram/getFilteredList';
                            var editUrl = '${request.getContextPath()}/trainingProgram/edit?trainingPlanId='+trainingPlanId;
                            trainingProgramGrid.jqGrid('setGridParam', {
                                url: listUrl,
                                editurl: editUrl,
                                postData: {searchField: 'trainingPlan.id', searchOper: 'eq', searchString: trainingPlanId}
                            }).jqGrid('setCaption', 'Training Programs for : ' + trainingPlanData.name).trigger("reloadGrid");
                        } else {
                            alert('Please select a row');
                        }
                    }
                });
        
    }

    /* Training Plan Programs Grid */

    function initTrainingProgramGrid(){

        // todo put in sysparam or domain
        var certifications = 'T:Test;P:Participant';
        var classifications = 'A:All;AWE:All With Exception;L:Leader;O:Operator;S:Staff;C:Crew';
        var categories = 'Core:Core;General:General;Managerial:Managerial;Technical:Technical';

        trainingProgramGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.trainingProgram.name.label" default="Name"/>',
                '<g:message code="app.trainingProgram.category.label" default="Category"/>',
                '<g:message code="app.trainingProgram.objective.label" default="Objective"/>',
                '<g:message code="app.trainingProgram.trainer.label" default="Trainer"/>',
                '<g:message code="app.trainingProgram.certification.label" default="Certification"/>',
                '<g:message code="app.trainingProgram.classification.label" default="Classification"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}  ,
                {name:'category', index:'category',editable:true,formatter:'select',edittype:'select',editoptions:{value:categories}},
                {name:'objective', index:'objective',editable:true,edittype:'textarea'}  ,
                {name:'trainer', index:'trainer',editable:true,edittype:'text'},
                {name:'certification', index:'certification',editable:true,formatter:'select',edittype:'select',editoptions:{value:certifications}},
                {name:'classification', index:'classification',editable:true,formatter:'select',edittype:'select',editoptions:{value:classifications}}
            ],
            height: 'auto',
            pager: '#TrainingProgram-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Training Programs',
            sortname: 'id',
            sortorder: 'asc'
        });

        trainingProgramGrid.jqGrid(
                'navGrid','#TrainingProgram-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = trainingProgramGrid.jqGrid('getGridParam','selrow');
                        var rd = trainingProgramGrid.jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        )
                .jqGrid('navSeparatorAdd', '#TrainingProgram-Pager',{sepclass : 'ui-separator',sepcontent: ''})
                .jqGrid('navButtonAdd','#TrainingProgram-Pager',{ caption:'Back', buttonicon:'ui-icon-arrowreturnthick-1-w', onClickButton:function() {toggleGrid('TrainingPlan-Grid');}, position: 'last', title:'', cursor: 'pointer'})
                .jqGrid('navButtonAdd','#TrainingProgram-Pager',{ caption:'Export', buttonicon:'ui-icon-arrowthickstop-1-s', onClickButton:exportProgramsToExcel, position: 'last', title:'', cursor: 'pointer'});
    }


    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function toggleGrid(gridId) {
        trainingPlanContainer.slideToggle(gridId == 'TrainingPlan-Grid');
        trainingProgramContainer.slideToggle(gridId == 'TrainingProgram-Grid');
        if (gridId == 'TrainingPlan-Grid') $('#TrainingPlan-Grid').jqGrid().trigger("reloadGrid");
    }

    function exportProgramsToExcel() {
        var trainingPlanId = trainingPlanGrid.jqGrid('getGridParam','selrow');
        var actionUrl = '${request.getContextPath()}/trainingProgram/exportToExcel?trainingPlanId='+trainingPlanId;
        window.open(actionUrl, '_blank');
    }

    $(function(){
        initTrainingPlanGrid();
        initTrainingProgramGrid();
    });

</script>
</body>
</html>