<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="TRAINING_PROGRAMS"/></title>
    <meta name="layout" content="embedded"/>
    <style>
    #training-table tr {
        vertical-align: top;
    }
    #limit-table {
        font-size: 90%;
        vertical-align: middle;
    }
    #costLimit, #timeLimit {
        width: 150px;
    }
    </style>
</head>

<body>

    <div class="action-bar">
        <g:select id="periodYear" name="periodYear" title="${g.message(code:'period.year',default:'Period Year')}" from="${years}" value="${defaultYear}"/>
        <g:select id="periodMonth" name="periodMonth" title="${g.message(code:'period.month',default:'Period Month')}" from="${months.entrySet()}" optionKey="key" optionValue="value" value="${defaultMonth}"/>
        <span id="reload">${g.message(code: "refresh", default: "Refresh")}</span>
        <span id="export">${g.message(code: "export", default: "Export")}</span>
    </div>

    <input type="hidden" id="totalCost"/>
    <input type="hidden" id="totalTime"/>
    <table id="limit-table">
        <tr>
            <td>${g.message(code: "trainingplan.costlimit", default: "Cost Limit (IDR)")} &nbsp;&nbsp; </td>
            <td><span id="costLimit"></span> &nbsp;&nbsp; ${g.img(dir: "images", file: 'gear.png', class: 'edit-button')}</td>
        </tr>
        <tr>
            <td>${g.message(code: "trainingplan.timelimit", default: "Time Limit (Day)")} &nbsp;&nbsp;</td>
            <td><span id="timeLimit"></span> &nbsp;&nbsp; ${g.img(dir: "images", file: 'gear.png', class: 'edit-button')}</td>
        </tr>
    </table>

    <br>

    <table id="training-table">
        <tr>
            <td>
                <table id="TrainingProgramGrid"></table>
                <div id="TrainingProgramPager"></div>
            </td>
            <td>
                <table id="TrainingProgramDetailsGrid"></table>
                <div id="TrainingProgramDetailsPager"></div>
            </td>
        </tr>
    </table>

</table>

<script type="text/javascript">
    var periodMonth = $('#periodMonth'),
        periodYear = $('#periodYear'),
        lastSel = null

    var grid = $("#TrainingProgramGrid"),
        gridListUrl = '${request.getContextPath()}/trainingProgram/getList',
        detailsGrid = $("#TrainingProgramDetailsGrid"),
        detailsGridListUrl = '${request.getContextPath()}/trainingProgram/getDetailsList',
        exportUrl = '${request.getContextPath()}/trainingProgram/exportToExcel'

    function initTrainingProgramGrid() {
        grid.jqGrid({
            url: gridListUrl,
            postData: getFilterParams(),
            datatype: 'json',
            mtype: 'POST',
            autowidth: false,
            colNames: [
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.actualscore.label" default="Actual Score"/>',
                '<g:message code="ucds.competency.expectedscore.label" default="Expected Score"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>'
            ],
            colModel: [
                {name: 'firstName', index: 'p.firstName', editable: false, edittype: 'text', searchoptions: {sopt: ['cn', 'nc']}},
                {name: 'lastName', index: 'p.lastName', editable: false, edittype: 'text', searchoptions: {sopt: ['cn', 'nc']}},
                {name: 'position', index: 'p.personPosition.name',editable: false, edittype: 'text', searchoptions: {sopt: ['cn', 'nc']}},
                {name: 'competency', index: 'c.name', editable: false, edittype: 'text', searchoptions: {sopt: ['cn', 'nc']}},
                {name: 'actualScore', index: 'actualScore', editable: false, edittype: 'text', width: 60, sortable: false, search: false},
                {name: 'expectedScore', index: 'expectedScore', editable: false, edittype: 'text', width: 60, sortable: false, search: false},
                {name: 'expectedIndicator', index: 'expectedIndicator', editable: false, edittype: 'text', width: 500, sortable: false, search: false}
            ],
            height: 'auto',
            pager: '#TrainingProgramPager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            gridview: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="TRAINING_PROGRAMS"/>',
            onSelectRow: function (rowid) {
                var rd = grid.jqGrid('getRowData', rowid);
                var listUrl = '${request.getContextPath()}/trainingProgram/getDetailsList';
                var editUrl = '${request.getContextPath()}/trainingProgram/edit';
                detailsGrid.jqGrid('setGridParam', {
                    url: listUrl,
                    editurl: editUrl,
                    postData: {personCompetencyId: rowid}
                }).trigger("reloadGrid");
                lastSel = rd
            },
            sortname: 'pc.id',
            sortorder: 'asc'
        });

        grid.jqGrid(
                'navGrid', '#TrainingProgramPager',
                {edit: false, add: false, del: false, view: false, search: true}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );
    }

    function initTrainingProgramDetailsGrid() {
        detailsGrid.jqGrid({
            postData: getFilterParams(),
            datatype: 'json',
            mtype: 'POST',
            autowidth: false,
            colNames: [
                '<g:message code="ucds.trainingmethod.level.label" default="Level"/>',
                '<g:message code="ucds.trainingmethod.label" default="Method"/>',
                '<g:message code="ucds.trainingmethod.description.label" default="Description"/>',
                '<g:message code="ucds.trainingProgram.trainerOrPic.label" default="Trainer/PIC"/>',
                '<g:message code="ucds.trainingProgram.trainerOrPicEmail.label" default="E-mail"/>',
                '<g:message code="ucds.trainingProgram.reminderDayMinus.label" default="Reminder Day(s)"/>',
                '<g:message code="ucds.trainingProgram.startDate.label" default="Start Date"/>',
                '<g:message code="ucds.trainingProgram.endDate.label" default="End Date"/>',
                '<g:message code="ucds.trainingProgram.cost.label" default="Cost (IDR)"/>',
                '<g:message code="ucds.trainingProgram.note.label" default="Note"/>'
            ],
            colModel: [
                {name: 'competencyLevel', width: 50, index: 'tp.competencyLevel', editable: false},
                {name: 'trainingMethodDisplay', index: 'tm.name', editable: false},
                {name: 'trainingMethodDescDisplay', index: 'trainingMethodDescDisplay', editable: false, hidden: true, sortable: false},
                {name: 'trainerOrPic', index: 'p.firstName', editable: true, edittype: 'text', editoptions: {dataInit: personAutoCompleteInit}},
                {name: 'trainerOrPicEmail', index: 'trainerOrPicEmail', editable: false, hidden: true, sortable: false},
                {name: 'reminderDayMinus', index: 'tp.reminderDayMinus', editable: true, edittype: 'text', hidden: true, editrules: {custom: true, custom_func: listOfNumbers, edithidden: true}},
                {name: 'startDate', index: 'tp.startDate', editable: true, edittype: 'text', formatter: 'date', formatoptions: {newformat: 'd-m-Y'}, editoptions: {
                    dataInit:function(el){
                        $(el)
                                .datepicker({
                                    dateFormat:'dd-mm-yy',
                                    changeYear: true,
                                    changeMonth: true,
                                    minDate: new Date(),
                                    showOn: 'both',
                                    buttonImage: '${resource(dir: "images", file: "calendar-icon.png")}',
                                    buttonImageOnly: true})
                                .change(function() {
                                    var val = $(el).val();
                                    if (val) {
                                        var dp = $('#endDate');
                                        var minDate = parseUcdsDate(val);
                                        if (dp.val()) {
                                            var endDate = parseUcdsDate(dp.val());
                                            if (endDate && endDate.getTime() < minDate.getTime()) {
                                                dp.val('');
                                                alert('End date is before start date');
                                            }
                                        }
                                        dp.datepicker('option', 'minDate', minDate);
                                    }
                                });
                    },
                    defaultValue: function() {
                        var currentTime = new Date();
                        return currentTime.getDate() + "-" + parseInt(currentTime.getMonth() + 1) + "-" + currentTime.getFullYear();
                    }
                }},
                {name: 'endDate', index: 'tp.endDate', editable: true, edittype: 'text', formatter: 'date', formatoptions: {newformat: 'd-m-Y'}, editoptions: {
                    dataInit:function(el){
                        $(el)
                                .datepicker({
                                    dateFormat:'dd-mm-yy',
                                    changeYear: true,
                                    changeMonth: true,
                                    minDate: new Date(),
                                    showOn: 'both',
                                    buttonImage: '${resource(dir: "images", file: "calendar-icon.png")}',
                                    buttonImageOnly: true
                                })
                                .change(function() {
                                    var val = $(el).val();
                                    if (val) {
                                        var dp = $('#startDate');
                                        var maxDate = parseUcdsDate(val);
                                        if (dp.val()) {
                                            var startDate = parseUcdsDate(dp.val());
                                            if (startDate && startDate.getTime() > maxDate.getTime()) {
                                                dp.val('');
                                                alert('Start date is after end date');
                                            }
                                        }
                                        dp.datepicker("option", "maxDate", maxDate);
                                    }
                                });
                    },
                    defaultValue: function() {
                        var currentTime = new Date();
                        return currentTime.getDate() + "-" + parseInt(currentTime.getMonth() + 1) + "-" + currentTime.getFullYear();
                    }
                }},
                {name: 'cost', index: 'tp.cost', editable: true, edittype: 'text', editrules:{number: true}, formatter: 'currency', formatoptions: {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 0, suffix:"", defaultValue: '0.00'}},
                {name: 'note', index: 'note', editable: true, edittype: 'textarea', editoptions: {rows:10, cols:30}, hidden: true, sortable: false, editrules: {edithidden: true}}
            ],
            height: 'auto',
            pager: '#TrainingProgramDetailsPager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: false,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="TRAINING_PROGRAMS_DETAILS"/>',
            sortname: 'tp.competencyLevel',
            sortorder: 'asc'
        });

        detailsGrid.jqGrid(
                'navGrid', '#TrainingProgramDetailsPager',
                {edit: true, add: false, del: true, view: false, search: false}, // options
                {
                    top: 100,
                    left: 300,
                    width: 500,
                    closeAfterEdit: true,
                    beforeSubmit: beforeSubmitForm,
                    afterSubmit: afterSubmitFormTrainingProgramDetails
                }, // edit options
                {}, // add options
                {
                    afterSubmit: afterSubmitFormTrainingProgramDetails
                }, // del options
                {}, // view options
                {} // search options
        );
    }

    function afterSubmitFormTrainingProgramDetails(response, postdata){
        loadTotal();
        var res = $.parseJSON(response.responseText);
        var success = (res.message) && ((res.message.toLowerCase().indexOf('success') > -1) || (res.message.toLowerCase().indexOf('sukses') > -1));
        alert(res.message);
        return [success, res.message];
    }

    function loadLimit() {
        var costLimit = $('#costLimit');
        var timeLimit = $('#timeLimit');
        costLimit.html(spinner);
        timeLimit.html(spinner);
        $.getJSON('${request.getContextPath()}/trainingPlan/getLimitCostAndTime', getFilterParams(), function(response) {
            costLimit.html($('<input>').attr('type','text').val(response.costLimit).attr('disabled',true).on('keypress', function(e) {if (e.keyCode == "13") {saveLimit(this); return false;}}));
            timeLimit.html($('<input>').attr('type','text').val(response.timeLimit).attr('disabled',true).on('keypress', function(e) {if (e.keyCode == "13") {saveLimit(this); return false;}}));
        });
    }

    function saveLimit(element) {
        var textbox;
        if ($(element).is('img')) {
            // on img click
            textbox = $(element).prev().find('input');
        } else {
            // on key press
            textbox = $(element);
        }
        var save = !textbox.attr('disabled');
        textbox.attr('disabled', save).focus();
        if (save) {
            var params = getFilterParams();
            var postData = {};
            postData['period'] = params.period;
            postData[textbox.parent().attr('id')] = textbox.val();
            $.getJSON('${request.getContextPath()}/trainingPlan/edit', postData, function(response) {
                if (response.error) {
                    alert(response.error);
                }
                // reload total
                loadTotal();
            });
        }
    }

    function loadTotal() {
        var postData = {};
        var params = getFilterParams();
        postData['period'] = params.period;
        $.getJSON('${request.getContextPath()}/trainingPlan/getTotalCostAndTime', postData, function(response) {
            if (response) {
                var costLimit = $('#costLimit').find('input').val();
                var timeLimit = $('#timeLimit').find('input').val();
                $('#totalCost').val(response.totalCost);
                $('#totalTime').val(response.totalTime);
                var costDelta = costLimit - response.totalCost;
                var timeDelta = timeLimit - response.totalTime;
                var costColor = (costDelta > 0) ? 'green' : 'tomato';
                var timeColor = (timeDelta > 0) ? 'green' : 'tomato';
                var costTitle = (costDelta > 0) ? '' : '${g.message(code: "trainingplan.exceedcost", default: "Cost limit exceeded")} ';
                var timeTitle = (timeDelta > 0) ? '' : '${g.message(code: "trainingplan.exceedtime", default: "Time limit exceeded")} ';
                $('#costLimit').find('input').attr('title', costTitle + costDelta).css('border-color', costColor);
                $('#timeLimit').find('input').attr('title', timeTitle + timeDelta).css('border-color', timeColor);
            }
        });
    }

    function listOfNumbers(value, colname) {
        if (value && value.length > 0) {
            var values = value.split(',');
            var valid = true;
            var invalidStr = null;
            for (var i=0; i < values.length; i++) {
                if (Number(values[i]) != values[i] || values[i] % 1 != 0) {
                    valid = false;
                    invalidStr = values[i];
                    break;
                }
            }
            if (!valid) {
                return [false, invalidStr + " is not a number"]
            }
        }
        return [true,""];
    }

    function getFilterParams() {
        var period = '1-' + $('#periodMonth').val() + '-' + $('#periodYear').val();
        return {period: period}
    }

    function beforeSubmitForm(postdata, formid) {
        // check if Person exist
        var traineeId,
            traineeName = lastSel.lastName ? lastSel.firstName + ' ' + lastSel.lastName : lastSel.firstName
        for (var i = 0; i < people.length; i++) {
            var person = people[i];
            if (person.value.toLowerCase() == postdata.trainerOrPic.toLowerCase()) {
                postdata.trainerOrPicId = person.id;
            }
            if (person.value.toLowerCase() == traineeName.toLowerCase()) {
                traineeId = person.id
            }
            if (postdata.trainerOrPicId && traineeId) break
        }
        var message
        if (!postdata.trainerOrPicId) {
            message = '${g.message(code:"message.invalidPerson")}'
        } else if (traineeId == postdata.trainerOrPicId) {
            message = '${g.message(code:"message.canNotBeSamePerson")}'
        }
        return[!message, message];
    }

    // reload button
    $('#reload').button({
        icons: {
            primary: 'ui-icon-refresh'
        }
    }).click(function() {
        loadLimit();

        grid.jqGrid('setGridParam', {
            url: gridListUrl,
            postData: getFilterParams()
        }).trigger("reloadGrid");

        detailsGrid.jqGrid('setGridParam', {
            url: detailsGridListUrl,
            postData: {personCompetencyId: null}
        }).trigger("reloadGrid");
    }).css({'font-size' : '90%'});

    // export button
    $('#export').button({
        icons: {
            primary: 'ui-icon-document'
        }
    }).click(function() {
        var period = getFilterParams().period
        window.open(exportUrl + '?period=' + period)
    }).css({'font-size' : '90%'});

    // edit limit button
    $('.edit-button','#limit-table').click(function(){saveLimit(this)});

    $(function () {
        initTrainingProgramGrid();
        initTrainingProgramDetailsGrid();
        loadLimit();
        loadTotal();
    });

</script>
</body>
</html>