<%=packageName ? "package ${packageName}" : ''%>
<% def instanceName = propertyName.replace('Instance','') %>
import grails.converters.JSON

class ${className}Controller {

    def ${instanceName}Service

    def index() { }

    def getList = {
        def res = ${instanceName}Service.getList(params)
        def ret = [rows:res.rows,records:res.totalRecords,page:params.page,total:res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [:]
        switch(params.oper){
            case "add" :
                try {
                    ${instanceName}Service.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "edit" :
                try {
                    ${instanceName}Service.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "del" :
                try {
                    ${instanceName}Service.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
