<%=packageName ? "package ${packageName}" : ''%>
<% def count = 0; def properties = domainClass.getPersistentProperties(); %>
<% def propertiesMapStr = ""; for (property in properties) { if (!property.isAssociation()) { propertiesMapStr += property.name + ": 'value'"; if (count < properties.length - 1) propertiesMapStr += ", "; count++; }  } %>
import com.emerio.baseapp.utils.GridUtilService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(${className}Service)
@Mock([${className},GridUtilService])
class ${className}ServiceTests {

    void testGetList() {
        def rowList = [new ${className}(${propertiesMapStr})]
        def serviceStub = mockFor(GridUtilService)
        def params = [page: 1]
        serviceStub.demand.getDomainList {Class c, Map map -> [rows: rowList, totalRecords: rowList.size(), page:1, totalPage: 1]}
        service.gridUtilService = serviceStub.createMock()
        service.getList(params)
    }

    void testAdd() {
        def params = [${propertiesMapStr}]
        service.add(params)
        assert ${className}.count() == 1
    }

    void testEdit() {
        mockDomain(${className}, [[${propertiesMapStr}]])
        def params = [${propertiesMapStr}]
        service.edit(params)
        assert ${className}.count() == 1
        assert ${className}.findByKey('value').value == 'value'
    }

    void testDelete() {
        mockDomain(${className}, [[${propertiesMapStr}]])
        def params = [${propertiesMapStr}]
        service.delete(params)
        assert ${className}.count() == 0
    }

}
