/**
 * Initialize method question form
 * @param el
 */
function initMethodQuestionForm(el, url, url2, pageSize) {

    pageSize = (pageSize) ? pageSize : 5;

    // create popup
    var popup = $('<div>Loading..</div>');
    $(popup)
        .dialog({
            autoOpen: false,
            modal: true,
            width: 800,
            title: 'Training Method Questions',
            buttons: {
                Ok: function() {
                    showOverlay();
                    var checkedQuestions = [];
                    $('input[type=checkbox]:checked', $(this)).each(function(index,cb) {
                        checkedQuestions.push(cb.value);
                    });
                    $.getJSON(url2, {questionIds:checkedQuestions.join(', ')}, function(data) {
                        if (data) {
                            $('#methods').val(data.methods.join(', '));
                            $(popup).dialog('close');
                        } else {
                            alert('Failed to get methods. Please try again');
                        }
                        hideOverlay();
                    });
                },
                Cancel: function() {
                    $(this).dialog('close');
                }
            }
        })
        .parent().css('font-size', '90%');

    // populate data from db
    $.getJSON(
        url, {},
        function (data) {
            var table = $('<table width="100%">');
            var paginator = $('<div class="paginator">').css('margin-top', '10px').css('text-align', 'center');
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var page = Math.floor(i / pageSize) + 1;
                    var rowClass = 'question page-' + page;
                    var style = (page > 1) ? ' style="display:none" ' : '';
                    $(table).append('<tr class="' + rowClass + '"' + style + '><td><p>' + data[i].value + '</p></td><td><input type="checkbox" value="' + data[i].id + '"/></td></tr>');
                }
                var totalPage = Math.ceil(data.length / pageSize);
                for (var i = 1; i <= totalPage; i++) {
                    var pageLink = $('<a>')
                        .attr('href', 'javascript:')
                        .css('margin-right', '5px')
                        .css('padding', '3px')
                        .css('text-decoration', 'none')
                        .attr('title', 'Page ' + i)
                        .text(i).
                        on('click', function () {
                            var pageId = $(this).text();
                            $('tr.question', popup).hide();
                            $('tr.page-' + pageId, popup).show();
                            $('a', paginator).css('color','blue').css('background-color','');
                            $(this).css('color','').css('background-color','#ccc');
                        });
                    // first page
                    pageLink.css('color', i == 1 ? '' : 'blue').css('background-color', i == 1 ? '#ccc' : '');
                    $(paginator).append(pageLink);
                }
            }
            $(popup).empty().append(table).append(paginator);
        }
    );

    // bind event
    $(el).attr('readonly','true').on('click', function() {
        $('input[type=checkbox]', $(popup)).attr('checked', false);
        $(popup).dialog('open');
    });

}
